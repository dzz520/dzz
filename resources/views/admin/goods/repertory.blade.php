@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/goods/repertory')}}" enctype="multipart/form-data" method="post" id="form">
            {{ csrf_field() }}
            <div id="attr_value"></div>
            <input type="hidden" id="id" name="id" value="{{isset($goods) ? $goods['id'] : '0'}}">
            <ul class="SKU_TYPE">
                <li is_required='1' propid='1' sku-type-name="商品名称"><em>*</em>商品名称：</li>
            </ul>
            <ul>
                <li><label><input type="checkbox" checked class="sku_value" propvalid="{{$goods['id']}}" attr="{{$goods['id']}}" value="{{$goods['goods_name']}}" />{{$goods['goods_name']}}</label></li>
            </ul>
            <div class="clear"></div>
            @foreach ($attr as $key => $val)
            <ul class="SKU_TYPE">
                <li is_required='1' propid='1' sku-type-name="{{$val[0]['attr_name']}}"><em>*</em>{{$val[0]['attr_name']}}：</li>
            </ul>
            <ul>
                @foreach ($val as $k => $v)
                <li><label><input type="checkbox" class="sku_value" propvalid="" attr="{{$v['attr_id']}}" valueId="{{$v['value_id']}}" value="{{$v['attr_value']}}" />{{$v['attr_value']}}</label></li>
                @endforeach
            </ul>
            <div class="clear"></div>
            @endforeach
            
            <!--单个sku值克隆模板-->
            <li style="display: none;" id="onlySkuValCloneModel">
                <input type="checkbox" class="model_sku_val" propvalid='' value="" />
                <input type="text" class="cusSkuValInput" />
                <a href="javascript:void(0);" class="delCusSkuVal">删除</a>
            </li>
            <div class="clear"></div>
            <div id="skuTable"></div>
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
<link rel="stylesheet" href="{{asset('css/sku_style.css')}}" />
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/createSkuTable.js')}}"></script>
<script type="text/javascript" src="{{asset('js/customSku.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/layer/layer.js')}}"></script>
<script type="text/javascript" src="{{asset('js/getSetSkuVals.js')}}"></script>
<script>
    $('.sku_value').click(function(){
        $('#attr_value').empty();
        $('#form :input:checkbox:checked').each(function(k,v){
            if (k != 0) {
                $('#attr_value').append('<input type="hidden" name="attr_value[]" value="'+$(v).attr('attr')+','+$(v).attr('valueId')+'">');
            }

        });
    })
</script>
@stop