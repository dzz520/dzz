<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'AdminLTE 2',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>服装商城</b>',

    'logo_mini' => '<b>服装</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'admin',

    'logout_url' => 'admin/logout',

    'logout_method' => 'GET',

    'login_url' => 'admin/login',

    'register_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        '后台管理',
        [
            'text' => '账号中心',
            'is_show' => true,
            'icon' => 'user',
            'url' => null,
            'submenu' => [
                [
                    'text' => '个人信息',
                    'url' => 'admin/current_admin_info',
                    'icon' => '',
                    'is_show' => true,
                ],
                [
                    'text' => '重置密码',
                    'url' => 'admin/current_admin_reset',
                    'icon' => '',
                    'is_show' => true,
                ]
            ],
            'active' => ['current_admin_*']
        ],
        [
            'text' => '权限管理',
            'icon' => 'lock',
            'permission_name' => 'power',
            // 'label'       => 4,
            // 'label_color' => 'success',
            'submenu' => [
                [
                    'text' => '管理员列表',
                    'url' => 'admin/admins',
                    'active' => ['admin/*', 'admins'],
                    'icon' => '',
                    'permission_name' => 'admin_list',
                ],
                [
                    'text' => '角色列表',
                    'url' => 'admin/roles',
                    'icon' => '',
                    'active' => ['roles', 'role/*'],
                    'permission_name' => 'role_list'
                ],
                [
                    'text' => '权限列表',
                    'url' => 'admin/permissions',
                    'icon' => '',
                    'active' => ['permissions', 'permission/*'],
                    'permission_name' => 'permission_list'
                ]
            ],
            'active' => ['admins', 'admin/*', 'roles','role/*', 'permissions', 'permission/*'],
        ],
        '前台管理',
        [
            'text' => '用户管理',
            'icon' => 'users',
            'permission_name' => 'user',
            'submenu' => [
                [
                    'text' => '用户列表',
                    'url' => 'admin/users',
                    'icon' => '',
                    'permission_name' => 'user_list'
                ]
            ],
            'active' => ['users', 'user/*'],
        ],
        [
            'text' => '分类管理',
            'icon' => 'sitemap',
            'permission_name' => 'category',
            'submenu' => [
                [
                    'text' => '分类添加',
                    'url' => 'admin/categorys/index',
                    'icon' => ''
                ],
                [
                    'text' => '分类列表',
                    'url' => 'admin/categorys',
                    'icon' => ''
                ]
            ],
            'active' => ['categorys', 'category/*', 'tags', 'tag/*']
        ],
        [
            'text' => '属性管理',
            'icon' => 'building',
            'permission_name' => 'company',
            'submenu' => [
                [
                    'text' => '添加商品属性名',
                    'url' => 'admin/goodsattrk',
                    'icon' => '',
                ],
                [
                    'text' => '添加商品属性值',
                    'url' => 'admin/goodsattrv',
                    'icon' => '',
                ]
            ],
            'active' => ['company/*', 'companys']
        ],
        [
            'text' => '商品管理',
            'icon' => 'building',
            'permission_name' => 'goods',
            'submenu' => [
                [
                    'text' => '添加商品',
                    'url' => 'admin/goods/index',
                    'icon' => ''
                ],
                [
                    'text' => '商品列表',
                    'url' => 'admin/goods',
                    'icon' => '',
                    'permission_name' => 'list'
                ]
            ],
            'active' => ['goods', 'goods/*']
        ],
        [
            'text' => '订单管理',
            'icon' => 'building',
            'permission_name' => 'goods',
            'submenu' => [
                [
                    'text' => '订单列表',
                    'url' => '',
                    'icon' => '',
                    'permission_name' => 'list'
                ]
            ],
            'active' => ['goods', 'goods/*']
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,

        App\Http\Controllers\Admin\Menu::class
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];