<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Goods;
use App\Models\Categorys;
use Validator;
/**
 * 
 */
class GoodsController extends Controller
{
    /**
     * 添加商品信息
     */
    public function index()
    {
        $dataType=(new Categorys)->dataType();
        return view('admin.goods.add_goods',[
            'content_header' => '商品管理',
            'box_title' => '添加商品',
            'dataType'=>$dataType
        ]);
    }

    /**
    * 商品列表
    */
    public function list()
    {
        return view('admin.goods.list', [
            'content_header' => '商品管理',
            'box_title' => '商品列表'
        ]);
    }

    /**
    *查询商品数据
    */
    public function data(Request $request)
    {
        $query = DB::table('goods_show')
                ->join('goods_image','goods_show.id','=','goods_image.goods_id')
                ->where('goods_show.goods_state','!=','3')
                ->groupBy('goods_image.goods_id')
                ->get();

        $data = datatables()->of($query)
            ->editColumn('hotgoods', function($data){
                return $data->hotgoods = $data->hotgoods ? '否' : '是';
            })
            ->addColumn('goods_state', function($data){
                if($data->goods_state == 1){
                    return '在售';
                }else if($data->goods_state == 2){
                    return '下架';
                }
            })
            ->toJson();
        return $data;
    }

    /**
    *删除数据
    */
    public function delete(request $request)
    {
        $goodsId = intval($request->id);
        
        if($goodsId <= 0){
            return $this->ajaxError('删除失败');
        }

        $result = (new Goods)->deleteGoodsById($goodsId);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }

    /**
    *商品文件上传
    */
    public function img(Request $request)
    {
        $disk = \Storage::disk('qiniu'); //使用七牛云上传
        $time = date('Y/m/d');
        $filename = $disk->put($time, $request->file('file'));//上传
        if(!$filename) {
            return $this->ajaxError('上传失败');
        }
        $img_url = $disk->getDriver()->downloadUrl($filename); //获取下载链接
        return $this->ajaxSuccess('上传成功', $img_url);
    }

    /**
     * 保存商品信息
     */
    public function goodsAdd(Request $request)
    {
        
        //验证
        $validator = Validator::make($request->all(), [
            'goods_name' => 'required',
        ],[
            'goods_name.required' => '商品名称不能为空',
            'goods_desc.required' => '商品内容不能为空',
        ]);

        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }

        // 验证是否存在
        $goodsId = intval($request->id);

        if(Goods::isExistName($request->goods_name, $goodsId)){
            return $this->ajaxError('商品名称已存在');
        }
        $goodsObj = new Goods;
        $data['goods_name'] = $request->goods_name;
        $data['goods_price'] = $request->goods_price;
        $data['up_time'] = $request->up_time;
        $data['hotgoods'] = $request->hotgoods;
        $data['type_id'] = $request->type_id;
        $data['goods_state'] = $request->goods_state;
        $data['goods_desc'] = trim($request->goods_desc,"<p> </p>");

        $goods_id = $goodsObj->add($data);
        if($goods_id){

            foreach ($request->goods_img as $key => $v) {
                $arr[$key]['goods_id'] = $goods_id;
                $arr[$key]['goods_img'] = $v;
            }

            $result = $goodsObj->addImg($arr);
            
            return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');

        }else{

           return $this->ajaxError('操作失败');
        }
    }

    /**
     * 检测商品名称是否存在
     */
    public function goodsExist(Request $request)
    {
        $result = true;
        $goodsId = intval($request->id);
        $checkField = $request->name;
        if($request->type == 'goods_name'){
            if(Goods::isExistName($checkField, $goodsId)){
                $result = false;
            }
        }else{
            return $this->ajaxError('非法请求');
        }
        
        return [
            'valid' => $result
        ];
    }

    /**
    * 修改页面
    */
    public function update(request $request)
    {
        $id = $request->get('id');
        $goods = new Goods;
        if ($id) {
            $data = $goods->selectGoods($id);
            $request->session()->put("goods",$data);
        } else{
            return redirect("goods");
        }
        
        return view("admin.goods.update", [
            "content_header" => "商品管理",
            "box_title" => "商品修改",
            "data"=>$data
        ]);
    }

    /**
    * 修改方法
    */
    public function updateGoods(Request $request)
    {
        $goodsNew = new Goods;
        $data = $request->all();
        $goods = $request->session()->get("goods");
        unset($data['_token']);
        if(!isset($data['goods_img'])){
            $img_url = $goods[0]['goods_img'];
        }else{
            $disk = \Storage::disk('qiniu');
            $time = date('Y/m/d-H:i:s-');
            $filename = $disk->put($time, $data['goods_img']);
            if(!$filename) {
                return back()->withErrors(['上传失败']);
            }else{
                $img_url = $disk->getDriver()->downloadUrl($filename);
            }
        }
        
        $data['goods_img'] = $img_url;
        $update = $goodsNew->updateGoods($data,$goods);
        if ($update) {
            return redirect('admin/goods');
        }
    }

    /**
    * 商品库存
    */
    public function repertory(Request $request)
    {
        $goodsId = intval($request->id);

        //获取商品信息
        $goods = Goods::findGoods($goodsId);
        $goods = json_decode(json_encode($goods), true);

        if ($request->isMethod('post')) {

            $data = $request->post();

            $file = $request->file('goods_img');
            //处理数据入货品表
            foreach ($data['value_list'] as $key => $val) {
                $arr[$key]['goods_id'] = $goodsId;
                $arr[$key]['value_list'] = substr($val, strpos($val, ',')+1);
                $arr[$key]['goods_number'] = trim($data['goods_number'][$key]);
                $arr[$key]['goods_price'] = trim($goods['goods_price']);
                $arr[$key]['cargo_price'] = intval($goods['goods_price']) + intval($data['goods_price'][$key]);
                $arr[$key]['goods_img'] = $this->upload($file[$key]);
                $arr[$key]['goods_desc'] = '这是:'.$val;
                $arr[$key]['cargo_state'] = '1';
            }

            //处理数据入商品，属性名，属性值的关联关系表
            foreach ($data['attr_value'] as $key => $v) {
                $attr_value[$key]['goods_id'] = $goodsId;
                $attr_value[$key]['attr_id'] = substr($v,0,strpos($v, ','));
                $attr_value[$key]['value_id'] = substr($v,strpos($v, ',')+1);
            }

            $result = (new Goods)->repertory($arr,$attr_value);

            if ($result) {
                return redirect('admin/goods');
            }else{
                return back()->withErrors(['添加失败']);
            }
        }else{
            //获取属性信息
            $attr = DB::table('attr')
                    ->join('attr_value','attr.attr_id','=','attr_value.attr_id')
                    ->get()
                    ->map(function($value){
                        return (array)$value;
                    })
                    ->toArray();;
            $arr = array();

            foreach ($attr as $key => $val) {
                $k = $val['attr_id'];
                $arr[$k][] = $val;
            }
//            dd($arr);
//            dd($goods);

            return view('admin.goods.repertory', [
                'content_header' => '商品管理',
                'box_title' => '添加库存',
                'goods' => $goods,
                'attr' => $arr,
            ]);
        }
    }

    /**
    * 货品文件上传
    */
    public function upload($file)
    {
        $disk = \Storage::disk('qiniu'); //使用七牛云上传
        $time = date('Y/m/d');
        $filename = $disk->put($time, $file);//上传
        if(!$filename) {
            return 'error';
        }
        $img_url = $disk->getDriver()->downloadUrl($filename); //获取下载链接
        return $img_url;
    }
}