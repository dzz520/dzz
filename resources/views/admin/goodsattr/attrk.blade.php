@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/goodsattr/attrk')}}" method="post" id="form">
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">商品属性</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="name" placeholder="请输入商品属性">
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label class="col-sm-2 control-label">所属分类</label>
                    <div class="col-sm-10">
                        <select id="images" class="form-control select2" style="width: 100%;" name="type_id">
                            @foreach($typeInfo as $val)
                                <option value="{{$val['type_id']}}">{{$val['type_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {message: '请输入商品属性'},    //非空提示 
                        regexp: {//匹配规则
                            regexp: /^[\u4e00-\u9fa5]+$/,  //正则表达式
                            message:'商品属性仅支持汉字'
                        },                    }
                },
            }
        }).on('success.form.bv', function(e) {
            // 阻止表单的默认提交
            e.preventDefault();
            var $form = $(e.target);

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/goodsattrv')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@stop