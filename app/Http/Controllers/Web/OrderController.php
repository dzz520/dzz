<?php
namespace App\Http\Controllers\Web;

use DB;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Cart;
class OrderController extends Controller
{

    /**
     * 购物车信息添加到订单
     */
    public function checkOut(Request $request)
    {   

        $orderObj = new Order;
        $orderDetailsObj = new OrderDetails;
        $cartId = $request->cartId;
        $goodsSum = 0;
        $cartGoodsInfo = $orderObj->selectCart($cartId);
        foreach ($cartGoodsInfo as $key => $val) {
            
            $goodsSum += $val->cargo_price * $val->goods_number;
        }
        return view('web.checkout',[
            'data'=>$cartGoodsInfo,
            'sum'=>$goodsSum
        ]);
        
    }

    /**
     * 地址页面、展示订单商品信息
     */

    public function accountCheckOut(Request $request)
    {   

        $orderObj = new Order;
        $orderDetailsObj = new OrderDetails;
        $goodsSum = 0;
        $orders = [];
        $orderDetails = [];
        $userId = $request->session()->get('userInfo');
        $userInfo = $request->all();
        //购物车ID
        $cartId = explode(',',$userInfo['cartId']);
        //订单地址
        $orderAddress = $userInfo['inputState'].'-'.$userInfo['inputCity'].'-'.$userInfo['inputStreet'];
        //订单编号
        $orderNumber = $orderObj->orderNum($userId);
        $cartGoodsInfo = $orderObj->selectCart($cartId);
        //订单总价
        foreach ($cartGoodsInfo as $key => $val) {
            
            $goodsSum += $val->cargo_price * $val->goods_number;
        }
        $orderYear = date('Y');
        $orderMonth = date('m');
        //季度
        $Quarter = $orderObj->orderQuarter($orderMonth);
        $result = true;
        DB::beginTransaction();
        try{
        
            $orders['order_sn'] = $orderNumber;
            $orders['user_id'] = $userId['id'];
            $orders['order_price'] = $goodsSum;
            $orders['real_price'] = $goodsSum;
            $orders['order_name'] = $userInfo['inputFirstName'];
            $orders['order_del'] = $userInfo['inputPhone'];
            $orders['order_address'] = $orderAddress;
            $orders['pay_type'] = $userInfo['pay_type'];
            
            $orderId = $orderObj->addOrders($orders,$orderYear,$Quarter);

            foreach ($cartGoodsInfo as $k => $v) {
                
                $orderDetails[$k]['goods_id'] = $v->goods_id;
                $orderDetails[$k]['goods_name'] = ''.$v->goods_name;
                $orderDetails[$k]['buy_num'] = $v->goods_number;
                $orderDetails[$k]['goods_price'] = $v->goods_price;
                $orderDetails[$k]['goods_desc'] = ''.$v->goods_desc;
                $orderDetails[$k]['sku_id'] = $v->sku_id;
                $orderDetails[$k]['order_id'] = $orderId;

            }

            $addDetails = $orderDetailsObj->addDetails($orderDetails);

            if ($addDetails) {
                
                $cartObj = new Cart;
                $deleteGoods = $cartObj->deleteGoods($cartId);

            }else{
                return $this->ajaxError('订单生成失败');
            }
            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('订单生成'.$e->getMessage());
            DB::rollBack();
        }
        
    }


}