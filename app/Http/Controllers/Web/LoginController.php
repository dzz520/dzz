<?php
namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Register;
use Validator;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $email = $request->input("email");
        $pwd = $request->input("pwd");

        if ($email == "") {
            return $this->ajaxError("用户名不能为空");
        }

        if ($pwd == "") {
            return $this->ajaxError("密码不能为空");
        }

        $data = DB::table('user')->where('email',$email)->first();

        if ($data) {
            if ($data->user_pwd == md5(md5($pwd).$data->rand_key)) {
                $data = json_decode(json_encode($data), true);
                $request->session()->put("userInfo",$data);
                return $this->ajaxSuccess("登录成功");
            } else{
                return $this->ajaxError("密码错误");
            }
        } else{
            return $this->ajaxError("邮箱不存在");
        }
    }
    /**
     * 账户个人信息
     * 退出账号
     */
    public function account(Request $request){
        //获取session
        $userInfo = $request->session()->get("userInfo");
        //判断
        if(Session::get('userInfo.id') == ''){
            return redirect('login');
        }else{
            $data = DB::table('user')
                ->where('id', $userInfo['id'])
                ->first();
            return view('web.account',['data' => $data]);
        }
    }
    public function logoutAccount(Request $request){
        //删除
        $request->session()->flash('userInfo');
        return redirect('/');
    }

    /**
     * 修改信息
     */
    public function update(Request $request){
        // 验证
        $validator = Validator::make($request->all(), [
            'user_name' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[\x7f-\xff]+$/', $value)){
                        return $fail($attribute.'名称必须是中文');
                    }
                }
            ],
            'email' => 'email',
            'mobile' => 'phone:CN'
        ],[
            'user_name.required' => '名称不能为空',
            'user_sex' => '性别不能为空',
            'email' => '请输入正确邮箱格式',
            'mobile.phone' => '请输入有效的手机号码'
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }

        // 验证是否存在
        $id = intval($request->id);
        if(Register::isExistName($request->user_name, $id)){
            return $this->ajaxError('名称已存在');
        }
        if(Register::isExistEmail($request->email, $id)){
            return $this->ajaxError('邮箱已存在');
        }
        if(Register::isExistMobile($request->mobile, $id)){
            return $this->ajaxError('手机号码已存在');
        }

        //接值
        $data['user_name'] = $request->user_name;
        $data['user_sex'] = $request->user_sex;
        $data['email'] = $request->email;
        $data['mobile'] = $request->mobile;
        //model
        $user = new Register;
        $result = $user->edit($id, $data);
        $res = DB::table('user')->where('user_name',$data['user_name'])->first();
        $data = json_decode(json_encode($res), true);
        //通过调用请求实例的 put 方法
        $request->session()->put("userInfo",$data);
        return $result ? $this->ajaxSuccess('修改成功') : $this->ajaxError('修改失败');
    }

    /**
     * 检测数据是否存在
     */
    public function check(Request $request)
    {
        $result = true;
        $id = intval($request->id);
        $checkField = $request->user_name;
        switch($request->type){
            case 'user_name':
                if(Register::isExistName($checkField, $id)){
                    $result = false;
                }
                break;
            case 'email':
                if(Register::isExistEmail($checkField, $id)){
                    $result = false;
                }
                break;
            case 'mobile':
                if(Register::isExistMobile($checkField, $id)){
                    $result = false;
                }
                break;
            default:
                return $this->ajaxError('非法请求');
        }
        return [
            'valid' => $result
        ];
    }
}
