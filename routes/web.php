
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('login', function () {
    return view('web/login');
});

Route::get('register', function () {
    return view('web/register');
});

Route::post("login_do","LoginController@index");

/**
 * 注册
 */
Route::post('register_do', 'RegisterController@registerDo');

/**
 * 账户信息
 * 账户退出
 */
Route::get('account', 'LoginController@account');
Route::get('logout_account', 'LoginController@logoutAccount');

//修改用户
Route::post('update', 'LoginController@update');

//地址
Route::get('address_add', 'AddressController@addressAdd');
Route::post('address_do', 'AddressController@addressDo');
Route::get('account_address', 'AddressController@accountAddress');
Route::post('address_del', 'AddressController@addressDel');
Route::get('address_update', 'AddressController@addressEdit');
Route::post('address_edit_do', 'AddressController@addressEditDo');

//购物车
Route::get('shoppingcart/cart', 'ShoppingCartController@cart');
Route::get('delectShoppingCartData', 'ShoppingCartController@delectShoppingCartData');
Route::get('shoppingcart/addCart', 'ShoppingCartController@addCart');

//商品展示
Route::get('product/{goods_id}', 'DetailsController@product');
Route::get('dtailsRevised', 'DetailsController@dtailsRevised');
Route::get('listing', 'DetailsController@listing');
Route::get('listing/{type_id}', 'DetailsController@listing');
Route::get('chanGe', 'DetailsController@chanGe');


/**
* 订单添加
*/
Route::get('order/checkOut', 'OrderController@checkOut');
Route::get('order/accountCheckOut', 'OrderController@accountCheckOut');

//404未找到
Route::get('404', function () {
    return view('web/404');
});