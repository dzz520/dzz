@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/goods/updateGoods')}}" method="post" id="form" enctype="multipart/form-data" files="ture">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">商品名称</label>

                    <div class="col-sm-10">
                        <input type="text" name="goods_name" class="form-control" value="<?= $data[0]['goods_name']?>">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品价格(‘元’)</label>

                    <div class="col-sm-10">
                        <input type="number" name="goods_price" class="form-control" value="<?= $data[0]['goods_price']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品封面</label>

                     <div class="col-sm-10">
                        <input type="file" name="goods_img" class="form-control">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">上架日期</label>

                    <div class="col-sm-10">
                        <input type="date" name="up_time" class="form-control">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">是否热销</label>

                    <div class="col-sm-10">
                       <?php if ($data[0]['hotgoods'] == 0) {?>
                            <input type="radio" name="hotgoods"  value="0" checked> 是
                            <input type="radio" name="hotgoods"  value="1"> 否
                       <?php } else{?>
                            <input type="radio" name="hotgoods"  value="0"> 是
                            <input type="radio" name="hotgoods"  value="1" checked> 否
                       <?php }?>
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">是否上架</label>

                    <div class="col-sm-10">
                         <?php if ($data[0]['goods_state'] == 2) {?>
                            <input type="radio" name="goods_state"  value="1"> 在售
                            <input type="radio" name="goods_state"  value="2" checked> 下架
                       <?php } else{?>
                            <input type="radio" name="goods_state"  value="1" checked> 在售
                            <input type="radio" name="goods_state"  value="2"> 下架
                       <?php }?>
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品内容</label>

                    <div class="col-sm-10">
                        <textarea name="goods_desc" class="form-control" id="lastname" cols="100%" rows="5" placeholder="请输入要修改的内容"></textarea>
                    </div>
              </div>
            <!-- /.box-body -->
            <div class="box-footer" style="width:150px;height: 100px;">
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">修改</button>
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
            </div>
        </div>
        </form>
    </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@endpush

@push('js')
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        var validator = $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                goods_name: {
                    validators: {
                        notEmpty: {message: '请输入商品名称'},    //非空提示
                        regexp: {//匹配规则
                            regexp: /^[\u4e00-\u9fa5]+$/,  //正则表达式
                            message:'商品名称仅支持汉字'
                        }
                    }
                },
                goods_desc: {
                    validators: {
                        regexp: {//匹配规则
                            regexp: /[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,  //正则表达式
                            message:'商品内容可以是中文或者字母或者数字'
                        },
                    }
                },
                goods_price: {
                    validators: {
                        notEmpty: {message: '请输入商品价格'},    //非空提示
                    }
                }
            }
        }).on('success.form.bv', function(e) {
            console.log(e);
        });
    })
</script>
@endpush