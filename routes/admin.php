<?php

// 首页
Route::get('admin/', 'AdminController@index');

//auth
Route::get('admin/login', 'AdminController@login');
Route::post('admin/login', 'AdminController@login');
Route::get('password/reset', 'AdminController@resetByEmail');
Route::get('admin/logout', 'AdminController@logout');
Route::get('admin/current_admin_info', 'AdminController@editCurrentAdminInfo');
Route::get('admin/current_admin_reset', 'AdminController@resetCurrentAdminPassword');
Route::post('admin/current_admin_reset', 'AdminController@resetCurrentAdminPassword');

// 管理员管理
Route::get('admin/admins', 'AdminController@list');
Route::get('admin/data', 'AdminController@data');
Route::get('admin/add', 'AdminController@add');
Route::get('admin/edit/{id}', 'AdminController@edit');
Route::post('admin/save', 'AdminController@save');
Route::post('admin/delete', 'AdminController@delete');
Route::post('admin/check', 'AdminController@check');

// 角色管理
Route::get('admin/roles', 'RoleController@list');
Route::get('admin/role/data', 'RoleController@data');
Route::get('admin/role/add', 'RoleController@add');
Route::get('admin/role/edit/{id}', 'RoleController@edit');
Route::post('admin/role/save', 'RoleController@save');
Route::post('admin/role/delete', 'RoleController@delete');
Route::post('admin/role/check', 'RoleController@check');
Route::get('admin/role/assign/{id}', 'RoleController@assign');
Route::post('admin/role/assign', 'RoleController@assignSave');

// 权限管理
Route::get('admin/permissions', 'PermissionController@list');
Route::get('admin/permission/data', 'PermissionController@data');
Route::get('admin/permission/add', 'PermissionController@add');
Route::get('admin/permission/edit/{id}', 'PermissionController@edit');
Route::post('admin/permission/save', 'PermissionController@save');
Route::post('admin/permission/delete', 'PermissionController@delete');
Route::post('admin/permission/check', 'PermissionController@check');

//用户管理
Route::get('admin/users', 'UsersController@user_list');
Route::get('admin/users/data', 'UsersController@data');
Route::get('admin/users/add', 'UsersController@add');
Route::get('admin/users/eint', 'UsersController@eint');
Route::get('admin/users/delete', 'UsersController@delete');

//商品管理
Route::get('admin/goods', 'GoodsController@list');
Route::get('admin/goods/data', 'GoodsController@data');
Route::post('admin/goods/delete', 'GoodsController@delete');
Route::get('admin/goods/update', 'GoodsController@update');
Route::post('admin/goods/updateGoods', 'GoodsController@updateGoods');
Route::post('admin/goods/goodsExist', 'GoodsController@goodsExist');
Route::post('admin/goods/goodsUpdate', 'GoodsController@goodsUpdate');
Route::get('admin/goods/index', 'GoodsController@index');
Route::post('admin/goods/goodsadd', 'GoodsController@goodsAdd');
Route::get('admin/goods/repertory', 'GoodsController@repertory');
Route::post('admin/goods/repertory', 'GoodsController@repertory');
Route::post('admin/goods/img', 'GoodsController@img');
Route::get('admin/goods/sales', 'GoodsController@salesRank');

//添加商品属性
Route::get('admin/goodsattrk', 'GoodsAttrController@attrk');
Route::get('admin/goodsattrv', 'GoodsAttrController@attrv');
Route::post('admin/goodsattr/attrk', 'GoodsAttrController@addAttrk');
Route::post('admin/goodsattr/attrv', 'GoodsAttrController@addAttrv');
Route::post('admin/goodsattr/isExistName', 'GoodsAttrController@isExistName');

//添加分类
Route::get('admin/categorys/index', 'CategorysController@index');
Route::post('admin/categorys/addtype', 'CategorysController@addType');
Route::get('admin/categorys', 'CategorysController@list_type');
Route::get('admin/categorys/data', 'CategorysController@data');