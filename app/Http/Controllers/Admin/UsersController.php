<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * [user_list description]
     * 用户列表的展示
     */
    public function user_list()
    {
        return view('admin.users.user_list', [
            'content_header' => '用户管理',
            'box_title' => '用户列表'
        ]);
    }
    
    /*
      用户数据的展示
     */
    public function data()
    {
        $query = DB::table('user')->where('user_img','!=','3')->get();
        $data = datatables()->of($query)
         ->editColumn('is_freeze', function($data){
                return $data->is_freeze = $data->is_freeze ? '可用' : '冻结';
        })
         ->toJson();
        return $data;
    }

    public function eint(Request $request)
    {
        $id = $request->id;

        $status = $request->status;
        
        $userObj = new Users;

        $data = $userObj->saveUpdate($id,$status);
        
        if($data){
            $this->ajaxSuccess(array("message"=>"修改成功","state"=>1));
        }else{
            $this->ajaxError(array("messagn"=>"修改失败","state"=>0));

        }
    }

    public function delete(Request $request)
    {
        $Id = intval($request->id);
        //print_r($Id);die;
        if($Id <= 0){
            return $this->ajaxError('删除失败');
        }

        $result = (new Users)->ConfirmDeletion($Id);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }
}
