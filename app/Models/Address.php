<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Session;
use DB;
use Log;
class address extends Model
{
    protected $table = 'address';

    //三级联动数据
    public function addInfo($id)
    {
        return DB::table('region')->where('parent_id',$id)->get();
    }

    public function addDo()
    {
        return DB::table('region')->where('parent_id',0)->get();
    }

    public function rigin($id)
    {
        return DB::table('region')->where('region_id',$id)->get(['region_name'])->map(function ($value) {
            return (array)$value;
        })->toArray();
    }

    /**
     * 默认地址
     */
    public function isDefault($userId)
    {
        return DB::table('address')
                ->where('user_id',$userId)
                ->update([
                    'is_default'=>2,
                ]);
    }

    /**
     * 添加地址
     */
    public function add($data)
    {
        foreach($data as $field => $value){
                $this->$field = $value;
        }
        $result = true;
        try{
            $this->save();
        }catch(\Exception $e){
            $result = false;
            Log::error('address:add '.$e->getMessage());
        }
        return $result;
    }

    //展示
    public function addressShow($userId)
    {
        return DB::table('address')->where('user_id',$userId)->get();
    }

    //检测地址是否存在
    public static function isExistAddress($address, $addressId = null)
    {
        $query = self::where('hcity', $address);
        if($addressId){
            $result = $query->where('address_id', '!=', $addressId);
        }
        $result = $query->first();
        return $result ? true : false;
    }
    
    /**
     * 删除地址
     */
    public function del($address_id){
        return DB::table('address')->where('address_id',$address_id)->delete();
    }
    /**
     * 修改地址
     */
    public function edit($address_id, $data)
    {
        return DB::table('address')->where('address_id',$address_id)->update($data);
    }
}