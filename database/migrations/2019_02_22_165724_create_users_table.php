<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name', 10)->comment('用户名');
            $table->char('user_pwd', 32)->comment('用户密码');
            $table->char('rand_key', 8)->comment('随机字符串');
            $table->string('user_sex', 10)->comment('用户性别')->nullable();
            $table->string('user_img', 100)->comment('用户头像')->nullable();
            $table->tinyInteger('is_freeze')->default(0)->comment('账号是否冻结');
            $table->string('email', 60)->default('')->comment('邮箱');
            $table->char('mobile', 11)->default('')->comment('手机号')->nullable();
            $table->datetime('last_login_time')->default('0000-01-01 00:00:00')->comment('最后一次登录时间');
            $table->timestamps();
        });
     }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
