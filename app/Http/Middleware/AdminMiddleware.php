<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Models\Admins;
use App\Models\Permission;

class AdminMiddleware
{

    protected $except = [
        'admin/login',
        'admin/reset',
        'admin/logout',
        'admin/current_admin_info',
        'admin/current_admin_reset'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $controllerAction = $this->getCurrentControllerAction($request);

        $admin = Admins::currentAdmin();
        if(!in_array($controllerAction, $this->except)){
            if(!$admin){
                return redirect('admin/login')->withErrors(['message' => '请先登录'], 'prompt');
            }

            if($admin->is_super == 1 || $controllerAction =='admin/index'){
                return $next($request);
            }

            $permissionName = Permission::getPermissionNameByControllerAction($controllerAction);
            if(empty($permissionName)){
                return back()->withErrors(['message' => $request->path().' 未设置权限', 'status' => 'error'], 'prompt');
            }
            if(!$admin->hasPermissionTo($permissionName)){
                return back()->withErrors(['message' => '权限不足', 'status' => 'error'], 'prompt');
            }
        }
        
        return $next($request);
    }

    private function getCurrentControllerAction($request)
    {
        $controller = $request->route()->getActionName();
        $controller = str_replace('\\', '/', $controller);
        preg_match("/Admin\/(.*)Controller/", $controller, $mnt);
        $controller = strtolower($mnt[1]);
        $action = strtolower($request->route()->getActionMethod());

        return $controller.'/'.$action;
    }
}
