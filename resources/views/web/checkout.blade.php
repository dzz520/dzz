@extends('web.layouts.common')
@section('common')

<div class="breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Checkout</li>
                </ul>
            </div>
        </div>
        <!-- Content -->
        <div id="pageContent">
            <div class="container offset-18">
                <h1 class="block-title large">查看</h1>
                <div class="row offset-48">
                    <div class="col-md-8">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs checkout-tab">
                            <li class="li-one active">
                                <a href="#Shipping" data-toggle="tab">
                                    <div class="numeral-box">
                                        <span class="numeral">1</span>
                                        <span class="icon icon-check"></span>
                                    </div>
                                    <div class="title">
                                        运输
                                    </div>
                                </a>
                            </li>
                            <li class="li-two">
                                <a href="#Review" data-toggle="tab">
                                    <div class="numeral-box">
                                        <span class="numeral">2</span>
                                    </div>
                                    <div class="title">
                                        审核和运输
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content checkout-tab-content">
                            <div class="tab-pane active" id="Shipping">
                                <h2 class="title-checkout">收件地址</h2>
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputFirstName" class="col-sm-3 control-label">名字: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputFirstName">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputStreet" class="col-sm-3 control-label">街道: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputStreet">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCity" class="col-sm-3 control-label">城市: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputCity">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputState" class="col-sm-3 control-label">省: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputState">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone" class="col-sm-3 control-label">电话: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputPhone">
                                        </div>
                                    </div>
                                </form>
                                <hr>
                                <h2 class="title-checkout">运输方法</h2>
                                <table class="checkout-table-01">
                                    <tbody>
                                        <tr>
                                            <td>运输方法:</td>
                                            <td>
                                                <!-- <label class="radio">
                                                <input id="radio1" type="radio" name="radios" value="">送货上门

                                                <span class="outer"><span class="inner"></span></span>
                                                </label> -->
                                                <select name="" id="">
                                                    <option value="1">空投快递</option>
                                                    <option value="2">陆地运输</option>
                                                    <option value="2">上门自取</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <!-- <tr>
                                            <td>价钱:</td>
                                            <td>
                                                <span class="color-base">100</span>
                                            </td>
                                        </tr> -->
                                        <tr>
                                            <td>配送商名称:</td>
                                            <td>索马里配送商</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <a class="btn icon-btn-right" href="#Review" data-toggle="tab">下一步<span class="icon icon-chevron_right"></span></a>
                            </div>
                            <div class="tab-pane checkout-tab-content" id="Review">
                                <h2 class="title-checkout">付款</h2>
                                <div class="checkout-box-01">
                                    <h6>支付方式</h6>
                                    <!-- <label class="radio">
                                    <input id="radio2" type="radio" name="radios">
                                    <span class="outer"><span class="inner"></span></span>
                                    我的账单和送货资质是相同的
                                    </label> -->
                                    <select name="pay_type" id="pay_type">
                                        <option value="1">在线支付</option>
                                        <option value="2">货到付款</option>
                                    </select>
                                </div>
                                <hr>
                                <h2 class="title-checkout">申请折扣码</h2>
                                <div class="checkout-box-02">
                                    <form class="form-inline">
                                        <input type="text" class="form-control">
                                        <a class="btn btn-border color-default" href="#">申请订购</a>
                                    </form>
                                </div>
                                <a class="btn" href="javascript:void(0)" id="addorder">请订购</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 checkout-box-aside ">
                        <div class="checkout-box">
                            <div class="checkout-table-02">
                                <table>
                                    <thead>
                                        <tr>
                                            <td colspan="2">订单摘要</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>运费:</td>
                                            <td>免邮费</td>
                                        </tr>
                                        <tr>
                                            <td>订单总价:</td>
                                            
                                            <td>
                                                <strong class="color-base">
                                                    {{$sum}}
                                                </strong>
                                            </td>
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="checkout-box">
                            <h3>订单商品信息</h3>
                            <div class="checkout-box-03">
                                @foreach ($data as $val)
                                <div class="item">
                                    <div class="img cartId"  value="{{$val->id}}">
                                        <a href="#"><img src="{{$val->goods_img}}" alt=""></a>
                                    </div>
                                    <div class="description">
                                        <a href="#" class="title">{{$val->goods_name}}</a>
                                        <p>
                                            {{$val->attr_name}}
                                        </p>
                                        <div class="price">${{$val->cargo_price}}</div>
                                        <p>
                                            {{$val->goods_number}}
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="checkout-box">
                            <div class="button-edit">
                                <span class="icon icon-create"></span>
                            </div>
                            <h3>运送到:</h3>
                            <div class="checkout-box-content">
                                <p>
                                    一切按照客户要求，不管是身处什么地理位置，只要在中国，
                                    我们的服务团队，就会送货上门。
                                </p>
                            </div>
                        </div>
                        <div class="checkout-box">
                            <div class="button-edit">
                                <span class="icon icon-create"></span>
                            </div>
                            <h3>邮寄方式:</h3>
                            <div class="checkout-box-content">
                                <p>送货上门</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('vendor/weblte/external/jquery/jquery-2.1.4.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/countdown/jquery.plugin.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/countdown/jquery.countdown.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/isotope/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/slick/slick.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/instafeed/instafeed.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/elevatezoom/jquery.elevatezoom.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/external/panelmenu/panelmenu.js')}}"></script>
        <script src="{{asset('vendor/weblte/js/main.js')}}"></script>
        <script>
            $('.numeral-bo').click(function(){
                $('.li-one').removeClass('active')
                $('.li-two').addClass('active')
            });
        </script>
    </body>
</html>
<script type="text/javascript">
    $(document).on('click','#addorder',function(){
        var cartId = '';
        //名称
        var inputFirstName = $("#inputFirstName").val();
        //街道
        var inputStreet = $("#inputStreet").val();
        //城市
        var inputCity = $("#inputCity").val();
        //省
        var inputState = $("#inputState").val();
        //电话
        var inputPhone = $("#inputPhone").val();
        //配送方式
        var pay_type = $("#pay_type").val();
        $('.cartId').each(function (i) { 
                cartId += ',' +$(this).attr('value');
         });
        cartId =cartId.substr(1);
        $.ajax({
            type:'get',
            url:"{{url('order/accountCheckOut')}}",
            data:{cartId:cartId,inputFirstName:inputFirstName,inputStreet:inputStreet,inputCity:inputCity,inputState:inputState,inputPhone:inputPhone,pay_type:pay_type},
            dataType:"json",
            success:function(e){
                
            }
        })
    })
</script>
@endsection