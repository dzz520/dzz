<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as BasePermission;
use DB;
use Log;

class Permission extends BasePermission
{
    /**
     * 获取所有的父级权限
     */
    public function getParentPermissions()
    {
    	return $this->where('pid', 0)->get();
    }

    /**
     * 获取所有权限
     * return [
     *     'id' => [
     *        'name' => 'xxx',
     *          'sons' => [
     *              '0' => [
     *                  'id' => n,
     *                  'name' => 'xxx',
     *              ],
     *              ...
     *           ]
     *      ]
     *      ...
     * ]
     */
    public static function getAllPermissionDatas($role)
    {
        $result = [];
        $permissions = self::orderBy('pid', 'asc')->get();

        foreach($permissions as $permission){
            $info['id'] = $permission['id'];
            $info['name'] = $permission['name'];
            $info['cn_name'] = $permission['cn_name'];
            $info['is_has'] = $role->hasPermissionTo($permission->name) ? 1 : 0;
            if($permission['pid'] == 0){
                $info['sons'] = [];
                $result[$permission['id']] = $info;
            }else{
                $result[$permission['pid']]['sons'][] = $info;
            }
        }

        return $result;
    }

    /**
	 * 检测名字是否存在
	 */
    public static function isExsitName($permissionName, $permissionId = 0)
    {
        $query = self::where('name', $permissionName);
        if($permissionId){
            $result = $query->where('id', '!=', $permissionId);
        }
        $result = $query->first();
        
        return $result ? true : false;
    }

    /**
     * 检测中文名是否存在
     */
    public static function isExsitCnName($permissionCnName, $permissionId = 0)
    {
        $query = self::where('cn_name', $permissionCnName);
        if($permissionId){
            $result = $query->where('id', '!=', $permissionId);
        }
        $result = $query->first();
        
        return $result ? true : false;
    }

    /**
     * 判断 控制器/方法 是否存在
     */
    public static function isExsitControllerAction($permissionId = 0, $pid, $controllerAction)
    {
    	if($pid == 0){
    		return $controllerAction == '/' ? false : true;
    	}

        $query = self::where('controller_action', $controllerAction);
        if($permissionId){
            $result = $query->where('id', '!=', $permissionId);
        }
        $result = $query->first();
        
        return $result ? true : false;
    }

    /**
     * 添加权限
     */
    public function add($data)
    {
    	foreach($data as $field => $value){
    		$this->$field = $value;
    	}
    	$this->save();

    	if($data['pid'] == 0){
    		$this->path = $this->id;
    	}else{
    		$this->path = $this->pid.'-'.$this->id;
    	}
    	

    	return $this->save();
    }

    /**
     * 编辑权限
     */
    public function edit($permissionId, $data)
    {
    	$premission = $this->find($permissionId);
    	foreach($data as $field => $value){
    		$premission->$field = $value;
    	}

    	if($premission->pid == 0){
    		$premission->path = $premission->id;
    	}else{
    		$premission->path = $premission->pid.'-'.$premission->id;
    	}
    	
    	return $premission->save();
    }

    /**
     * 删除权限
     */
    public function deletePermissionById($permissionId)
    {
    	$result = true;
        DB::beginTransaction();
        try{
            $permission = $this->findById($permissionId);
            $permission->syncRoles();
            $permission->delete();

            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('permission:delete '.$e->getMessage());
            DB::rollBack();
        }
        
        return $result;
    }

    public static function getPermissionNameByControllerAction($controllerAction)
    {
        return self::where('controller_action', 'like', '%'.$controllerAction.'%')->value('name');
    }
}
