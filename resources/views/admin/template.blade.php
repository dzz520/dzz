@extends('adminlte::page')

@section('title', 'IT稷下学宫-管理后台')

@section('content_header')
    <h1>{{$content_header}}</h1>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/toast.min.css')}}" />
@endpush

@push('js')
<script src="{{asset('js/toast.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });

    // 设置bootstrapValidator框提示样式
    var icon = {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    };

    // 设置默认全局弹框的显示位置和存在时间
    $toast.config({
        position:'center center',
        duration: 3000
    })

    var prompt_status = "{{$errors->prompt->first('status')}}";
    if(prompt_status == 'error'){
        $toast.error("{{$errors->prompt->first('message')}}");
    }
    if(prompt_status == 'success'){
        $toast.success("{{$errors->prompt->first('message')}}");
    }
</script>
@endpush