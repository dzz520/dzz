<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use Validator;
use Session;

class RoleController extends Controller
{
    /**
     * 角色列表
     */
    public function list()
    {
    	return view('admin.role.list', [
            'content_header' => '角色管理',
            'box_title' => '角色列表'
        ]);
    }

    /**
     * 获取角色数据
     */
    public function data()
    {
    	$data = datatables()->of(Role::all())->toJson();

        return $data;
    }

    /**
     * 添加角色
     */
    public function add()
    {
        return view('admin.role.edit', [
            'content_header' => '角色管理',
            'box_title' => '添加角色'
        ]);
    }

    /**
     * 编辑角色
     */
    public function edit(Request $request)
    {
        $roleId = intval($request->id);
        if($roleId <= 0){
            return back();
        }
        $role = Role::findById($roleId);

        return view('admin.role.edit', [
            'content_header' => '角色管理',
            'box_title' => '编辑角色',
            'role' => $role
        ]);
    }

    /**
     * 检测角色名称
     */
    public function check(Request $request)
    {
        $result = true;
        $roleId = intval($request->id);

        switch($request->type){
            case 'name':
                $name = $request->name;
                if(Role::isExsitName($name, $roleId)){
                    $result = false;
                }
                break;
            case 'cn_name':
                $cnName = $request->cn_name;
                if(Role::isExsitCnName($cnName, $roleId)){
                    $result = false;
                }
                break;
            default:
                return $this->ajaxError('非法请求');
        }
        
        return [
            'valid' => $result
        ];
    }

    /**
     * 检测角色中文名称是否存在
     */
    public function checkCnName(Request $request)
    {
        $roleId = intval($request->id);
        $cnName = $request->cn_name;

        return [
            'valid' =>  Role::isExsitCnName($cnName, $roleId) ? false : true
        ];
    }

    /**
     * 保存角色信息
     */
    public function save(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'name' => 'required|alpha_dash',
            'cn_name' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[\x7f-\xff]+$/', $value)){
                        return $fail($attribute.'必须是中文');
                    }
                }
            ]
        ],[
            'name.required' => '角色英文名称不能为空',
            'name.alpha_dash' => '角色英文名称必须是小写字母和下划线的组合',
            'cn_name.required' => '角色中文名称不能为空',
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }

        // 验证是否存在
        $roleId = intval($request->id);
        if(Role::isExsitName($request->name, $roleId)){
            return $this->ajaxError('角色英文名称已存在');
        }
        if(Role::isExsitCnName($request->cn_name, $roleId)){
            return $this->ajaxError('角色中文名称已存在');
        }

        $roleObj = new Role;
        $data['name'] = $request->name;
        $data['cn_name'] = $request->cn_name;
        $result = $roleId > 0 ? $roleObj->edit($roleId, $data) : $roleObj->add($data);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }

    /**
     * 分配权限页面
     */
    public function assign(Request $request)
    {
        $roleId = intval($request->id);
        if($roleId <= 0){
            return back()->withErrors(['message' => '非法请求'], 'prompt');
        }

        $role = Role::findById($roleId);
        $permissions = Permission::getAllPermissionDatas($role);

        return view('admin.role.assign', [
            'content_header' => '角色管理',
            'box_title' => '分配权限',
            'role' => $role,
            'permissions' => $permissions,
            'success' => Session::get('assign_success')
        ]);
    }

    /**
     * 分配权限
     */
    public function assignSave(Request $request)
    {
        $roleId = intval($request->id);
        if($roleId <= 0 || !is_array($request->permissions)){
            return back()->withErrors(['message' => '非法请求'], 'prompt');
        }

        $role = Role::findById($roleId);
        $role->syncPermissions($request->permissions);

        return back()->with('assign_success', '分配成功');
    }

    /**
     * 删除角色
     */
    public function delete(Request $request)
    {
        $roleId = intval($request->id);
        if($roleId <= 0){
            return $this->ajaxError('删除失败');
        }

        $result = (new Role)->deleteRoleById($roleId);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }
}
