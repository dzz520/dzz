@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/save')}}" method="post" id="form">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="{{isset($admin) ? $admin['id'] : '0'}}">
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">管理员昵称</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" id="name" placeholder="请输入管理员昵称，范围在4-10个字符或2-10个汉字" value="{{isset($admin) ? $admin['name'] : ''}}">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">管理员邮箱</label>

                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" id="email" placeholder="请输入管理员邮箱" value="{{isset($admin) ? $admin['email'] : ''}}">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="mobile" class="col-sm-2 control-label" style="color: #333">管理员手机</label>

                    <div class="col-sm-10">
                        <input type="text" name="mobile" class="form-control" id="mobile" placeholder="请输入管理员手机号" value="{{isset($admin) ? $admin['mobile'] : ''}}">
                    </div>
                </div>
                @if(isset($roles))
                <div class="form-group" style="height: 55px">
                    <label class="col-sm-2 control-label">选择角色</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="role">
                            @foreach($roles as $role)
                            <option value="{{$role['name']}}" {{isset($admin) && $admin['role'] == $role['name'] ? 'selected' : ''}}>{{$role['cn_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {message: '请输入用户名'},    //非空提示
                        stringLength: {    //长度限制
                            min: 4,
                            max: 10,
                            message: '用户名长度必须在4到10之间'
                        }, 
                        regexp: {//匹配规则
                            regexp: /^[a-zA-Z0-9_\\u4e00-\\u9fa5]+$/,  //正则表达式
                            message:'用户名仅支持汉字、字母、数字、下划线的组合'
                        },
                        remote: { //ajax校验，获得一个json数据（{'valid': true or false}）
                            url: '{{url("admin/check")}}',                  //验证地址
                            message: '用户已存在',   //提示信息
                            type: 'POST',                   //请求方式
                            data: function(validator){  //自定义提交数据，默认为当前input name值
                                return {
                                    id: $("#id").val(),
                                    name: $("input[name='name']").val(),
                                    type: 'name'
                                }
                            }
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: '请输入邮箱'
                        },
                        emailAddress: {
                            message: '请输入正确的邮箱'
                        },
                        remote: {
                            url: '{{url("admin/check")}}',
                            message: '邮箱已存在',
                            type: 'post',
                            data: function(validator){  //自定义提交数据，默认为当前input name值
                                return {
                                    id: $("#id").val(),
                                    name: $("input[name='email']").val(),
                                    type: 'email'
                                }
                            }
                        }
                    }
                },
                mobile: {
                    validators: {
                        notEmpty: {
                            message: '请输入手机号码'
                        },
                        phone: {
                            country: 'CN',
                            message: '请输入正确的手机号码'
                        },
                        remote: {
                            url: '{{url("admin/check")}}',
                            message: '手机号码已存在',
                            type: 'post',
                            data: function(validator){  //自定义提交数据，默认为当前input name值
                                return {
                                    id: $("#id").val(),
                                    name: $("input[name='mobile']").val(),
                                    type: 'mobile'
                                }
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv', function(e) {
            // 阻止表单的默认提交
            e.preventDefault();
            var $form = $(e.target);

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/admins')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@stop