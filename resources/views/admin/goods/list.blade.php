@extends('admin.template')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{$box_title}}</h3>
        </div>
        <div>
            <a href="{{url('admin/goods/index')}}" class="btn btn-info btn-flat" style="margin-left: 10px">添加商品</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="clear: both">
            <table id="table" class="table table-bordered table-striped"></table>
        </div>
    </div>
@stop

@section('js')
    @parent
    <script src="{{asset('js/layer/layer.js')}}"></script>
    <script>
        $(function(){
            var table = $('#table').DataTable({
                processing: true,
                serverSide: true,
                "oLanguage": {
                    "sLengthMenu": "每页显示 _MENU_ 条记录",
                    "sZeroRecords": "抱歉， 没有找到",
                    "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                    "sInfoEmpty": "没有数据",
                    "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                    "oPaginate": {
                        "sFirst": "首页",
                        "sPrevious": "前一页",
                        "sNext": "后一页",
                        "sLast": "尾页"
                    },
                    "sZeroRecords": "没有检索到数据",
                },
                ordering: false,
                ajax: "{{url('admin/goods/data')}}",
                columns: [
                    {data: 'id'},
                    {data: 'goods_name'},
                    {data: 'goods_price'},
                    {data: 'goods_desc'},
                    {data: 'goods_img',
                        render: function (data, type, now) {
                            return '<img src="'+now.goods_img+'" style="width:50px; height:50px;">'
                        }
                    },
                    {data: 'up_time'},
                    {data: 'hotgoods'},
                    {data: 'goods_state'},
                ],
                columnDefs: [
                    {title: 'ID', targets: 0},
                    {title: '商品名称', targets: 1},
                    {title: '商品价格/(元)', targets: 2},
                    {title: '商品内容', targets: 3},
                    {title: '商品图片', targets: 4},
                    {title: '上架日期', targets: 5},
                    {title: '热销商品', targets: 6},
                    {title: '商品状态', targets: 7},
                    {
                        title: '操作',
                        targets: 8,
                        render: function(data, type ,row){
                            return '\
                                    <a href="{{url("admin/goods/update")}}?id='+row.id+'" class="btn btn-sm btn-info btn-flat">编辑</a> | <a href="{{url("admin/goods/repertory")}}?id='+row.id+'" class="btn btn-sm btn-info btn-flat">库存</a> | \
                                <a href="javascript:;" class="btn btn-sm btn-flat btn-danger" id="delete" data-id="'+row.id+'" >删除</a> \
                                </span>';
                        }
                    },
                ]
            });

            $('#table').on('click', '#delete', function(){
                var id = $(this).data('id');
                layer.msg('确定要删除该条记录？', {
                    time: 0, //不自动关闭
                    btn: ['确定', '取消'],
                    yes: function(index){
                        $.post("{{url('admin/goods/delete')}}", {id: id}, function(result){
                            if(result.status == 'success'){
                                $toast.success(result.message);
                                table.ajax.reload();
                                layer.close(index);
                            }else{
                                $toast.error(result.message);
                            }
                        }, 'json')
                    }
                });
            })
        })
    </script>
@stop
