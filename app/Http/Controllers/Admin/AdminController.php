<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Admins;
use Session;
use Datatables;
use Carbon\Carbon;
use App\Models\Role;

class AdminController extends Controller
{
    /**
     * 首页
     */
    public function index()
    {
        return view('admin.home', [
            'content_header' => '',
            'box_title' => '管理员列表'
        ]);
    }

    /**
     * 登录
     */
    public function login(Request $request)
    {
        if (Session::get('admin_info')) {
            return redirect('admin');
        }
        
        if ($request->isMethod('post')) {

            // 验证邮箱
            $validator = Validator::make($request->all(), [
                'email' => 'email',
            ],[
                'email.email' => '请输入有效的邮箱'
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator);
            }

            $adminObj = new Admins;
            $adminInfo = $adminObj->getAdminInfoByEmail($request->email);
            if($adminInfo->password != md5(md5($request->password).$adminInfo->key)){
                return back()->withErrors(['message' => '用户名或密码错误'], 'prompt');
            }
            $adminInfo->last_login_time = Carbon::now();
            $adminInfo->save();
            Session::put('admin_info', $adminInfo);

            return redirect('admin/');
        }
        return view('admin.auth.login');
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        Session::forget('admin_info');

        return redirect('admin/login');
    }

    /**
     * 通过邮箱重置密码
     */
    public function resetByEmail()
    {
        return view('admin.auth.passwords.email');
    }

    /**
     * 管理员列表页面
     */
    public function list()
    {
        return view('admin.admin.list', [
            'content_header' => '管理员管理',
            'box_title' => '管理员列表'
        ]);
    }

    /**
     * 修改当前登录用户信息
     */
    public function editCurrentAdminInfo()
    {
        $adminObj = new Admins;
        $adminInfo = $adminObj->find(Admins::currentAdmin()->id);

        return view('admin.admin.edit', [
            'content_header' => '账号中心',
            'box_title' => '个人信息',
            'admin' => $adminInfo
        ]);
    }

    /**
     * 重置当前登录用户密码
     */
    public function resetCurrentAdminPassword(Request $request)
    {
        if($request->isMethod('post')){
            // 验证
            $validator = Validator::make($request->all(), [
                'old_password' => 'required|alpha_dash|between:6,15',
                'new_password' => 'required|alpha_dash|between:6,15',
                'confirm_password' => 'required|alpha_dash|between:6,15',

            ],[
                'old_password.required' => '原始密码不能为空',
                'old_password.between' => '原始密码必须为:min - :max位数字、字符和_组成',
                'old_password.alpha_dash' => '原始密码只能由数字、字符和_组成',
                'new_password.required' => '新密码不能为空',
                'new_password.between' => '新密码必须为:min - :max位数字、字符和_组成',
                'new_password.alpha_dash' => '新密码只能由数字、字符和_组成',
                'confirm_password.required' => '确认密码不能为空',
                'confirm_password.between' => '确认密码必须为:min - :max位数字、字符和_组成',
                'confirm_password.alpha_dash' => '确认密码只能由数字、字符和_组成',
            ]);
            if ($validator->fails()) {
                return $this->ajaxError($validator->errors()->first());
            }
            if($request->new_password != $request->confirm_password){
                return $this->ajaxError('新密码和确认密码不一致');
            }

            $admin = Admins::currentAdmin();
            if($admin->password != md5(md5($request->old_password).$admin->key)){
                return $this->ajaxError('旧密码输入错误');
            }
            $admin->password = md5(md5($request->new_password).$admin->key);
            $admin->save();

            return $this->ajaxSuccess('修改密码成功');
        }
        
        return view('admin.admin.reset', [
            'content_header' => '账号中心',
            'box_title' => '重置密码'
        ]);
    }

    /**
     * 添加管理员
     */
    public function add()
    {
        $roles = Role::all();

        return view('admin.admin.edit', [
            'content_header' => '管理员管理',
            'box_title' => '添加管理员',
            'roles' => $roles
        ]);
    }

    /**
     * 编辑管理员
     */
    public function edit(Request $request)
    {
        $adminId = intval($request->id);
        if($adminId <= 0){
            return back()->withErrors(['message' => '非法请求', 'prompt']);
        }

        $roles = Role::all();
        $adminObj = new Admins;
        $adminInfo = $adminObj->find($adminId);
        $adminInfo->role = $adminObj->getAdminRole($adminInfo);

        return view('admin.admin.edit', [
            'content_header' => '管理员管理',
            'box_title' => '编辑管理员',
            'admin' => $adminInfo,
            'roles' => $roles
        ]);
    }

    /**
     * 保存管理员信息
     */
    public function save(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'name' => 'required|between:4,10',
            'email' => 'email',
            'mobile' => 'phone:CN'

        ],[
            'name.required' => '管理员昵称不能为空',
            'name.between' => '管理名昵称必须为:min - :max位数字、字符和_组成',
            'email.email' => '请输入有效的邮箱',
            'mobile.phone' => '请输入有效的手机号码'
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }

        if($request->has('role') && empty(Role::findByName($request->role))){
            return $this->ajaxError('角色不存在');
        }

        // 验证是否存在
        $adminId = intval($request->id);
        if(Admins::isExistName($request->name, $adminId)){
            return $this->ajaxError('管理员昵称已存在');
        }
        if(Admins::isExistEmail($request->email, $adminId)){
            return $this->ajaxError('邮箱已存在');
        }
        if(Admins::isExistMobile($request->mobile, $adminId)){
            return $this->ajaxError('手机号码已存在');
        }

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['mobile'] = $request->mobile;
        if($request->has('role')){
            $data['role'] = $request->role;
        }
        $adminObj = new Admins;
        $result = $adminId > 0 ? $adminObj->edit($adminId, $data) : $adminObj->add($data);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }

    /**
     * 获取管理员列表数据
     */
    public function data(Request $request)
    {
        $query = Admins::all();

        $data = datatables()->of($query)
            ->editColumn('is_freeze', function($data){
                return $data->is_freeze = $data->is_freeze ? '是' : '否';
            })
            ->addColumn('role_name', function($data){
                if($data->is_super == 1){
                    return '超级管理员';
                }
                $role = $data->getAdminRole($data);

                return Role::findByName($role)->cn_name;
            })
            ->removeColumn('key')
            ->removeColumn('password')
            ->toJson();

        return $data;
    }

    /**
     * 检测数据是否存在
     */
    public function check(Request $request)
    {
        $result = true;
        $adminId = intval($request->id);
        $checkField = $request->name;
        switch($request->type){
            case 'name':
                if(Admins::isExistName($checkField, $adminId)){
                    $result = false;
                }
                break;
            case 'email':
                if(Admins::isExistEmail($checkField, $adminId)){
                    $result = false;
                }
                break;
            case 'mobile':
                if(Admins::isExistMobile($checkField, $adminId)){
                    $result = false;
                }
                break;
            default:
                return $this->ajaxError('非法请求');
        }

        return [
            'valid' => $result
        ];
    }

    /**
     * 删除管理员
     */
    public function delete(Request $request)
    {
        $adminId = intval($request->id);
        if($adminId <= 0){
            return $this->ajaxError('删除失败');
        }

        $result = (new Admins)->deleteAdminById($adminId);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }
}








