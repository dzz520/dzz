@extends('web.layouts.common')
@section('common')
    <div class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="index.html">家</a></li>
                <li>创建一个账户</li>
            </ul>
        </div>
    </div>
    <!-- Content -->
    <div id="pageContent">
        <div class="container offset-14">
            <h1 class="block-title large">创建一个账户</h1>
            <div class="row">
                <div class="col-sm-8 col-sm-push-2 col-lg-6 col-lg-push-3">
                    <div class="login-form-box">
                        <h2 class="text-uppercase">个人信息</h2>
                        <form action="register_do" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="icon icon-person_outline"></span>
                                    </span>
                                    <input type="text" name="user_name" id="LoginFormName1" class="form-control" placeholder="名称">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="icon icon-email"></span>
                                    </span>
                                    <input type="email" name="email" id="LoginEmail" class="form-control" placeholder="电子邮件">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="icon icon-lock_outline"></span>
                                    </span>
                                    <input type="password" name="user_pwd" id="LoginFormPass1" class="form-control" placeholder="密码">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="button-block">
                                        <button type="button" class="btn" id="registerFrom">创建一个账户</button>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="additional-links-01">or <a href="#">返回商店</a></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="external/jquery/jquery-2.1.4.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
<script>
    $(document).on("blur","#LoginFormName1",function () {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var userName = $(this).val();
        if(userName==''){
            swal('名称不能为空');
            return false;
        }
    });
    //邮箱验证
    $(document).on("blur","#LoginEmail",function () {
        var userEmail = $(this).val();
        var patrn = /^([0-9A-Za-z\-_\.]+)@([0-9A-Za-z]+\.[A-Za-z]{2,3}(\.[A-Za-z]{2})?)$/g;
        if (!patrn.exec(userEmail)){
            swal('邮箱格式不正确');
            return false;
        }
    });
    //密码验证
    $(document).on("blur","#LoginFormPass1",function () {
        var userPwd = $(this).val();
        var patrn=/^\w{6,}$/;
        if (!patrn.exec(userPwd)){
            swal('密码长度不能少于六位');
            return false;
        }
    });
    $(document).on("click","#registerFrom",function(){
        var userName = $("#LoginFormName1").val();
        var userEmail = $("#LoginEmail").val();
        var userPwd = $("#LoginFormPass1").val();
        if(userName=='' && userRealName=='' && userEmail=='' && userPwd==''){
            swal('请填写信息');
            return false;
        }
        $.ajax({
            type:'post',
            url:"/register_do",
            data:{user_name:userName,email:userEmail,user_pwd:userPwd},
            dataType:"json",
            success:function(e){
                swal(e.message);
                if(e.status=="success"){
                    window.location.href="/";
                }
            }
        })
    })
</script>
@endsection