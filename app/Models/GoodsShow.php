<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

/**
 * 
 */
class GoodsShow extends Model
{
    protected $table = 'goods_show';

    public function selectHotProduct()
    {
        return Db::table('goods_show')
                ->join('goods_image','goods_show.id','=','goods_image.goods_id')
                ->where('hotgoods',0)
                ->groupBy('goods_image.goods_id')
                ->get();
    }

    public function clasiSfy($typeId)
    {
        return $this->join('goods_image','goods_show.id','=','goods_image.goods_id')
                    ->where('type_id',$typeId)
                    ->groupBy('goods_image.goods_id')
                    ->paginate(8);
    }

    public function sortBy($status,$typeId)
    {
        if($status == 1){
            return $this->join('goods_image','goods_show.id','=','goods_image.goods_id')
                        ->where('type_id',$typeId)
                        ->groupBy('goods_image.goods_id')
                        ->orderBy('goods_price','desc')
                        ->get();
        }else{
            return $this->join('goods_image','goods_show.id','=','goods_image.goods_id')
                        ->where('type_id',$typeId)
                        ->groupBy('goods_image.goods_id')
                        ->orderBy('goods_price','asc')
                        ->get();
        }
    }

}
