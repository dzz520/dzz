@extends('web.layouts.common')
@section('common')

<div class="breadcrumb">
    <div class="container">
        <ul>
            <li><a href="javascript:void(0)">家</a></li>
            <li>清单</li>
        </ul>
    </div>
</div>
<!-- Content -->
<div id="pageContent">
    <div class="container offset-0">
        <div class="row">
            <!-- left col -->
            <div class="slide-column-close">
                <a href="#"><span class="icon icon-close"></span>CLOSE</a>
            </div>
            <div class="col-md-4 col-lg-3 col-xl-3 aside leftColumn">
                <div class="collapse-block open collapse-block-mobile">
                    <h3 class="collapse-block_title hidden">Filter:</h3>
                    <div class="collapse-block_content">
                        <div class="filters-mobile">

                        </div>
                    </div>
                </div>

            </div>
            <!-- center col -->
            <div class="col-md-12">
                <div class="content">
                    <div class="filters-row row filters-row-small-lg">
                        <div class="pull-left">
                            <div class="filters-row_select hidden-sm hidden-xs">
                                <label>按价格排序:</label>
                                <select class="form-control sort-position" id="btn">
                                    <option value="请选择">请选择</option>
                                    <option value="1">从高到低</option>
                                    <option value="2">从低到高</option>
                                </select>
                            </div>
                        </div>
                        <div class="pull-right">
                            <a class="link-view-mobile hidden-lg hidden-md" href="#"><span class="icon icon-view_stream"></span></a>
                            <a class="link-mode link-grid-view active" href="#"><span class="icon icon-view_module"></span></a>
                            <a class="link-mode link-row-view" href="#"><span class="icon icon-view_list"></span></a>
                        </div>
                    </div>
                </div>
                <div class="product-listing row" id="tr">
                    @foreach($data as $v)
                    <div class="col-md-3">
                        <div class="product btns" type_id="{{$v->type_id}}">
                            <div class="product_inside">
                                <div class="image-box">
                                    <a href='{{url("product/$v->id")}}'>
                                        <img src="{{$v->goods_img}}?imageView2/1/w/256/h/340/q/75|imageslim" alt="">
                                        <div class="label-new">New</div>
                                    </a>
                                    <a href='{{url("product/$v->id")}}' class="quick-view">
                                    <span>
                                    <span class="icon icon-visibility"></span>快速查看
                                    </span>
                                    </a>
                                </div>
                                <h2 class="title">
                                    <a href="product.html">{{$v->goods_name}}</a>
                                </h2>
                                <div class="price">
                                    ￥{{$v->goods_price}}
                                </div>
                                <div class="description">
                                    {{$v->goods_desc}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="content">
                    <hr>
                </div>
                <div class="content offset-40">
                    <div class="pagination">
                        {{ $data->render()}}
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 col-xl-3 hidden-md hidden-sm hidden-xs aside rightColumn">
            </div>
        </div>
    </div>
</div>
<script src="{{asset('vendor/weblte/external/jquery/jquery-2.1.4.min.js')}}"></script>
<script>
    $(document).change(function(event) {
        status = $('#btn').val();
        type_id = $('.btns').attr("type_id");
        $.ajax({
            url:'/chanGe',
            data:{status:status,type_id:type_id},
            dataType:'json',
            success:function(e){
                var str='';
                $.each(e.data.data,function(v,i){
                    str+='<div class="col-md-3">';
                    str+='<div class="product btns" type_id="'+i.type_id+'">';
                    str+='<div class="product_inside">';
                    str+='<div class="image-box">';
                    str+='<a href="javascipt:void(0)">';
                    str+='<img src="'+i.goods_img+'?imageView2/1/w/256/h/340/q/75|imageslim" alt="">';
                    str+='<div class="label-new">New</div>';
                    str+='</a>';
                    str+='<a href="#" data-toggle="modal" data-target="#ModalquickView" class="quick-view">';
                    str+='<span>';
                    str+='<span class="icon icon-visibility"></span>QUICK VIEW</span>';
                    str+='</a>';
                    str+='</div>';
                    str+='<h2 class="title">';
                    str+='<a href="product.html">'+i.goods_name+'</a>';
                    str+='</h2>';
                    str+='<div class="price">';
                    str+='￥'+i.goods_price;
                    str+='</div>';
                    str+='<div class="description">';
                    str+='"'+i.goods_desc+'"';
                    str+='</div>';
                    str+='</div>';
                    str+='</div>';
                    str+='</div>';
                })
                $("#tr").empty();
                $("#tr").append(str);
            }
        }) 
    });
</script>
@endsection