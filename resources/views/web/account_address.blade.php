@extends('web.layouts.common')
@section('common')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="loader-wrapper">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="loader-animation" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                </svg>
            </div>
        </div>

        <!-- Content -->
        <div id="pageContent">
            <div class="container offset-18">
                <h1 class="block-title large">账号</h1>
                <div class="offset-36">
                    <h4 class="text-uppercase">我的账号</h4>
                    <hr class="hr-offset-7">
                    <p>
                        <a href="{{url('address_add')}}?id={{Session::get('userInfo.id')}}" class="btn btn--ys text-uppercase">添加地址</a>
                    </p>
                    <p>
                        <a class="link-icon" href="{{url('account')}}"><span class="icon icon-chevron_left"></span>返回账号页面</a>
                    </p>
                    <hr class="hr-offset-7">
                    <h4 class="text-uppercase">
                        @foreach ($data as $v)
                        @if($v->is_default == 1)
                            {{$v->hcity}}(默认地址)
                        @endif
                        @endforeach
                    </h4>
                    <div class="responsive-table-order-history-02">
                        <table class="table-order-history-02" id="table">
                            <thead>
                            <tr>
                                <th>收货人</th>
                                <th>收货地址</th>
                                <th>手机</th>
                                <th>邮政编码</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $v)
                                <tr>
                                    <td>{{$v->accept_name}}</td>
                                    <td>{{$v->hcity}}</td>
                                    <td>{{$v->postcode}}</td>
                                    <td>{{$v->mobile}}</td>
                                    <td>
                                        <a href="address_update?id={{$v->address_id}}" class="btn icon-btn-left edit">编辑</a>
                                        <a href="javascript:void(0)" class="btn icon-btn-left delete" id="{{$v->address_id}}">删除</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection
<script src="external/jquery/jquery-2.1.4.min.js"></script>
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script src="js/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script>
    $(document).on("click",".delete",function(){
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        var id = $(this).attr('id');
        $.ajax({
            type:'post',
            url:"/address_del",
            data:{id:id},
            dataType:"json",
            success:function(e){
                if(e.status=="success"){
                    window.location.href="{{url('account_address')}}?id={{Session::get('userInfo.id')}}";
                }else{
                    swal(e.message);
                }
            }
        })
    })
</script>