<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo', function (Blueprint $table) {
            $table->increments('sku_id')->comment('自增id');
            $table->integer('goods_id')->comment('商品id');
            $table->string('value_list', 100)->comment('属性值组合');
            $table->integer('goods_number')->comment('货品数量');
            $table->decimal('goods_price',10,2)->comment('货品原价');
            $table->decimal('cargo_price',10,2)->comment('货品现价');
            $table->string('goods_img', 100)->comment('货品图片');
            $table->string('goods_desc', 100)->comment('货品描述');
            $table->integer('cargo_state')->default('1')->comment('货品状态 1-在售 2-下架');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo');
    }
}
