<?php

namespace App\Models;

use Spatie\Permission\Models\Role as BaseRole;
use DB;
use Log;

class Role extends BaseRole
{
    protected $table = 'role';

    /**
     * 检测角色是否存在
     */
    public static function isExsitName($roleName, $roleId = 0)
    {
        if($roleId){
            $result = self::where('id', '!=', $roleId)->where('name', $roleName)->first();    
        }else{
            $result = self::where('name', $roleName)->first();    
        }
        
        return $result ? true : false;
    }

    /**
     * 检测角色中文名是否存在
     */
    public static function isExsitCnName($roleCnName, $roleId = 0)
    {
        if($roleId){
            $result = self::where('id', '!=', $roleId)->where('cn_name', $roleCnName)->first();    
        }else{
            $result = self::where('cn_name', $roleCnName)->first();    
        }
        
        return $result ? true : false;
    }

    /**
     * 添加角色
     */
    public function add($data)
    {
        return self::create([
            'name' => $data['name'],
            'cn_name' => $data['cn_name']
        ]);
    }

    /**
     * 编辑角色
     */
    public function edit($roleId, $data)
    {
        $role = $this->find($roleId);
        foreach($data as $field => $value){
            $role->$field = $value;
        }

        return $role->save();
    }

    /**
     * 删除角色
     */
    public function deleteRoleById($roleId)
    {
        $result = true;
        DB::beginTransaction();
        try{
            $role = $this->findById($roleId);
            $role->syncPermissions();
            $role->delete();

            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('role:delete '.$e->getMessage());
            DB::rollBack();
        }
        
        return $result;
    }
}
