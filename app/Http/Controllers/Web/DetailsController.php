<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use App\Models\GoodsAttr;
use App\Models\GoodsShow;
use DB;
use App\Http\Controllers\Controller;

class DetailsController extends Controller
{
    //详情页面数据展示
    public function product(Request $request)
    { 
        $id = $request->goods_id;

        //如果没有该商品则提示并返回
        if (!DB::table('goods_show')->where('id',$id)->first()) {
            return redirect('/404');
        }

        $goodsAttr = new GoodsAttr;

        $dataList = $goodsAttr->getlistId($id);
        
        $displayData = $goodsAttr->attributeId($id);

        $dataimgs = $goodsAttr->img($id);
        
        $scope = $goodsAttr->scope($id);
        
        $data = [];
        
        foreach($displayData as $key=>$value){
        
            if( !isset($data['attr'][''.$value['attr_id']])){
                $data[''.$value['attr_id']]['name'] = $value['attr_name'];//名称

                $nweArray['attr_value'] = $value['attr_value']; //值
                
                $data[''.$value['attr_id']]['child'][] = $nweArray; //组合

            }else{
                $nweArray['attr_value'] = $value['attr_value'];
                
                $data['attr'][''.$value['attr_id']]['child'][] = $nweArray;
            }
        }
        
        $num = $goodsAttr->num($id);

        return view('web.product',[
            'data' => $data,
            'num' => $num, 
            'dataList' => $dataList, 
            'scope' => $scope,
            'dataimgs'=>$dataimgs
        ]);
    }
    //ajax的价格更改
    public function dtailsRevised(Request $request)
    {
        $data = $request->all();
        
        $goodsId = $data['goods_id'];
        
        $str = implode(',',$data);

        $CharacterString = substr($str,0,strrpos($str,','));

        $goodsAttr = new GoodsAttr;


        $returnData = $goodsAttr->record($CharacterString,$goodsId);

        
        foreach($returnData as $v){
            $price = $v['cargo_price'];
            $sku_id = $v['sku_id'];
            $goodsNumber = $v['goods_number'];
        }

        if(!$returnData){
            return $this->ajaxError('default');die;
        }

        return $this->ajaxSuccess('success',array(
            'data' => $price,
            "sku_id" => $sku_id,
            "goodsNumber"=>$goodsNumber
        ));

    }

    /**
    * 分类展示服装
    */
    public function listing(Request $request)
    {
        $typeId = $request->type_id;

        $goodsShow = new GoodsShow;

        $data = $goodsShow->clasiSfy($typeId);
        
        return view('web.listing',[
            'data'=>$data
        ]);
    }

    /**
    * 分类展示
    */
    public function chanGe(Request $request)
    {
        $status = $request->status;

        $typeId = $request->type_id;
        
        $goodsShow = new GoodsShow;

        $data = $goodsShow->sortBy($status,$typeId);

        if($data){
            return $this->ajaxSuccess('success',array(
                'data'=>$data
            ));
        }
    }
}