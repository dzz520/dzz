@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/goods/goodsadd')}}" method="post" id="form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">商品名称</label>

                    <div class="col-sm-10">
                        <input type="text" name="goods_name" class="form-control" placeholder="请输入商品名称">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">服装分类</label>

                    <div class="col-sm-10">
                        <select class="form-control" name="type_id">
                            <option>请选择</option>
                            @foreach($dataType as $data)
                            <option value="{{$data['type_id']}}">{{$data['type_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品价格(‘元’)</label>

                    <div class="col-sm-10">
                        <input type="number" name="goods_price" class="form-control" placeholder="请输入商品价格">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品封面：</label>
                    <div class="file-loading">
                        <input id="file" name="file" type="file" multiple>
                    </div>
                </div>
                <div id="img"></div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">上架日期</label>

                    <div class="col-sm-10">
                        <input type="datetime-local" name="up_time" class="form-control">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">是否热销</label>

                    <div class="col-sm-10">
                       <input type="radio" name="hotgoods"  value="0" checked> 是
                       <input type="radio" name="hotgoods"  value="1"> 否
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">是否上架</label>

                    <div class="col-sm-10">
                        <input type="radio" name="goods_state"  value="1" checked> 是
                        <input type="radio" name="goods_state"  value="2"> 否
                        
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="describe" class="col-sm-2 control-label" style="color: #333">商品描述</label>
                    <div class="col-sm-10">
                        <script id="editor" name="goods_desc" type="text/plain" style="width:800px;height:200px;"></script>
                    </div>
                </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
<link href="{{asset('bootstrap')}}/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="{{asset('bootstrap')}}/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
@endpush

@push('js')
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor')}}/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor')}}/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="{{asset('ueditor')}}/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript">
        var ue = UE.getEditor('editor');
</script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="{{asset('bootstrap')}}/js/plugins/sortable.js" type="text/javascript"></script>
<script src="{{asset('bootstrap')}}/js/fileinput.js" type="text/javascript"></script>
<script src="{{asset('bootstrap')}}/js/locales/fr.js" type="text/javascript"></script>
<script src="{{asset('bootstrap')}}/js/locales/es.js" type="text/javascript"></script>
<script src="{{asset('bootstrap')}}/themes/fas/theme.js" type="text/javascript"></script>
<script src="{{asset('bootstrap')}}/themes/explorer-fas/theme.js" type="text/javascript"></script>
<script>
    $(function(){
          $("#file").fileinput({
            theme: 'fas',
            language: 'zh', //设置语言
            uploadUrl:'{{url('admin/goods/img')}}', //上传的地址
            allowedFileExtensions: ['jpg', 'gif', 'png'],//接收的文件后缀
            //uploadExtraData:{"id": 1, "fileName":'123.mp3'},
            fileType: "any",
            uploadAsync: true, //默认异步上传
            showUpload:true, //是否显示上传按钮
            showRemove :true, //显示移除按钮
            showPreview :true, //是否显示预览
            showCaption:true,//是否显示标题
            browseClass:"btn btn-primary", //按钮样式    
            dropZoneEnabled: true,//是否显示拖拽区域
            maxFileCount:10, //表示允许同时上传的最大文件个数
            enctype:'multipart/form-data',
            validateInitialCount:true,
            overwriteInitial: true,
            initialPreviewAsData: false,
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
            msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
        }).on("fileuploaded", function (event, data, previewId, index){
            $('#img').append('<input type="hidden" name="goods_img[]" value='+data.response.data+'>')
        });
        
        var validator = $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                goods_name: {
                    validators: {
                        notEmpty: {message: '请输入商品名称'},    //非空提示
                        regexp: {//匹配规则
                            regexp: /^[\u4e00-\u9fa5]+$/,  //正则表达式
                            message:'商品名称仅支持汉字'
                        },
                        remote: { //ajax校验，获得一个json数据（{'valid': true or false}）
                            url: '{{url("admin/goods/goodsExist")}}',                  //验证地址
                            message: '该名称已存在',   //提示信息
                            type: 'post',
                            data: function(){
                                return {
                                    id: $('[name=id]').val(),
                                    name: $('[name=goods_name]').val(),
                                    type: 'goods_name'
                                }
                            }
                        }
                    }
                },
                goods_desc: {
                    validators: {
                        notEmpty: {message: '请输入商品介绍'},    //非空提示
                        regexp: {//匹配规则
                            regexp: /[^\a-\z\A-\Z0-9\u4E00-\u9FA5]/g,  //正则表达式
                            message:'商品内容可以是中文或者字母或者数字'
                        },
                    }
                },
                goods_price: {
                    validators: {
                        notEmpty: {message: '请输入商品价格'},    //非空提示
                    }
                }
            }
        }).on('success.form.bv', function(e) {
            e.preventDefault();
            var $form = $(e.target);

            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/goods')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@endpush