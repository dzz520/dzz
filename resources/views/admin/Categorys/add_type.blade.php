@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/categorys/addtype')}}" method="post" id="form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">添加分类</label>

                    <div class="col-sm-10">
                        <input type="text" name="type_name" class="form-control" id="type_name" placeholder="请输入分类名称">
                    </div>
                </div>
            </div>
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label class="col-sm-2 control-label">所属分类</label>
                    <div class="col-sm-10">
                        <select id="images" class="form-control select2" style="width: 100%;" name="pid">
                            <option value="0">请选择</option>
                            @foreach($data as $val)
                            <option value="{{$val['type_id']}}">{{$val['type_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
             <div class="box-body img">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">分类图片</label>

                    <div class="col-sm-10">
                        <input type="file" name="images" class="form-control">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(document).on('change','#images',function(){
        var pid = $(this).val();
        if (pid != 0){
            $('.img').remove();
        }
    })

    $(function(){
        $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                type_name: {
                    validators: {
                        notEmpty: {message: '请输入分类名称'},
                        regexp: {
                            regexp: /^[\u4e00-\u9fa5]+$/,
                            message:'商品名称仅支持汉字'
                        },
                    } 
                }
            }
        }).on('success.form.bv', function(e) {
            console.log(e);
        });
    })
</script>
@stop