<?php
namespace App\Http\Controllers\Web;
use App\Models\Address;
use App\Models\Cart;
use DB;
use Validator;
use App\Models\Checkout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
    //三级联动
    public function addressAdd(Request $request)
    {
        if($request->ajax()){
            $id = $request->input("id");
            $one = (new Address)->addInfo($id);
            echo json_encode($one);die;
        }
        $one = (new Address)->addDo();
        return view('web.address_add',['one'=>$one]);
    }

    /**
     * 收货地址添加
     */
    public function addressDo(Request $request)
    {
        //验证
        $validator = Validator::make($request->all(), [
            'accept_name' => 'required|min:2',
            'address' => 'required|min:5',
            'postcode' => 'required|max:6',
            'is_default' => 'required',
            'mobile' => 'phone:CN'
        ], [
            'accept_name.required' =>'收货人不能为空',
            'accept_name.min' =>'收货人不能小于2位',
            'address.min' => '具体地址不能小于5位',
            'address.required' => '具体地址不能为空',
            'zip.required' => '邮政编码不能为空',
            'zip.max' => '邮政编码不能大于6位',
            'is_default.required' => '是否默认不能为空',
            'tel.phone' => '请输入有效的手机号码'
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }
        //验证
        $addressId= intval($request->address_id);
        if(Address::isExistAddress($request->address, $addressId)){
            return $this->ajaxError('地址已存在');
        }
        //Model
        $info = new Address;
        $data['user_id'] = $request->session()->get('userInfo.id');
        $data['accept_name'] = $request->accept_name;
        //循环地址
        foreach ($request->input('country') as $key=>$v){
            $into[] = $info->rigin($v);
        }
        $regionName = "";
        //地址拼接
        foreach ($into as $key=>$val) {
            if ($key != 0){
                foreach ($val as $v){
                    $regionName = $regionName.$v['region_name'];
                }
            }
        }
        $data['hcity'] = $regionName.$request->address;
        $data['postcode'] = $request->postcode;
        $data['mobile'] = $request->mobile;
        $data['is_default'] = $request->is_default;
        //判断默认地址
        if ($data['is_default'] == 1){
            //Model
            $userId = $request->session()->get('userInfo.id');
            $info = new Address;
            $info->isDefault($userId, $data);
        }
        $addressAdd = $info->add($data);
        if($addressAdd){
            return $this->ajaxSuccess("添加成功");
        }else{
            return $this->ajaxError("添加失败");
        }
    }
    /**
     * 收货地址展示
     */
    public function accountAddress(Request $request)
    {
        $userId = intval($request->id);
        $checkout = new Address;
        $data = $checkout->addressShow($userId);
        return view('web.account_address',['data'=>$data]);
    }

    /**
     * 收货地址删除
     */
    public function addressDel(Request $request)
    {
        $address_id = $request->input('id');
        $res = (new address)->del($address_id);
        if($res){
            return $this->ajaxSuccess("删除成功");
        }else{
            return $this->ajaxError("删除失败");
        }
    }
    
    /**
     * 收货地址编辑展示
     */
    public function addressEdit(Request $request){

        $id = $request->all();
        $res = DB::table('address')->where('address_id',$id)->first();
        $one = (new address)->addDo();
        return view('web.address_update',['res'=>$res,"one"=>$one]);


    }

    /**
     * 收货地址编辑
     */
    public function addressEditdo(Request $request)
    {
        //验证
        $validator = Validator::make($request->all(), [
            'accept_name' => 'required|min:2',
            'address' => 'required|min:5',
            'postcode' => 'required|max:6',
            'is_default' => 'required',
            'mobile' => 'phone:CN'
        ], [
            'accept_name.required' =>'收货人不能为空',
            'accept_name.min' =>'收货人不能小于2位',
            'address.min' => '具体地址不能小于5位',
            'address.required' => '具体地址不能为空',
            'zip.required' => '邮政编码不能为空',
            'zip.max' => '邮政编码不能大于6位',
            'is_default.required' => '是否默认不能为空',
            'tel.phone' => '请输入有效的手机号码'
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }
        //验证
        $address_id= intval($request->address_id);
        if(Address::isExistAddress($request->address, $address_id)){
            return $this->ajaxError('地址已存在');
        }
        //Model
        $info = new Address;
        //$data['address_id']= $request->address_id;
        $data['user_id'] = $request->session()->get('userInfo.id');
        $data['accept_name'] = $request->accept_name;
        //循环地址
        foreach ($request->input('country') as $key=>$v){
            $into[] = $info->rigin($v);
        }
        $regionName = "";
        //地址拼接
        foreach ($into as $key=>$val) {
            if ($key != 0){
                foreach ($val as $v){
                    $regionName = $regionName.$v['region_name'];
                }
            }
        }
        $data['hcity'] = $regionName.$request->address;
        $data['postcode'] = $request->postcode;
        $data['mobile'] = $request->mobile;
        $data['is_default'] = $request->is_default;
        //判断默认地址
        if ($data['is_default'] == 1){
            //Model
            $userId = $request->session()->get('userInfo.id');
            $info = new Address;
            $info->isDefault($userId, $data);
        }
        $address = new Address;
        $result =  $address->edit($address_id, $data);
        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }
}