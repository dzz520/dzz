<?php

namespace App\Console\Commands;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Console\Command;

class order extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $year = date('Y');
        for ($i=1;$i<=4;$i++){
            Schema::create($year.'_'.$i.'_'.'orders', function (Blueprint $table) {
                $table->increments('order_id')->commit('自增id');
                $table->integer('order_sn')->comment('订单货号');
                $table->decimal('order_price',10,2)->comment('订单应付金额');
                $table->decimal('real_price', 10,2)->comment('订单实付金额');
                $table->integer('user_id')->comment('用户id');
                $table->string('order_name', 100)->comment('收货人姓名');
                $table->char('order_tel',11)->comment('收货人手机');
                $table->string('order_address', 200)->comment('收货地址');
                $table->char('order_state',10)->comment('订单状态  1-待付款 2-待发货 3-配送中  5-待评价  6-已完成  7-已取消 ');
                $table->char('pay_type',10)->comment('支付方式 1-在线支付   2-货到付款');
                $table->char('pay_state',10)->comment('支付状态 1-已付款  2-未付款  3-取消付款');
                $table->datetime('pay_time')->default('0000-01-01 00:00:00')->comment('付款时间');
                $table->datetime('sell_time')->default('0000-01-01 00:00:00')->comment('发货时间');
                $table->integer('mode_id')->comment('物流id');
                $table->char('mode_sn',50)->comment('物流货号');
                $table->string('order_message', 200)->comment('订单留言');
                $table->timestamps();
            });
        }
    }
}
