@extends('web.layouts.common')
@section('common')

    <div class="loader-wrapper">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="loader-animation" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
        </div>
    </div>
    <nav class="panel-menu">
        <ul>
            <li>
                <a href="index.html">LAYOUT</a>
                <ul>
                    <li><a href="index.html">Home page 1</a></li>
                    <li><a href="index-02.html">Home page 2</a></li>
                    <li><a href="index-03.html">Home page 3</a></li>
                    <li><a href="index-04.html">Home page 4</a></li>
                    <li><a href="index-05.html">Home page 5</a></li>
                    <li><a href="index-06.html">Home page 6</a></li>
                    <li><a href="index-07.html">Home page 7</a></li>
                    <li><a href="index-08.html">Home page 8</a></li>
                    <li><a href="index-09.html">Home page 9</a></li>
                    <li><a href="index-10.html">Home page 10</a></li>
                    <li><a href="index-11.html">Home page 11</a></li>
                    <li><a href="index-12.html">Home page 12</a></li>
                </ul>
            </li>
            <li><a href="index-rtl.html">RTL</a></li>
            <li>
                <a href="listing-left-column.html">LISTING</a>
                <ul>
                    <li><a href="listing-left-column.html">Left Column</a></li>
                    <li><a href="listing-left-right-column.html">Left and Right Column</a></li>
                    <li><a href="listing-right-column.html">Right Column</a></li>
                    <li><a href="listing-left-column-without-block.html">Left Column Without Block</a></li>
                    <li><a href="listing-without-columns.html">Without Columns</a></li>
                    <li><a href="listing-without-columns-items6.html">Without Column and 6 items in line</a></li>
                    <li><a href="listing-without-columns-items4.html">Without Column and 4 items in line</a></li>
                    <li><a href="listing-without-columns-items3.html">Without Column and 3 items in line</a></li>
                    <li><a href="listing-without-columns-items2.html">Without Column and 2 items in line</a></li>
                </ul>
            </li>
            <li>
                <a href="product.html">PRODUCT</a>
                <ul>
                    <li><a href="product.html">Image Size - Small</a></li>
                    <li><a href="product-02.html">Image Size - Medium</a></li>
                    <li><a href="product-03.html">Image Size - Big</a></li>
                    <li><a href="product-04.html">Image Size - Medium</a></li>
                    <li><a href="product-05.html">Image Size - Long</a></li>
                </ul>
            </li>
            <li>
                <a href="blog_listing.html">BLOG</a>
                <ul>
                    <li><a href="blog_listing.html">Listing</a></li>
                    <li><a href="blog_grid_col_2.html">Grid Layout 2 cols</a></li>
                    <li><a href="blog_grid_col_3.html">Grid Layout 3 cols</a></li>
                    <li><a href="blog_masonry_col_2.html">Masonry Layout 2 cols</a></li>
                    <li><a href="blog_masonry_col_3.html">Masonry Layout 3 cols</a></li>
                    <li><a href="blog_single_post.html">Blog single post 1</a></li>
                    <li><a href="blog_single_post_01.html">Blog single post 2</a></li>
                </ul>
            </li>
            <li>
                <a href="gallery_masonry_col_3.html">GALLERY</a>
                <ul>
                    <li><a href="gallery_grid_col_2.html">Grid col 2</a></li>
                    <li><a href="gallery_grid_col_3.html">Grid col 3</a></li>
                    <li><a href="gallery_masonry_col_2.html">Masonry col 2</a></li>
                    <li><a href="gallery_masonry_col_3.html">Masonry col 3</a></li>
                </ul>
            </li>
            <li>
                <a href="about.html">PAGES</a>
                <ul>
                    <li><a href="about.html">About</a></li>
                    <li><a href="about_01.html">About 2</a></li>
                    <li><a href="contact.html">Contacts</a></li>
                    <li><a href="contact_01.html">Contacts 2</a></li>
                    <li><a href="comming-soon.html">Under Construction</a></li>
                    <li><a href="look-book.html">Lookbook</a></li>
                    <li><a href="collections.html">Collections</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="infographic.html">Infographic</a></li>
                    <li><a href="faqs.html">Delivery Page</a></li>
                    <li><a href="faqs.html">Payment page</a></li>
                    <li><a href="checkout.html">Checkout</a></li>
                    <li><a href="compare.html">Compare</a></li>
                    <li><a href="wishlist.html">Wishlist</a></li>
                    <li><a href="shopping_cart_01.html">Shopping cart 01</a></li>
                    <li><a href="shopping_cart_02.html">Shopping cart 02</a></li>
                    <li><a href="account.html">Page Account</a></li>
                    <li><a href="account-address.html">Page Account address</a></li>
                    <li><a href="account-order.html">Page Account order</a></li>
                    <li><a href="login-form.html">Login form</a></li>
                    <li><a href="faqs.html">Support page</a></li>
                    <li><a href="faqs.html">FAQs</a></li>
                    <li><a href="page-404.html">Page 404</a></li>
                    <li><a href="page-empty-category.html">Page Empty Category</a></li>
                    <li><a href="page-empty-search.html">Page Empty Search</a></li>
                    <li><a href="page-empty-shopping-cart.html">Page Empty shopping Cart</a></li>
                </ul>
            </li>
            <li>
                <a href="listing-left-column.html">WOMEN'S</a>
                <ul>
                    <li>
                        <a href="listing-left-column.html">TOPS</a>
                        <ul>
                            <li><a href="listing-left-column.html">Blouses & Shirts</a></li>
                            <li><a href="listing-left-column.html">Dresses</a></li>
                            <li>
                                <a href="listing-left-column.html">Tops & T-shirts</a>
                                <ul>
                                    <li><a href="listing-left-column.html">Link level 1</a></li>
                                    <li>
                                        <a href="listing-left-column.html">Link level 1</a>
                                        <ul>
                                            <li><a href="listing-left-column.html">Link level 2</a></li>
                                            <li>
                                                <a href="listing-left-column.html">Link level 2</a>
                                                <ul>
                                                    <li><a href="listing-left-column.html">Link level 3</a></li>
                                                    <li><a href="listing-left-column.html">Link level 3</a></li>
                                                    <li><a href="listing-left-column.html">Link level 3</a></li>
                                                    <li>
                                                        <a href="listing-left-column.html">Link level 3</a>
                                                        <ul>
                                                            <li>
                                                                <a href="listing-left-column.html">Link level 4</a>
                                                                <ul>
                                                                    <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                </ul>
                                                            </li>
                                                            <li><a href="listing-left-column.html">Link level 4</a></li>
                                                            <li><a href="listing-left-column.html">Link level 4</a></li>
                                                            <li><a href="listing-left-column.html">Link level 4</a></li>
                                                            <li><a href="listing-left-column.html">Link level 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="listing-left-column.html">Link level 3</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="listing-left-column.html">Link level 2</a></li>
                                            <li><a href="listing-left-column.html">Link level 2</a></li>
                                            <li><a href="listing-left-column.html">Link level 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="listing-left-column.html">Link level 1</a></li>
                                    <li><a href="listing-left-column.html">Link level 1</a></li>
                                    <li><a href="listing-left-column.html">Link level 1</a></li>
                                </ul>
                            </li>
                            <li><a href="listing-left-column.html">Sleeveless Tops</a></li>
                            <li><a href="listing-left-column.html">Sweaters</a></li>
                            <li><a href="listing-left-column.html">Jackets</a></li>
                            <li><a href="listing-left-column.html">Outerwear</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing-left-column.html">BOTTOMS</a>
                        <ul>
                            <li><a href="listing-left-column.html">Trousers</a></li>
                            <li><a href="listing-left-column.html">Jeans</a></li>
                            <li><a href="listing-left-column.html">Leggings</a></li>
                            <li><a href="listing-left-column.html">Jumpsuit & shorts</a></li>
                            <li><a href="listing-left-column.html">Skirts</a></li>
                            <li><a href="listing-left-column.html">Tights</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing-left-column.html">ACCESSORIES</a>
                        <ul>
                            <li><a href="listing-left-column.html">Jewellery</a></li>
                            <li><a href="listing-left-column.html">Hats</a></li>
                            <li><a href="listing-left-column.html">Scarves & snoods</a></li>
                            <li><a href="listing-left-column.html">Belts</a></li>
                            <li><a href="listing-left-column.html">Bags</a></li>
                            <li><a href="listing-left-column.html">Shoes</a></li>
                            <li><a href="listing-left-column.html">Sunglasses</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing-left-column.html">SPECIALS</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="listing-right-column.html">MEN'S</a>
                <ul>
                    <li>
                        <a href="listing-right-column.html">TOPS</a>
                        <ul>
                            <li><a href="listing-right-column.html">Blouses & Shirts</a></li>
                            <li><a href="listing-right-column.html">Dresses</a></li>
                            <li>
                                <a href="listing-right-column.html">Tops & T-shirts</a>
                                <ul>
                                    <li><a href="listing-right-column.html">Link level 1</a></li>
                                    <li>
                                        <a href="listing-right-column.html">Link level 1</a>
                                        <ul>
                                            <li><a href="listing-right-column.html">Link level 2</a></li>
                                            <li>
                                                <a href="listing-right-column.html">Link level 2</a>
                                                <ul>
                                                    <li><a href="listing-right-column.html">Link level 3</a></li>
                                                    <li><a href="listing-right-column.html">Link level 3</a></li>
                                                    <li><a href="listing-right-column.html">Link level 3</a></li>
                                                    <li>
                                                        <a href="listing-right-column.html">Link level 3</a>
                                                        <ul>
                                                            <li>
                                                                <a href="listing-right-column.html">Link level 4</a>
                                                                <ul>
                                                                    <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                    <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                </ul>
                                                            </li>
                                                            <li><a href="listing-right-column.html">Link level 4</a></li>
                                                            <li><a href="listing-right-column.html">Link level 4</a></li>
                                                            <li><a href="listing-right-column.html">Link level 4</a></li>
                                                            <li><a href="listing-right-column.html">Link level 4</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="listing-right-column.html">Link level 3</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="listing-right-column.html">Link level 2</a></li>
                                            <li><a href="listing-right-column.html">Link level 2</a></li>
                                            <li><a href="listing-right-column.html">Link level 2</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="listing-right-column.html">Link level 1</a></li>
                                    <li><a href="listing-right-column.html">Link level 1</a></li>
                                    <li><a href="listing-right-column.html">Link level 1</a></li>
                                </ul>
                            </li>
                            <li><a href="listing-right-column.html">Sleeveless Tops</a></li>
                            <li><a href="listing-right-column.html">Sweaters</a></li>
                            <li><a href="listing-right-column.html">Jackets</a></li>
                            <li><a href="listing-right-column.html">Outerwear</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing-right-column.html">BOTTOMS</a>
                        <ul>
                            <li><a href="listing-right-column.html">Trousers</a></li>
                            <li><a href="listing-right-column.html">Jeans</a></li>
                            <li><a href="listing-right-column.html">Leggings</a></li>
                            <li><a href="listing-right-column.html">Jumpsuit & shorts</a></li>
                            <li><a href="listing-right-column.html">Skirts</a></li>
                            <li><a href="listing-right-column.html">Tights</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="listing-right-column.html">ACCESSORIES</a>
                        <ul>
                            <li><a href="listing-right-column.html">Jewellery</a></li>
                            <li><a href="listing-right-column.html">Hats</a></li>
                            <li><a href="listing-right-column.html">Scarves & snoods</a></li>
                            <li><a href="listing-right-column.html">Belts</a></li>
                            <li><a href="listing-right-column.html">Bags</a></li>
                            <li><a href="listing-right-column.html">Shoes</a></li>
                            <li><a href="listing-right-column.html">Sunglasses</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">BUY TEMPLATE</a></li>
        </ul>
        <div class="mm-navbtn-names" style="display:none">
            <div class="mm-closebtn">CLOSE</div>
            <div class="mm-backbtn">BACK</div>
        </div>
    </nav>

    <div id="pageContent">
        <div class="container-fluid offset-0">
            <div class="row">
                <div class="slider-revolution revolution-default">
                    <div class="tp-banner-container">
                        <div class="tp-banner revolution">
                            <ul>
                                <li data-thumb="{{asset('vendor/weblte/images/slides/04/slide-1.jpg')}}" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
                                    <img src="{{asset('vendor/weblte/images/slides/04/slide-1.jpg')}}"  alt="slide1"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
                                    <div class="tp-caption tp-resizeme  lfb stb text-center"
                                         data-x="center"
                                         data-y="center"
                                         data-hoffset="0"
                                         data-voffset="-20"
                                         data-speed="600"
                                         data-start="900"
                                         data-easing="Power4.easeOut"
                                         data-endeasing="Power4.easeIn"
                                         data-responsive_offset="on"
                                         style="z-index: 2;">
                                        <div class="tp-caption1-wd-2 color-white">时尚看我的,我看世界的！<br>Webstore</div>
                                    </div>
                                </li>
                                <li data-thumb="images/slides/04/slide-2.jpg" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
                                    <img src="{{asset('vendor/weblte/images/slides/04/slide-2.jpg')}}"  alt="slide1"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" >
                                    <div class="tp-caption tp-resizeme  lfr str text-right"
                                         data-x="right"
                                         data-y="center"
                                         data-voffset="-20"
                                         data-hoffset="-351"
                                         data-speed="600"
                                         data-start="900"
                                         data-easing="Power4.easeOut"
                                         data-endeasing="Power4.easeIn"
                                         data-responsive_offset="on"
                                         style="z-index: 2;">
                                        <div class="tp-caption1-wd-1 color-base">时代名流,尚帝生活</div>
                                        <div class="tp-caption1-wd-2">潮流风尚<br>追逐时尚</div>
                                    </div>
                                </li>
                                <li data-thumb="images/slides/video/video_img.jpg" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off"  data-title="Slide">
                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-fade fadeout fullscreenvideo"
                                         data-x="0"
                                         data-y="0"
                                         data-speed="600"
                                         data-start="0"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="1500"
                                         data-endeasing="Power4.easeIn"
                                         data-autoplay="true"
                                         data-autoplayonlyfirsttime="false"
                                         data-nextslideatend="true"
                                         data-forceCover="1"
                                         data-dottedoverlay="twoxtwo"
                                         data-aspectratio="16:9"
                                         data-forcerewind="on"
                                         style="z-index: 2">
                                        <video class="video-js vjs-default-skin" preload="none"
                                               poster="{{asset('vendor/weblte/images/slides/video/video_img.jpg')}}" data-setup="{}">
                                            <source src="{{asset('vendor/weblte/images/slides/video/video.mp4')}}" type='video/mp4' />
                                        </video>
                                    </div>
                                    <div class="tp-caption  tp-fade"
                                         data-x="right"
                                         data-y="bottom"
                                         data-voffset="-60"
                                         data-hoffset="-90"
                                         data-speed="600"
                                         data-start="900"
                                         data-easing="Power4.easeOut"
                                         data-endeasing="Power4.easeIn"
                                         data-responsive_offset="off"
                                         style="z-index: 7;">
                                        <div class="video-play">
                                            <a class="icon icon-play_circle_filled btn-play" href="#"></a>
                                            <a class="icon icon-pause_circle_filled btn-pause" href="#"></a>
                                        </div>
                                    </div>
                                    <!-- TEXT -->
                                    <div class="tp-caption tp-resizeme  lfb lft text-center"
                                         data-x="center"
                                         data-y="center"
                                         data-voffset="-20"
                                         data-hoffset="0"
                                         data-speed="600"
                                         data-start="900"
                                         data-easing="Power4.easeOut"
                                         data-endeasing="Power4.easeIn"
                                         data-responsive_offset="on"
                                         style="z-index: 2;">
                                        <div class="tp-caption1-wd-2 color-white">时尚看我的,我看 <br>in <span class="color-base">Clothing</span></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <div class="container">
            <h1 class="block-title">每日推荐</h1>
            <div class="row">
                <div class="carousel-products-mobile subcategory-listing">
                    @foreach($information as $v)
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <a href='{{url("listing/$v->type_id")}}' class="subcategory-item zoom-in">
                            <span>
                            <img src="{{$v->type_img}}?imageView2/1/w/256/h/340/q/75|imageslim" alt="">
                            </span>
                            <span class="title">{{$v->type_name}}</span>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container hidden-mobile">
            <hr>
        </div>
        <div class="container">
            <h1 class="block-title">特色产品</h1>
            <div class="row product-listing carousel-products-mobile products-mobile-arrow">
            @foreach($data as $v)
            <div class="col-xs-6 col-sm-4 col-md-3 ">
                    <div class="product">
                        <div class="product_inside">
                            <div class="image-box">
                                <a href='{{url("product/$v->id")}}'>
                                    <img src='{{$v->goods_img}}?imageView2/1/w/256/h/340/q/75|imageslim' alt="">
                                    <div class="label-new">新</div>
                                </a>
                                <a href='{{url("product/$v->id")}}' data-target="#ModalquickView" class="quick-view">
                                    <span>
                                    <span class="icon icon-visibility"></span>快速查看
                                    </span>
                                </a>
                            </div>
                            <h2 class="title">
                                <a href='{{url("product/$v->id")}}'>{{$v->goods_name}}</a>
                            </h2>
                            <div class="price view">
                                ${{$v->goods_price}}
                            </div>
                            <div class="description">
                               {{$v->goods_desc}}
                            </div>
                        </div>
                    </div>
            </div>
            @endforeach
        </div>
        <div class="container hidden-mobile">
            <hr>
        </div>
        <div class="container">
            <div class="row">
                <div class="carousel-brands">
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-01.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-02.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-03.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-04.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-05.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-06.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-07.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-08.png')}}" alt="">
                        </a>
                    </div>
                    <div>
                        <a href="#">
                            <img src="{{asset('vendor/weblte/images/custom/brand-09.png')}}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <a href="{{asset('listing')}}" class="home4_banner_big zoom-in">
                    <img src="{{asset('vendor/weblte/images/custom/promo-img-05.jpg')}}" alt="">
                    <div class="box-arrow-bottom-right">
                        <div class="animation-hover">
                            <div class="svg-icon">
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
                                        <path     d="M234.625,393.808c0,0,86.365,97.951,151.666,151.666s37.917,13.692,37.917,16.852c0,3.16,104.27,73.727,183.263,111.643
                                            c78.993,37.917,133.761,43.183,168.518,45.289c34.757,2.106,38.97,0,38.97,0s-86.365-4.213-117.963-20.012
                                            c-31.597-15.799,55.822,15.799,117.963,5.266c62.141-10.532,70.567-22.118,70.567-22.118s-230.659,42.012-409.709-125.394
                                            s-179.05-175.832-179.05-175.832s52.662,51.609,116.909,50.555c64.247-1.053,85.312-48.449,50.555-58.981
                                            S217.773,237.929,217.773,237.929s-10.532-12.639-44.236,6.319s-33.704,18.958-33.704,18.958s51.609,78.993,71.62,218.02
                                            s21.065,182.21,21.065,182.21l15.799-54.768"/>
                                    </svg>
                            </div>
                            <div class="text">
                                MyShop超过3000小时<br>
                                努力工作!
                            </div>
                        </div>
                    </div>
                    <div class="description">
                        <div class="block-table">
                            <div class="block-table-cell text-center">
                                <div class="title color-defaulttext2">我的店铺</div>
                                <span class="btn btn-lg">现在去购物!</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="container">
            <h2 class="block-title">最新消息</h2>
            <div class="row">
                <div class="carousel-products-mobile blog-thumb-listing">
                    <div class="col-xs-6 col-sm-4">
                        <div class="blog-thumb">
                            <a class="img" href="javascipt:void(0)">
                                <img src="{{asset('vendor/weblte/images/custom/img_03.jpg')}}" alt="">
                            </a>
                            <a class="title" href="javascipt:void(0)">Resposive/Retina 准备好了.</a>
                            <p class="data">
                                由于<a href="#">MyShop</a> 发布 <b>2016年09月</b>
                            </p>
                            <p>
                                最高的响应水平。您的网站在任何设备上都会显得清晰而神奇。无论您使用何种显示器。 测试一下。
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="blog-thumb">
                            <a class="img" href="javascipt:void(0)">
                                <img src="{{asset('vendor/weblte/images/custom/img_02.jpg')}}" alt="">
                            </a>
                            <a class="title" href="javascipt:void(0)">顶级Notch SEO性能。  </a>
                            <p class="data">
                                由于<a href="#">MyShop</a> 发布 <b>2016年09月</b>
                            </p>
                            <p>
                                最高的响应水平。您的网站在任何设备上都会显得清晰而神奇。无论您使用何种显示器。 测试一下。
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="blog-thumb">
                            <a class="img" href="javascipt:void(0)">
                                <img src="{{asset('vendor/weblte/images/custom/img_01.jpg')}}" alt="">
                            </a>
                            <a class="title" href="javascipt:void(0)">Fully <br>customizable.</a>
                            <p class="data">
                                由于<a href="#">MyShop</a> 发布 <b>2016年09月</b>
                            </p>
                            <p>
                                最高的响应水平。您的网站在任何设备上都会显得清晰而神奇。无论您使用何种显示器。 测试一下。.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal (quickViewModal) -->
    <div class="modal  fade"  id="ModalquickView" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                </div>
                <form>
                    <div class="modal-body">
                        <!--modal-quick-view-->
                        <div class="modal-quick-view">
                            <div class="row">
                                <div class="col-sm-5 col-lg-6">
                                    <div class="product-main-image">
                                        <img src='{{asset('vendor/weblte/images/product/product-big-1.jpg')}}' alt="" />
                                    </div>
                                </div>
                                <div class="col-sm-7 col-lg-6">
                                    <div class="product-info">
                                        <div class="add-info">
                                            <div class="sku pull-left">
                                                <span class="font-weight-medium color-defaulttext2">库存单位:</span> mtk012c
                                            </div>
                                            <div class="availability pull-left">
                                                <span class="font-weight-medium color-defaulttext2">有效:</span> <span class="color-red">库存的</span> <span class="color-base">缺货</span>
                                            </div>
                                        </div>
                                        <h1 class="title">黛西街3 / 4袖板式休闲衬衫</h1>
                                        <div class="price">
                                            $45
                                        </div>
                                        <div class="description hidden-xs">
                                            <div class="text">
                                                银色、金属蓝和金属紫丝绸混纺提花面料、图案、衣领褶边、袖口扣扣袖口长袖、扣银细腰带、臀部大褶玫瑰花饰
                                            </div>
                                        </div>
                                        <div class="wrapper">
                                            <div class="title-options">结构<span class="color-required">*</span></div>
                                            <ul class="options-swatch-texture">
                                                <li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-01.jpg')}}" alt=""></a></li>
                                                <li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-02.jpg')}}" alt=""></a></li>
                                                <li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-03.jpg')}}" alt=""></a></li>
                                            </ul>
                                        </div>
                                        <div class="wrapper">
                                            <div class="title-options">尺寸 <span class="color-required">*</span></div>
                                            <ul class="tags-list">
                                                <li><a href="#">XS</a></li>
                                                <li class="active"><a href="#">S</a></li>
                                                <li><a href="#">M</a></li>
                                                <li><a href="#">L</a></li>
                                            </ul>
                                        </div>
                                        <div class="wrapper">
                                            <div class="pull-left"><label class="qty-label">数量</label></div>
                                            <div class="pull-left">
                                                <div class="style-2 input-counter">
                                                    <span class="minus-btn"></span>
                                                    <input type="text" value="1" size="5"/>
                                                    <span class="plus-btn"></span>
                                                </div>
                                            </div>
                                            <div class="pull-left">
                                                <a href="#" class="btn btn-addtocart"><span class="icon icon-shopping_basket"></span>现在去购物!</a>
                                            </div>
                                        </div>
                                        <div class="wrapper">
                                            <div class="pull-left">
                                                <ul class="product_inside_info_link">
                                                    <li class="text-right">
                                                        <a href="#">
                                                            <span class="fa fa-heart-o"></span>
                                                            <span class="text">添加到愿望列表</span>
                                                        </a>
                                                    </li>
                                                    <li class="text-left">
                                                        <a href="#" class="compare-link">
                                                            <span class="fa fa-balance-scale"></span>
                                                            <span class="text">添加到比较</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="wrapper">
                                            <ul class="social-icon-square">
                                                <li><a class="icon-01" href="#"></a></li>
                                                <li><a class="icon-02" href="#"></a></li>
                                                <li><a class="icon-03" href="#"></a></li>
                                                <li><a class="icon-04" href="#"></a></li>
                                                <li><a class="icon-05" href="#"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/modal-quick-view-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal  fade"  id="Modalnewsletter" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"  data-pause=2000>
        <div class="modal-dialog modal-md-middle">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    <h4 class="modal-title text-center text-uppercase">获取最新消息<br>DELIVERED DAILY!</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <!--modal-add-cart-->
                        <div class="modal-newsletter">
                            <p>
                                请给我们您的电子邮件，我们将每天向您详细更新< br >最新事件 !
                            </p>
                            <div  class="subscribe-form">
                                <div class="row-subscibe">
                                    <div class="col-left">
                                        <div class="input-group">
                                                <span class="input-group-addon">
                                                <span class="icon icon-email"></span>
                                                </span>
                                            <input type="text" class="form-control" placeholder="Enter please your e-mail">
                                        </div>
                                    </div>
                                    <div class="col-right">
                                        <button type="submit" class="btn btn-fill">订阅</button>
                                    </div>
                                </div>
                                <div class="checkbox-group form-group-top clearfix">
                                    <input type="checkbox" id="checkBox1">
                                    <label for="checkBox1">
                                        <span class="check"></span>
                                        <span class="box"></span>
                                        不要再显示这个弹出窗口
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!--/modal-add-cart-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal  fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md-small">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    <h4 class="modal-title text-center text-uppercase">登录表单</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <!--modal-add-login-->
                        <div class="modal-login">
                            <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                        <span class="icon icon-person_outline"></span>
                                        </span>
                                    <input type="text" id="LoginFormName" class="form-control" placeholder="Name:">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                        <span class="icon icon-lock_outline"></span>
                                        </span>
                                    <input type="password" id="LoginFormPass" class="form-control" placeholder="Password:">
                                </div>
                            </div>
                            <div class="checkbox-group">
                                <input type="checkbox" id="checkBox2">
                                <label for="checkBox2">
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    记得我吗
                                </label>
                            </div>
                            <button type="button" class="btn btn-full">签到</button>
                            <button type="button" class="btn btn-full">CREATE AN ACCOUNT</button>
                            <div class="text-center"></div>
                            <div class="social-icon-fill">
                                <div>或者登录社交网站</div>
                                <ul>
                                    <li><a class="icon bg-facebook fa fa-facebook" href="http://www.facebook.com/"></a></li>
                                    <li><a class="bg-twitter fa fa-twitter" href="http://www.twitter.com/"></a></li>
                                    <li><a class="bg-google-plus fa fa-google-plus" href="http://www.google.com/"></a></li>
                                </ul>
                            </div>
                            <ul class="link-functional">
                                <li><a href="#">忘记你的用户名了?</a></li>
                                <li><a href="#">忘了你的密码?</a></li>
                            </ul>
                        </div>
                        <!--/modal-add-login-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal  fade"  id="modalAddToCart" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                </div>
                <div class="modal-body">
                    <!--  -->
                    <div class="modal-add-cart">
                        <span class="icon color-base icon-check_circle"></span>
                        <p>
                            已成功添加到购物车 !
                        </p>
                        <a class="btn btn-underline color-defaulttext2" href="#">转到购物车</a>
                    </div>
                    <!-- / -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal  fade"  id="modalAddToCartProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                </div>
                <div class="modal-body">
                    <!--  -->
                    <div class="modal-add-cart-product desctope">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="modal-messages">
                                    <span class="icon color-base icon-check_circle"></span>
                                    <p>
                                        产品已成功添加到购物车
                                    </p>
                                </div>
                                <div class="modal-product">
                                    <div class="image-box">
                                        <img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt="">
                                    </div>
                                    <div class="title">
                                        黛西街3 / 4袖板式休闲衬衫
                                    </div>
                                    <div class="description">
                                        Black,  Xl
                                    </div>
                                    <div class="qty">
                                        Qty: 1
                                    </div>
                                </div>
                                <div class="total">
                                    TOTAL: <span>$135</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="cart-item-total">
                                    <a href="shopping_cart_01.html">
                                        <div class="cart-item-icon">
                                            <span class="icon icon-shopping_basket"></span>View Cart
                                        </div>
                                    </a>
                                    <p>
                                        共有3个项目<br> 在你的车上
                                    </p>
                                </div>
                                <div class="total-product">
                                    产品总数: <span>$245</span>
                                </div>
                                <div class="total">
                                    TOTAL: <span>$135</span>
                                </div>
                                <a href="#" class="btn invert">继续购物</a>
                                <a href="#" class="btn">继续结账</a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-add-cart-product mobile">
                        <div class="modal-messages">
                            <span class="icon color-base icon-check_circle"></span>
                            <p>
                                已成功添加到购物车 !
                            </p>
                        </div>
                        <a href="#" class="btn btn-underline">转到购物车 !</a>
                    </div>
                    <!-- / -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal-compare">
        <div class="container">
            <a href="#" class="icon icon-close button-close"></a>
            <div class="title-top">比较</div>
            <div class="row-content">
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                </div>
            </div>
            <div class="row-button">
                <a href="#" class="clear-all btn btn-lg btn-underline"><span>全部清除</span></a>
                <a href="#" class="btn btn-inversion btn-compare"><span class="fa fa-balance-scale"></span>比较</a>
            </div>
        </div>
    </div>

    <div class="modal-wishlist">
        <div class="container">
            <a href="#" class="icon icon-close button-close"></a>
            <div class="title-top">我的愿望清单</div>
            <div class="row-content">
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
                <div class="item">
                    <a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
                    <a href="#" class="title">黛西街3 / 4袖板式休闲衬衫</a>
                    <div class="price">
                        $45
                    </div>
                    <a href="#" class="icon icon-delete"></a>
                    <a href="#" class="icon icon-check icon-check_circle"></a>
                </div>
            </div>
            <div class="row-button">
                <a href="#" class="clear-all btn btn-lg btn-underline"><span>全部清除</span></a>
            </div>
        </div>
    </div>
@endsection
