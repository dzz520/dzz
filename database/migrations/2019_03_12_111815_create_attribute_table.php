<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attr', function (Blueprint $table) {
            $table->increments('attr_id')->comment('属性id');
            $table->string('attr_name', 50)->comment('属性名称');
            $table->integer('type_id')->comment('类型id')->nullable();
        });

        Schema::create('goods_attr', function (Blueprint $table) {
            $table->increments('goods_attr_id')->commit('自增id');
            $table->integer('goods_id')->comment('商品id');
            $table->integer('attr_id')->comment('属性名id');
            $table->integer('value_id')->comment('属性值id');
        });

        Schema::create('attr_value', function (Blueprint $table) {
            $table->increments('value_id')->comment('自增id');
            $table->integer('attr_id')->comment('属性id');
            $table->string('attr_value', 50)->comment('属性值');
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attr');
        Schema::dropIfExists('goods_attr');
        Schema::dropIfExists('attr_value');
    }
}
