<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Session;
use DB;
use Log;

class Goods extends Model
{
    protected $table = 'goods_show';

    /**
     * 添加商品
     */
    public function add($data)
    {        
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        $this->save();
        //获取添加商品的ID
        return DB::getPdo()->lastInsertId();    
    }

    /**
    * 添加多张商品图片
    */
    public function addImg($data)
    {
        return DB::table('goods_image')->insert($data);
    }

    /**
    * 检测商品名称是否存在
    */
    public static function isExistName($goodsName, $goodsId = 0)
    {

        if($goodsId){
            $result = self::where('id', '!=', $goodsId)->where('goods_name', $goodsName)->first();    
        }else{
            $result = self::where('goods_name', $goodsName)->first();    
        }
        
        return $result ? true : false;

    }


    /**
    * 删除商品信息
    */
    public function deleteGoodsById($goodsId)
    {
        $goods = $this->find($goodsId);
        
        $goods->goods_state = '3';

        return $goods->save();
    }

    /**
    * 查询商品信息
    */
    public function selectGoods($id)
    {
        $data = DB::table("goods_show")->where('id',$id)->get()->map(function ($value) {
                return (array)$value;
            })->toArray();

        return $data;
    }

    /**
    * 修改方法
    */
    public function updateGoods($data=[],$goods=[])
    {
        foreach ($goods[0] as $k => $v) {
            foreach ($data as $key => $val) {
                if ($k == $key && $v != $val && $val != "") {
                    $update = DB::table("goods_show")
                                    ->where("id",$goods[0]['id'])
                                    ->update([$key=>$val]);

                }
            }
        }

        return $update;
    }

    /**
     *添加库存
     */
    public function repertory($data,$attr_value)
    {
        //货品表
        foreach ($data as $key => $val) {
            //入库
            $res = DB::table('cargo')->insert($val);
        }
        
        //商品，属性名，属性值的关联关系表
        foreach ($attr_value as $v) {
            $res = DB::table('goods_attr')->insert($v);
        }
        
        return $res;
    }

    /**
     * 获取一条商品信息
     */
    public static function findGoods($goodsId)
    {        
        return self::where('id', $goodsId)->first();
    }
}