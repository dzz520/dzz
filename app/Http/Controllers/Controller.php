<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ajaxSuccess($message, $data = '')
    {
        $result = [
            'message' => $message,
            'data' => $data,
            'status' => 'success'
        ];

        return json_encode($result);
    }

    public function ajaxError($message, $data = '')
    {
        $result = [
            'message' => $message,
            'data' => $data,
            'status' => 'error'
        ];

        return json_encode($result);
    }
}
