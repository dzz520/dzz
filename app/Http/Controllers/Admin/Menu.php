<?php

namespace App\Http\Controllers\Admin;

use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use App\Models\Permission;
use App\Models\Admins;

class Menu implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        $admin = Admins::currentAdmin();

        // 必定显示以及超级管理员处理
        if((isset($item['is_show']) && $item['is_show'] === true) || $admin->is_super == 1){
            return $item;
        }

        if(empty($item['permission_name']) || !Permission::isExsitName($item['permission_name']) || !$admin->hasPermissionTo($item['permission_name'])){
            return false;
        }

        return $item;
    }
}
