<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsShowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_show', function (Blueprint $table) {
            $table->increments('id');
            $table->string('goods_name', 100)->comment('商品名称');
            $table->decimal('goods_price', 10,2)->comment('商品价格');
            $table->string('goods_desc', 300)->comment('商品信息');
            $table->datetime('up_time')->default('0000-01-01 00:00:00')->comment('上架日期');
            $table->char('hotgoods', 10)->default('0')->comment('热销商品,默认为0');
            $table->integer('type_id')->comment('类型Id');
            $table->char('goods_state', 10)->default('1')->comment('商品状态,默认为1,1-在售。2-下架。3-删除');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_show');
    }
}

