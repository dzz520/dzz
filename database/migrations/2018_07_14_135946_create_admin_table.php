<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 10)->comment('真实姓名');
            $table->char('key', 8)->comment('加密盐');
            $table->char('password', 32)->comment('密码');
            $table->string('email', 60)->comment('管理员邮箱');
            $table->char('mobile', 11)->default('')->comment('管理员手机号');
            $table->tinyInteger('is_super')->default(0)->comment('是否为超级管理员');
            $table->tinyInteger('is_freeze')->default(0)->comment('账号是否冻结');
            $table->string('create_user_name', 10)->comment('创建该账号的管理员');
            $table->datetime('last_login_time')->default('0000-01-01 00:00:00')->comment('最后一次登录时间');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_admins');
    }
}
