@extends('web.layouts.common')
@section('common')
	<body>
		<div class="loader-wrapper">
			<div class="loader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="loader-animation" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
				</svg>
			</div>
		</div>
		<nav class="panel-menu">
			<ul>
				<li>
					<a href="index.html">LAYOUT</a>
					<ul>
						<li><a href="index.html">Home page 1</a></li>
						<li><a href="index-02.html">Home page 2</a></li>
						<li><a href="index-03.html">Home page 3</a></li>
						<li><a href="index-04.html">Home page 4</a></li>
						<li><a href="index-05.html">Home page 5</a></li>
						<li><a href="index-06.html">Home page 6</a></li>
						<li><a href="index-07.html">Home page 7</a></li>
						<li><a href="index-08.html">Home page 8</a></li>
						<li><a href="index-09.html">Home page 9</a></li>
						<li><a href="index-10.html">Home page 10</a></li>
						<li><a href="index-11.html">Home page 11</a></li>
						<li><a href="index-12.html">Home page 12</a></li>
					</ul>
				</li>
				<li><a href="index-rtl.html">RTL</a></li>
				<li>
					<a href="listing-left-column.html">LISTING</a>
					<ul>
						<li><a href="listing-left-column.html">Left Column</a></li>
						<li><a href="listing-left-right-column.html">Left and Right Column</a></li>
						<li><a href="listing-right-column.html">Right Column</a></li>
						<li><a href="listing-left-column-without-block.html">Left Column Without Block</a></li>
						<li><a href="listing-without-columns.html">Without Columns</a></li>
						<li><a href="listing-without-columns-items6.html">Without Column and 6 items in line</a></li>
						<li><a href="listing-without-columns-items4.html">Without Column and 4 items in line</a></li>
						<li><a href="listing-without-columns-items3.html">Without Column and 3 items in line</a></li>
						<li><a href="listing-without-columns-items2.html">Without Column and 2 items in line</a></li>
					</ul>
				</li>
				<li>
					<a href="product.html">PRODUCT</a>
					<ul>
						<li><a href="product.html">Image Size - Small</a></li>
						<li><a href="product-02.html">Image Size - Medium</a></li>
						<li><a href="product-03.html">Image Size - Big</a></li>
						<li><a href="product-04.html">Image Size - Medium</a></li>
						<li><a href="product-05.html">Image Size - Long</a></li>
					</ul>
				</li>
				<li>
					<a href="blog_listing.html">BLOG</a>
					<ul>
						<li><a href="blog_listing.html">Listing</a></li>
						<li><a href="blog_grid_col_2.html">Grid Layout 2 cols</a></li>
						<li><a href="blog_grid_col_3.html">Grid Layout 3 cols</a></li>
						<li><a href="blog_masonry_col_2.html">Masonry Layout 2 cols</a></li>
						<li><a href="blog_masonry_col_3.html">Masonry Layout 3 cols</a></li>
						<li><a href="blog_single_post.html">Blog single post 1</a></li>
						<li><a href="blog_single_post_01.html">Blog single post 2</a></li>
					</ul>
				</li>
				<li>
					<a href="gallery_masonry_col_3.html">GALLERY</a>
					<ul>
						<li><a href="gallery_grid_col_2.html">Grid col 2</a></li>
						<li><a href="gallery_grid_col_3.html">Grid col 3</a></li>
						<li><a href="gallery_masonry_col_2.html">Masonry col 2</a></li>
						<li><a href="gallery_masonry_col_3.html">Masonry col 3</a></li>
					</ul>
				</li>
				<li>
					<a href="about.html">PAGES</a>
					<ul>
						<li><a href="about.html">About</a></li>
						<li><a href="about_01.html">About 2</a></li>
						<li><a href="contact.html">Contacts</a></li>
						<li><a href="contact_01.html">Contacts 2</a></li>
						<li><a href="comming-soon.html">Under Construction</a></li>
						<li><a href="look-book.html">Lookbook</a></li>
						<li><a href="collections.html">Collections</a></li>
						<li><a href="typography.html">Typography</a></li>
						<li><a href="infographic.html">Infographic</a></li>
						<li><a href="faqs.html">Delivery Page</a></li>
						<li><a href="faqs.html">Payment page</a></li>
						<li><a href="checkout.html">Checkout</a></li>
						<li><a href="compare.html">Compare</a></li>
						<li><a href="wishlist.html">Wishlist</a></li>
						<li><a href="shopping_cart_01.html">Shopping cart 01</a></li>
						<li><a href="shopping_cart_02.html">Shopping cart 02</a></li>
						<li><a href="account.html">Page Account</a></li>
						<li><a href="account-address.html">Page Account address</a></li>
						<li><a href="account-order.html">Page Account order</a></li>
						<li><a href="login-form.html">Login form</a></li>
						<li><a href="faqs.html">Support page</a></li>
						<li><a href="faqs.html">FAQs</a></li>
						<li><a href="page-404.html">Page 404</a></li>
						<li><a href="page-empty-category.html">Page Empty Category</a></li>
						<li><a href="page-empty-search.html">Page Empty Search</a></li>
						<li><a href="page-empty-shopping-cart.html">Page Empty shopping Cart</a></li>
					</ul>
				</li>
				<li>
					<a href="listing-left-column.html">WOMEN'S</a>
					<ul>
						<li>
							<a href="listing-left-column.html">TOPS</a>
							<ul>
								<li><a href="listing-left-column.html">Blouses & Shirts</a></li>
								<li><a href="listing-left-column.html">Dresses</a></li>
								<li>
									<a href="listing-left-column.html">Tops & T-shirts</a>
									<ul>
										<li><a href="listing-left-column.html">Link level 1</a></li>
										<li>
											<a href="listing-left-column.html">Link level 1</a>
											<ul>
												<li><a href="listing-left-column.html">Link level 2</a></li>
												<li>
													<a href="listing-left-column.html">Link level 2</a>
													<ul>
														<li><a href="listing-left-column.html">Link level 3</a></li>
														<li><a href="listing-left-column.html">Link level 3</a></li>
														<li><a href="listing-left-column.html">Link level 3</a></li>
														<li>
															<a href="listing-left-column.html">Link level 3</a>
															<ul>
																<li>
																	<a href="listing-left-column.html">Link level 4</a>
																	<ul>
																		<li><a href="listing-left-column.html">Link level 5</a></li>
																		<li><a href="listing-left-column.html">Link level 5</a></li>
																		<li><a href="listing-left-column.html">Link level 5</a></li>
																		<li><a href="listing-left-column.html">Link level 5</a></li>
																		<li><a href="listing-left-column.html">Link level 5</a></li>
																	</ul>
																</li>
																<li><a href="listing-left-column.html">Link level 4</a></li>
																<li><a href="listing-left-column.html">Link level 4</a></li>
																<li><a href="listing-left-column.html">Link level 4</a></li>
																<li><a href="listing-left-column.html">Link level 4</a></li>
															</ul>
														</li>
														<li><a href="listing-left-column.html">Link level 3</a></li>
													</ul>
												</li>
												<li><a href="listing-left-column.html">Link level 2</a></li>
												<li><a href="listing-left-column.html">Link level 2</a></li>
												<li><a href="listing-left-column.html">Link level 2</a></li>
											</ul>
										</li>
										<li><a href="listing-left-column.html">Link level 1</a></li>
										<li><a href="listing-left-column.html">Link level 1</a></li>
										<li><a href="listing-left-column.html">Link level 1</a></li>
									</ul>
								</li>
								<li><a href="listing-left-column.html">Sleeveless Tops</a></li>
								<li><a href="listing-left-column.html">Sweaters</a></li>
								<li><a href="listing-left-column.html">Jackets</a></li>
								<li><a href="listing-left-column.html">Outerwear</a></li>
							</ul>
						</li>
						<li>
							<a href="listing-left-column.html">BOTTOMS</a>
							<ul>
								<li><a href="listing-left-column.html">Trousers</a></li>
								<li><a href="listing-left-column.html">Jeans</a></li>
								<li><a href="listing-left-column.html">Leggings</a></li>
								<li><a href="listing-left-column.html">Jumpsuit & shorts</a></li>
								<li><a href="listing-left-column.html">Skirts</a></li>
								<li><a href="listing-left-column.html">Tights</a></li>
							</ul>
						</li>
						<li>
							<a href="listing-left-column.html">ACCESSORIES</a>
							<ul>
								<li><a href="listing-left-column.html">Jewellery</a></li>
								<li><a href="listing-left-column.html">Hats</a></li>
								<li><a href="listing-left-column.html">Scarves & snoods</a></li>
								<li><a href="listing-left-column.html">Belts</a></li>
								<li><a href="listing-left-column.html">Bags</a></li>
								<li><a href="listing-left-column.html">Shoes</a></li>
								<li><a href="listing-left-column.html">Sunglasses</a></li>
							</ul>
						</li>
						<li>
							<a href="listing-left-column.html">SPECIALS</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="listing-right-column.html">MEN'S</a>
					<ul>
						<li>
							<a href="listing-right-column.html">TOPS</a>
							<ul>
								<li><a href="listing-right-column.html">Blouses & Shirts</a></li>
								<li><a href="listing-right-column.html">Dresses</a></li>
								<li>
									<a href="listing-right-column.html">Tops & T-shirts</a>
									<ul>
										<li><a href="listing-right-column.html">Link level 1</a></li>
										<li>
											<a href="listing-right-column.html">Link level 1</a>
											<ul>
												<li><a href="listing-right-column.html">Link level 2</a></li>
												<li>
													<a href="listing-right-column.html">Link level 2</a>
													<ul>
														<li><a href="listing-right-column.html">Link level 3</a></li>
														<li><a href="listing-right-column.html">Link level 3</a></li>
														<li><a href="listing-right-column.html">Link level 3</a></li>
														<li>
															<a href="listing-right-column.html">Link level 3</a>
															<ul>
																<li>
																	<a href="listing-right-column.html">Link level 4</a>
																	<ul>
																		<li><a href="listing-right-column.html">Link level 5</a></li>
																		<li><a href="listing-right-column.html">Link level 5</a></li>
																		<li><a href="listing-right-column.html">Link level 5</a></li>
																		<li><a href="listing-right-column.html">Link level 5</a></li>
																		<li><a href="listing-right-column.html">Link level 5</a></li>
																	</ul>
																</li>
																<li><a href="listing-right-column.html">Link level 4</a></li>
																<li><a href="listing-right-column.html">Link level 4</a></li>
																<li><a href="listing-right-column.html">Link level 4</a></li>
																<li><a href="listing-right-column.html">Link level 4</a></li>
															</ul>
														</li>
														<li><a href="listing-right-column.html">Link level 3</a></li>
													</ul>
												</li>
												<li><a href="listing-right-column.html">Link level 2</a></li>
												<li><a href="listing-right-column.html">Link level 2</a></li>
												<li><a href="listing-right-column.html">Link level 2</a></li>
											</ul>
										</li>
										<li><a href="listing-right-column.html">Link level 1</a></li>
										<li><a href="listing-right-column.html">Link level 1</a></li>
										<li><a href="listing-right-column.html">Link level 1</a></li>
									</ul>
								</li>
								<li><a href="listing-right-column.html">Sleeveless Tops</a></li>
								<li><a href="listing-right-column.html">Sweaters</a></li>
								<li><a href="listing-right-column.html">Jackets</a></li>
								<li><a href="listing-right-column.html">Outerwear</a></li>
							</ul>
						</li>
						<li>
							<a href="listing-right-column.html">BOTTOMS</a>
							<ul>
								<li><a href="listing-right-column.html">Trousers</a></li>
								<li><a href="listing-right-column.html">Jeans</a></li>
								<li><a href="listing-right-column.html">Leggings</a></li>
								<li><a href="listing-right-column.html">Jumpsuit & shorts</a></li>
								<li><a href="listing-right-column.html">Skirts</a></li>
								<li><a href="listing-right-column.html">Tights</a></li>
							</ul>
						</li>
						<li>
							<a href="listing-right-column.html">ACCESSORIES</a>
							<ul>
								<li><a href="listing-right-column.html">Jewellery</a></li>
								<li><a href="listing-right-column.html">Hats</a></li>
								<li><a href="listing-right-column.html">Scarves & snoods</a></li>
								<li><a href="listing-right-column.html">Belts</a></li>
								<li><a href="listing-right-column.html">Bags</a></li>
								<li><a href="listing-right-column.html">Shoes</a></li>
								<li><a href="listing-right-column.html">Sunglasses</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="#">BUY TEMPLATE</a></li>
			</ul>
			<div class="mm-navbtn-names" style="display:none">
				<div class="mm-closebtn">CLOSE</div>
				<div class="mm-backbtn">BACK</div>
			</div>
		</nav>
	
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="index.html">Home</a></li>
					<li>Gallery</li>
				</ul>
			</div>
		</div>
		<!-- Content -->
		<div id="pageContent">
			<div class="container-fluid offset-18">
				<h1 class="block-title large">GALLERY</h1>
				<div class="gallery">
					<div class="filter-nav">
						<div rel="all" class="current">ALL</div>
						<div rel="woman">WOMAN</div>
						<div rel="man">MAN</div>
					</div>
					<div class="gallery-content row effect-1">
						<div class="item all man col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-01.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-01.jpg')}}" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="item all man col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-02.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-02.jpg')}}" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="item all man col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-03.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-03.jpg')}}" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="item all man col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-04.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="images/gallery/gallery-large/gallery-img-04.jpg" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="item all woman col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-05.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-05.jpg')}}" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="item all woman col-sm-6">
							<figure>
								<img src="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-06.jpg')}}" class="all man" alt="" />
								<figcaption>
									<div class="block-table">
										<div class="block-table-cell">
											<a href="#" class="tag">Fashion</a>
											<h6 class="title"><a href="#">Resposive Retina Ready</a></h6>
											<p>
												Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut.
											</p>
											<div class="btn-icon">
												<a href="{{asset('vendor/weblte/images/gallery/gallery-large/gallery-img-06.jpg')}}" class="zomm-gallery"  title="Resposive Retina Ready"></a>
											</div>
										</div>
									</div>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<!-- Modal (quickViewModal) -->
		<div class="modal  fade"  id="ModalquickView" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
					</div>
					<form>
						<div class="modal-body">
							<!--modal-quick-view-->
							<div class="modal-quick-view">
								<div class="row">
									<div class="col-sm-5 col-lg-6">
										<div class="product-main-image">
											<img src="{{asset('vendor/weblte/images/product/product-big-1.jpg')}}" alt="" />
										</div>
									</div>
									<div class="col-sm-7 col-lg-6">
										<div class="product-info">
											<div class="add-info">
												<div class="sku pull-left">
													<span class="font-weight-medium color-defaulttext2">SKU:</span> mtk012c
												</div>
												<div class="availability pull-left">
													<span class="font-weight-medium color-defaulttext2">Availability:</span> <span class="color-red">In Stock</span> <span class="color-base">Out stock</span>
												</div>
											</div>
											<h1 class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</h1>
											<div class="price">
												$45
											</div>
											<div class="description hidden-xs">
												<div class="text">
													Silver, metallic-blue and metallic-lavender silk-blend jacquard, graphic pattern, pleated ruffle along collar, long sleeves with button-fastening cuffs, buckle-fastening silver skinny belt, large pleated rosettes at hips.
												</div>
											</div>
											<div class="wrapper">
												<div class="title-options">Texture <span class="color-required">*</span></div>
												<ul class="options-swatch-texture">
													<li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-01.jpg')}}" alt=""></a></li>
													<li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-02.jpg')}}" alt=""></a></li>
													<li><a href="#"><img src="{{asset('vendor/weblte/images/custom/texture-img-03.jpg')}}" alt=""></a></li>
												</ul>
											</div>
											<div class="wrapper">
												<div class="title-options">Size <span class="color-required">*</span></div>
												<ul class="tags-list">
													<li><a href="#">XS</a></li>
													<li class="active"><a href="#">S</a></li>
													<li><a href="#">M</a></li>
													<li><a href="#">L</a></li>
												</ul>
											</div>
											<div class="wrapper">
												<div class="pull-left"><label class="qty-label">qty</label></div>
												<div class="pull-left">
													<div class="style-2 input-counter">
														<span class="minus-btn"></span>
														<input type="text" value="1" size="5"/>
														<span class="plus-btn"></span>
													</div>
												</div>
												<div class="pull-left">
													<a href="#" class="btn btn-addtocart"><span class="icon icon-shopping_basket"></span>SHOP NOW!</a>
												</div>
											</div>
											<div class="wrapper">
												<div class="pull-left">
													<ul class="product_inside_info_link">
														<li class="text-right">
															<a href="#">
															<span class="fa fa-heart-o"></span>
															<span class="text">ADD TO WISHLIST</span>
															</a>
														</li>
														<li class="text-left">
															<a href="#" class="compare-link">
															<span class="fa fa-balance-scale"></span>
															<span class="text">ADD TO COMPARE</span>
															</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="wrapper">
												<ul class="social-icon-square">
													<li><a class="icon-01" href="#"></a></li>
													<li><a class="icon-02" href="#"></a></li>
													<li><a class="icon-03" href="#"></a></li>
													<li><a class="icon-04" href="#"></a></li>
													<li><a class="icon-05" href="#"></a></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--/modal-quick-view-->
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- / Modal (quickViewModal) -->
		<!-- Modal (newsletter) -->
		<div class="modal  fade"  id="Modalnewsletter" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"  data-pause=2000>
			<div class="modal-dialog modal-md-middle">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
						<h4 class="modal-title text-center text-uppercase">GET THE LATEST NEWS<br>DELIVERED DAILY!</h4>
					</div>
					<form>
						<div class="modal-body">
							<!--modal-add-cart-->
							<div class="modal-newsletter">
								<p>
									Give us your email and you will be daily updated with <br>the latest events, in detail!
								</p>
								<div  class="subscribe-form">
									<div class="row-subscibe">
										<div class="col-left">
											<div class="input-group">
												<span class="input-group-addon">
												<span class="icon icon-email"></span>
												</span>
												<input type="text" class="form-control" placeholder="Enter please your e-mail">
											</div>
										</div>
										<div class="col-right">
											<button type="submit" class="btn btn-fill">SUBSCRIBE</button>
										</div>
									</div>
									<div class="checkbox-group form-group-top clearfix">
										<input type="checkbox" id="checkBox1">
										<label for="checkBox1">
										<span class="check"></span>
										<span class="box"></span>
										Don’t show this popup again
										</label>
									</div>
								</div>
							</div>
							<!--/modal-add-cart-->
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- / Modal (newsletter) -->
		<!-- modalLoginForm-->
		<div class="modal  fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-md-small">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
						<h4 class="modal-title text-center text-uppercase">Login form</h4>
					</div>
					<form>
						<div class="modal-body">
							<!--modal-add-login-->
							<div class="modal-login">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-person_outline"></span>
										</span>
										<input type="text" id="LoginFormName" class="form-control" placeholder="Name:">
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
										<span class="icon icon-lock_outline"></span>
										</span>
										<input type="password" id="LoginFormPass" class="form-control" placeholder="Password:">
									</div>
								</div>
								<div class="checkbox-group">
									<input type="checkbox" id="checkBox2">
									<label for="checkBox2">
									<span class="check"></span>
									<span class="box"></span>
									Remember me
									</label>
								</div>
								<button type="button" class="btn btn-full">SIGN IN</button>
								<button type="button" class="btn btn-full">CREATE AN ACCOUNT</button>
								<div class="text-center"></div>
								<div class="social-icon-fill">
									<div>Or sign in with social</div>
									<ul>
										<li><a class="icon bg-facebook fa fa-facebook" href="http://www.facebook.com/"></a></li>
										<li><a class="bg-twitter fa fa-twitter" href="http://www.twitter.com/"></a></li>
										<li><a class="bg-google-plus fa fa-google-plus" href="http://www.google.com/"></a></li>
									</ul>
								</div>
								<ul class="link-functional">
									<li><a href="#">Forgot your username?</a></li>
									<li><a href="#">Forgot your password?</a></li>
								</ul>
							</div>
							<!--/modal-add-login-->
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /modalLoginForm-->
		<!-- modalAddToCart -->
		<div class="modal  fade"  id="modalAddToCart" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
					</div>
					<div class="modal-body">
						<!--  -->
						<div class="modal-add-cart">
							<span class="icon color-base icon-check_circle"></span>
							<p>
								Added to cart successfully!
							</p>
							<a class="btn btn-underline color-defaulttext2" href="#">GO TO CART</a>
						</div>
						<!-- / -->
					</div>
				</div>
			</div>
		</div>
		<!-- /modalAddToCart -->
		<!-- modalAddToCartProduct -->
		<div class="modal  fade"  id="modalAddToCartProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content ">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
					</div>
					<div class="modal-body">
						<!--  -->
						<div class="modal-add-cart-product desctope">
							<div class="row">
								<div class="col-sm-6">
									<div class="modal-messages">
										<span class="icon color-base icon-check_circle"></span>
										<p>
											Product successfully added to your shopping cart
										</p>
									</div>
									<div class="modal-product">
										<div class="image-box">
											<img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt="">
										</div>
										<div class="title">
											Daisy Street 3/4 Sleeve Panelled Casual Shirt
										</div>
										<div class="description">
											Black,  Xl
										</div>
										<div class="qty">
											Qty: 1
										</div>
									</div>
									<div class="total">
										TOTAL: <span>$135</span>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="cart-item-total">
										<a href="shopping_cart_01.html">
											<div class="cart-item-icon">
												<span class="icon icon-shopping_basket"></span>View Cart
											</div>
										</a>
										<p>
											There are 3 items<br> in your cart
										</p>
									</div>
									<div class="total-product">
										Total products: <span>$245</span>
									</div>
									<div class="total">
										TOTAL: <span>$135</span>
									</div>
									<a href="#" class="btn invert">CONTINUE SHOPPING</a>
									<a href="#" class="btn">PROCEED TO CHECKOUT</a>
								</div>
							</div>
						</div>
						<div class="modal-add-cart-product mobile">
							<div class="modal-messages">
								<span class="icon color-base icon-check_circle"></span>
								<p>
									Added to cart successfully!
								</p>
							</div>
							<a href="#" class="btn btn-underline">GO TO CART!</a>
						</div>
						<!-- / -->
					</div>
				</div>
			</div>
		</div>
		<!-- /modalAddToCartProduct -->
		<!-- modalCompare -->
		<div class="modal-compare">
			<div class="container">
				<a href="#" class="icon icon-close button-close"></a>
				<div class="title-top">COMPARE</div>
				<div class="row-content">
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="images/product/product-01.jpg" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
					</div>
				</div>
				<div class="row-button">
					<a href="#" class="clear-all btn btn-lg btn-underline"><span>CLEAR ALL</span></a>
					<a href="#" class="btn btn-inversion btn-compare"><span class="fa fa-balance-scale"></span>COMPARE</a>
				</div>
			</div>
		</div>
		<!-- /modalCompare -->
		<!-- modalWishlist -->
		<div class="modal-wishlist">
			<div class="container">
				<a href="#" class="icon icon-close button-close"></a>
				<div class="title-top">MY WISHLIST</div>
				<div class="row-content">
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
					<div class="item">
						<a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
						<a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
						<div class="price">
							$45
						</div>
						<a href="#" class="icon icon-delete"></a>
						<a href="#" class="icon icon-check icon-check_circle"></a>
					</div>
				</div>
				<div class="row-button">
					<a href="#" class="clear-all btn btn-lg btn-underline"><span>CLEAR ALL</span></a>
				</div>
			</div>
		</div>
@endsection		