@extends('web.layouts.common')
@section('common')
	<div id="pageContent">
		<div class="container offset-80">
			<div class="on-duty-box">
				<img src="{{asset('images/empty-404-icon.png')}}" alt="">
				<br>
				<a class="btn btn-border color-default" href="/">首页</a>
			</div>
		</div>
	</div>
@endsection