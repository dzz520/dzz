<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Validator;
use App\Models\Admins;
use DB;

class CreateSuperAdministrator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create {--email=} {--password=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建超级管理员账户';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->option('email');
        $password = $this->option('password');

        // 验证邮箱
        $validator = Validator::make(['email' => $email], ['email' => 'email'], [
            'email.email' => '邮箱格式不正确'
        ]);
        if ($validator->errors()->has('email')) {
            return $this->error($validator->errors()->first('email'));
        }

        $adminObj = new Admins;
        $superAdmin = $adminObj->where('is_super', 1)->first();
        if(!empty($superAdmin)){
            return $this->error('超级管理员已存在');
        }

        $adminObj->name = 'admin';
        $adminObj->email = $email;
        $adminObj->key = str_random(8);
        $adminObj->password = md5(md5($password).$adminObj->key);
        $adminObj->is_super = 1;
        $adminObj->create_user_name = 'system';
        $adminObj->save();

        return $this->info('创建超级管理员账户成功');
    }
}
