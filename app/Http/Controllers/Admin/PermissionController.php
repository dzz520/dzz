<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;
use Validator;

class PermissionController extends Controller
{
    /**
     * 权限列表
     */
    public function list()
    {
        return view('admin.permission.list', [
            'content_header' => '权限管理',
            'box_title' => '权限列表'
        ]);
    }

    /**
     * 获取权限数据
     */
    public function data()
    {
        $query = Permission::orderBy('path', 'asc');
        $data = datatables()->of($query)
            ->editColumn('cn_name', function($data){
                $count = substr_count($data->path, '-');
                return str_repeat('|——', $count).$data->cn_name;
            })
            ->toJson();

        return $data;
    }

    /**
     * 添加角色
     */
    public function add()
    {
        $parentPermissions = (new Permission)->getParentPermissions();

        return view('admin.permission.edit', [
            'content_header' => '权限管理',
            'box_title' => '添加权限',
            'parentPermissions' => $parentPermissions
        ]);
    }

    /**
     * 编辑角色
     */
    public function edit(Request $request)
    {
        $permissionId = intval($request->id);
        if($permissionId <= 0){
            return back();
        }
        $permission = Permission::findById($permissionId);
        $parentPermissions = (new Permission)->getParentPermissions();

        return view('admin.permission.edit', [
            'content_header' => '权限管理',
            'box_title' => '编辑权限',
            'permission' => $permission,
            'parentPermissions' => $parentPermissions
        ]);
    }

    /**
     * 检测名称
     */
    public function check(Request $request)
    {
        $result = true;
        $permissionId = intval($request->id);
        switch($request->type){
            case 'name':
                $name = $request->name;
                if(Permission::isExsitName($name, $permissionId)){
                    $result = false;
                }
                break;
            case 'cn_name':
                $cnName = $request->cn_name;
                if(Permission::isExsitCnName($cnName, $permissionId)){
                    $result = false;
                }
                break;
            case 'controller_action':
                $pid = intval($request->pid);
                $controllerAction = $request->controller_action;
                if(Permission::isExsitControllerAction($permissionId, $pid, $controllerAction)){
                    $result = false;
                }
                break;
            default:
                return $this->ajaxError('非法请求');
        }

        return [
            'valid' => $result
        ];
    }

    /**
     * 保存角色信息
     */
    public function save(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[a-z_]+$/', $value)){
                        return $fail('权限英文名称必须是小写字母和下划线的组合');
                    }
                }
            ],
            'cn_name' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[\x7f-\xff]+$/', $value)){
                        return $fail('权限中文名称必须是汉字');
                    }
                }
            ],
            'controller_action' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[a-z_\/,]+$/', $value)){
                        return $fail('权限uri只能是小写字母、下划线和/组成');
                    }
                }
            ]
        ],[
            'name.required' => '权限英文名称不能为空',
            'cn_name.required' => '权限中文名称不能为空',
            'controller_action.required' => '权限URI不能为空'
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }

        // 验证是否存在
        $permissionId = intval($request->id);
        $pid = intval($request->pid);
        if(Permission::isExsitName($request->name, $permissionId)){
            return $this->ajaxError('角色英文名称已存在');
        }
        if(Permission::isExsitCnName($request->cn_name, $permissionId)){
            return $this->ajaxError('角色中文名称已存在');
        }
        if(Permission::isExsitControllerAction($permissionId, $pid, $request->controller_action)){
            return $this->ajaxError('uri已存在');
        }

        $permissionObj = new Permission;
        $data['name'] = $request->name;
        $data['cn_name'] = $request->cn_name;
        $data['pid'] = $pid;
        $data['controller_action'] = strtolower($request->controller_action);
        $result = $permissionId > 0 ? $permissionObj->edit($permissionId, $data) : $permissionObj->add($data);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }

    /**
     * 删除权限
     */
    public function delete(Request $request)
    {
        $permissionId = intval($request->id);
        if($permissionId <= 0){
            return $this->ajaxError('删除失败');
        }

        $result = (new Permission)->deletePermissionById($permissionId);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');
    }}
