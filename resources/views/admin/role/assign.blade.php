@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{$box_title}} - {{$role['name']}}</h3>
    </div>
    <div class="box-body" style="clear: both">
        <form class="form-horizontal" action="{{url('admin/role/assign')}}" method="post" id="form">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$role['id']}}">  
            <div class="form-group" style="height: 55px">
                @foreach($permissions as $permission)
                <div class="col-sm-10 permissions" style="margin-bottom: 30px;">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">
                        <input type="checkbox" class="flat-red parent-permission" name="permissions[]" {{$permission['is_has'] == 1 ? 'checked' : ''}} value="{{$permission['name']}}">
                        {{$permission['cn_name']}}
                    </label>
                    <div class="col-sm-10 son-permissions">
                        <label style="margin-right: 20px">
                            <input type="checkbox" class="flat-red checkall">
                            <span>全选</span>
                        </label>
                        @foreach($permission['sons'] as $sonPermission)
                        <label>
                            <input type="checkbox" class="flat-red permission" name="permissions[]" {{$sonPermission['is_has'] == 1 ? 'checked' : ''}} value="{{$sonPermission['name']}}">
                            <span>{{$sonPermission['cn_name']}}</span>
                        </label>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
            <div style="clear:both"></div>
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
    
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('js/iCheck/skins/all.css')}}">
@endpush

@push('js')
<script src="{{asset('js/iCheck/icheck.min.js')}}"></script>
<script>
    $(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green',
        })

        $('.checkall').on({
            ifChecked: function(){
                $(this).parents('.permissions').find('.permission').iCheck('check');
                $(this).parents('.permissions').find('.parent-permission').iCheck('check');
            },
            ifUnchecked: function(){
                $(this).parents('.permissions').find('.permission').iCheck('uncheck');
                $(this).parents('.permissions').find('.parent-permission').iCheck('uncheck');
            },
        })

        $('.permission').on({
            ifChecked: function(){
                $(this).parents('.permissions').find('.parent-permission').iCheck('check');
            },
            ifUnchecked: function(){
                var input_checked_size = $(this).parents('.son-permissions').find('input:checkbox:checked').length;
                if(input_checked_size == 0){
                    $(this).parents('.permissions').find('.parent-permission').iCheck('uncheck');
                }
            },
        })

        var success = "{{$success}}";
        if(success != ''){
            $toast.success(success);
        }
    })
</script>
@endpush