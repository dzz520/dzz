@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/current_admin_reset')}}" method="post" id="form">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">原始密码</label>

                    <div class="col-sm-10">
                        <input type="password" name="old_password" class="form-control" id="old_password" placeholder="请输入原始密码" value="">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">新密码</label>

                    <div class="col-sm-10">
                        <input type="password" name="new_password" class="form-control" id="new_password" placeholder="请输入新密码，由6-15位数字、字母及下划线组成" value="">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="mobile" class="col-sm-2 control-label" style="color: #333">确认密码</label>

                    <div class="col-sm-10">
                        <input type="password" name="confirm_password" class="form-control" id="confirm_password" placeholder="请再次输入新密码" value="">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                old_password: {
                    validators: {
                        notEmpty: {message: '请输入原始密码'},    //非空提示
                        stringLength: {    //长度限制
                            min: 6,
                            max: 15,
                            message: '密码长度必须在6到15之间'
                        }, 
                        regexp: {//匹配规则
                            regexp: /^[a-zA-Z0-9_]+$/,  //正则表达式
                            message:'密码仅支持字母、数字、下划线的组合'
                        }
                    }
                },
                new_password: {
                    validators: {
                        notEmpty: {message: '请输入新密码'},    //非空提示
                        stringLength: {    //长度限制
                            min: 6,
                            max: 15,
                            message: '新密码长度必须在6到15之间'
                        }, 
                        regexp: {//匹配规则
                            regexp: /^[a-zA-Z0-9_]+$/,  //正则表达式
                            message:'新密码仅支持字母、数字、下划线的组合'
                        }
                    }
                },
                confirm_password: {
                    validators: {
                        notEmpty: {message: '请再次输入新密码'},    //非空提示
                        stringLength: {    //长度限制
                            min: 6,
                            max: 15,
                            message: '密码长度必须在6到15之间'
                        }, 
                        regexp: {//匹配规则
                            regexp: /^[a-zA-Z0-9_]+$/,  //正则表达式
                            message:'密码仅支持字母、数字、下划线的组合'
                        }
                    }
                },
            }
        }).on('success.form.bv', function(e) {
            // 阻止表单的默认提交
            e.preventDefault();
            var $form = $(e.target);

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/admins')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@stop