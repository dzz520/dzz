<?php

namespace App\Http\Middleware;
use Dingo\Api\Routing\Helpers;

use Closure;

class SignatureVerification
{

    use Helpers;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('sign.debug')){
            return $next($request);
        }

        $timeout = config('sign.sign_timeout');
        if ($timeout != 0) {
            if (!$request->has('sign_time')) {
                return $this->response->errorUnauthorized('签名时间错误');
            }

            $differenceTime = time() - $request->sign_time;
            if ($differenceTime > $timeout) {
                return $this->response->error('Request Timeout', 410);
            }   
        }

        $params = $request->all();
        // ksort($params);
        $paramsStr = '';
        foreach($params as $key => $param){
            if($paramsStr != ''){
                $paramsStr .= '&';
            }
            $paramsStr .= $key.'='.$param;
        }
        $token = md5(md5($paramsStr).config('sign.sign_secret_key'));
        if($token != $request->header('X-Auth-Token')){
            return $this->response->error('Authentication Failed', 401);
        }

        return $next($request);
    }
}
