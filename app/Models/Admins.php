<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Session;
use DB;
use Log;

class Admins extends Model
{
    use HasRoles;

    protected $table = 'sys_admins';

    protected $guard_name = 'web';

    private $defaulPassword = 123456;

    /**
     * 通过邮箱获取管理员信息
     */
    public function getAdminInfoByEmail($email)
    {
        return $this->where('email', $email)->first();
    }

    /**
     * 通过ID获取管理员信息
     */
    public function getAdminInfoById($id)
    {
        return $this->find($id);
    }

    /**
     * 获取管理员角色信息
     */
    public function getAdminRole($admin)
    {
        $roles = $admin->getRoleNames()->toArray();
        if(!empty($roles)){
            return $roles[0];
        }

        return '';
    }

    /**
     * 移除管理员的所有角色
     */
    public function removeAdminRole($admin)
    {
        $role = $this->getAdminRole($admin);
        if(!empty($role)){
            return $admin->removeRole($role);
        }

        return false;
    }

    /**
     * 检测用户名是否存在
     */
    public static function isExistName($adminName, $adminId = null)
    {
        $query = self::where('name', $adminName);
        if($adminId){
            $result = $query->where('id', '!=', $adminId);
        }
        $result = $query->first();
        
        return $result ? true : false;
    }

    /**
     * 检测邮箱是否存在
     */
    public static function isExistEmail($email, $adminId = null)
    {
        $query = self::where('email', $email);
        if($adminId){
            $result = $query->where('id', '!=', $adminId);
        }
        $result = $query->first();

        return $result ? true : false;
    }

    /**
     * 检测手机号是否存在
     */
    public static function isExistMobile($mobile, $adminId = null)
    {
        $query = self::where('mobile', $mobile);
        if($adminId){
            $result = $query->where('id', '!=', $adminId);
        }
        $result = $query->first();

        return $result ? true : false;
    }

    /**
     * 获取当前登录管理员信息
     */
    public static function currentAdmin()
    {
        return Session::get('admin_info');
    }

    /**
     * 添加管理员
     */
    public function add($data)
    {
        foreach($data as $field => $value){
            if($field != 'role'){
                $this->$field = $value;
            }
        }
        $this->key = str_random(8);
        $this->password = md5(md5($this->defaulPassword).$this->key);
        $this->create_user_name = self::currentAdmin()->name;
        
        $result = true;
        DB::beginTransaction();
        try{
            $this->save();
            $this->assignRole($data['role']);

            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('admin:add '.$e->getMessage());
            DB::rollBack();
        }
        
        return $result;
    }

    /**
     * 编辑管理员
     */
    public function edit($adminId, $data)
    {
        $admin = $this->find($adminId);
        foreach($data as $field => $value){
            if($field != 'role'){
                $admin->$field = $value;
            }
        }

        $result = true;
        DB::beginTransaction();
        try{
            $admin->save();
            $admin->removeAdminRole($admin);
            if(isset($data['role'])){
                $admin->assignRole($data['role']);
            }

            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('admin:edit '.$e->getMessage());
            DB::rollBack();
        }
        
        return $result;
    }

    /**
     * 删除管理员
     */
    public function deleteAdminById($adminId)
    {
        $result = true;
        DB::beginTransaction();
        try{
            $admin = $this->getAdminInfoById($adminId);
            $admin->removeAdminRole($admin);
            $admin->delete();

            DB::commit();
        }catch(\Exception $e){
            $result = false;
            Log::error('admin:delete '.$e->getMessage());
            DB::rollBack();
        }
        
        return $result;
    }
}
