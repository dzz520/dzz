<?php

namespace App\Http\Controllers\Web;
use App\Models\Cart;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShoppingCartController extends Controller
{

    public function cart(Request $request)
    {
        $userInfo = $request->session()->get('userInfo');
        if (empty($userInfo)) {
            return redirect('login');
        }
        $data = new Cart;
        $info = $data->getShoppingCartData($userInfo['id']);
        $isShow = true;
        if ($info->isEmpty()){
            $isShow = 0;
        }
        return view('cart.shopping_cart_01',['data'=>$info,'isShow'=>$isShow]);
    }

    public function delectShoppingCartData(Request $request)
    {
        $id = $request->id;
        $data = new Cart;
        $obj = $data->deleteShoppingCartData($id);
        if ($obj) {
            return $this->ajaxSuccess("删除成功");
        }else{
            return $this->ajaxError("删除失败");
        }
    }

    public function addCart(Request $request)
    {
        $data = $request->data;
        $userInfo = $request->session()->get('userInfo');
        if (empty($userInfo)) {
            return $this->ajaxError("3");die;
        }
        $userId = $userInfo['id'];
        $goodsId = $request->goodsId;
        $skuId = $request->skuId;
        $attrName = implode(',',$data);
        $num = $request->number;
        $obj = new Cart;
        $info = $obj->addCart($attrName,$goodsId,$userId,$num,$skuId);
        if (!$info) {
            return $this->ajaxError("0");die;
        }
        return $this->ajaxSuccess("1");die;
    }

}