## 项目初始化
1. 通过composer下载vendor目录：`composer update`
2. 生成后台样式文件：`php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets --force`
3. 执行迁移文件：`php artisan migrate`
4. 创建超级管理员：`php artisan admin:create --email=example@qq.com --password=password`