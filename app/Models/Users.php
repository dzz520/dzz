<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

/**
 * 
 */
class Users extends Model
{
	protected $table = 'user';

	public function saveUpdate($id,$status)
	{
		return Db::table('user')->where('id',$id)->update(['is_freeze'=>$status]);
	}

	public function ConfirmDeletion($id)
	{
		$id = $this->find($id);
        
        $id->user_img = '3';

        return $id->save();
	}
	
}