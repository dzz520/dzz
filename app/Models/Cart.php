<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Session;
use DB;
use Log;

class Cart extends Model
{
    protected $table = 'cart';

    protected $primaryKey = 'id';

    public function getShoppingCartData($id)
    {
        $data = DB::table('cart')
                ->select('cart.id','cart.attr_name','cart.goods_number','goods_show.goods_name','cargo.cargo_price','cargo.goods_img')
                ->join('goods_show', 'cart.goods_id', '=', 'goods_show.id')
                ->join('cargo','cart.attr_name','=','cargo.value_list')
                ->where('cart.user_id','=',$id)
                ->paginate(5);
        return $data;
    }

    public function deleteShoppingCartData($id)
    {
        $data = DB::table('cart')->where('id','=',$id)->get()->toArray();
        $skuId = $data[0]->sku_id;
        $goodsNumber = $data[0]->goods_number;
        $obj = DB::table('cart')->where('id','=',$id)->delete();
        if (!$obj) {
            return false;die;
        }
        DB::table('cargo')->where('sku_id','=',$skuId)->increment('goods_number',$goodsNumber);
        return true;
    }

    public function addCart($attrName,$goodsId,$userId,$num,$skuId)
    {
        $info = DB::table('cart')
                    ->select('goods_number')
                    ->where('attr_name','=',$attrName)
                    ->where('user_id','=',$userId)
                    ->where('goods_id','=',$goodsId)
                    ->get()
                    ->toArray();
        $cartNum = DB::table('cargo')
                    ->select('goods_number')
                    ->where('value_list','=',$attrName)
                    ->where('goods_id','=',$goodsId)
                    ->get()
                    ->toArray();
        if ($cartNum[0]->goods_number < $num) {
            return false;
        }
        if ($info) {
            $obj = DB::table('cart')
                    ->where('attr_name','=',$attrName)
                    ->where('user_id','=',$userId)
                    ->where('goods_id','=',$goodsId)
                    ->increment('goods_number',$num);
            if (!$obj) {
                return false;
            }
            $stock = DB::table('cargo')
                    ->where('value_list','=',$attrName)
                    ->where('goods_id','=',$goodsId)
                    ->decrement('goods_number',$num);
            return true;
        }else{
            $obj = DB::table('cart')->insert(['sku_id'=>$skuId,'attr_name'=>$attrName,'goods_id'=>$goodsId,'user_id'=>$userId,'goods_number'=>$num]);
            if (!$obj) {
                return false;
            }
            $stock = DB::table('cargo')
                    ->where('value_list','=',$attrName)
                    ->where('goods_id','=',$goodsId)
                    ->decrement('goods_number',$num);
            return true;
        }
        
    }

    /**
     * 清空购物车
     */

    public function deleteGoods($cartId)
    {
        return Cart::destroy($cartId);
    }
}