@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/role/save')}}" method="post" id="form">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{isset($role) ? $role['id'] : ''}}">
            <div class="box-body">
                <div class="form-group" style="height: 55px">
                    <label for="real_name" class="col-sm-2 control-label" style="color: #333">角色英文名称</label>

                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" placeholder="请输入角色英文名称" value="{{isset($role) ? $role['name'] : ''}}">
                    </div>
                </div>
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">角色中文名称</label>

                    <div class="col-sm-10">
                        <input type="text" name="cn_name" class="form-control" placeholder="请输入角色中文名称" value="{{isset($role) ? $role['cn_name'] : ''}}">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
        </form>
    </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@endpush

@push('js')
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        var validator = $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {message: '请输入角色英文名称'},    //非空提示
                        regexp: {//匹配规则
                            regexp: /^[a-z_\d]+$/,  //正则表达式
                            message:'角色英文名称必须是小写字母、数字和下划线的组合'
                        },
                        remote: { //ajax校验，获得一个json数据（{'valid': true or false}）
                            url: '{{url("admin/role/check")}}',                  //验证地址
                            message: '该名称已存在',   //提示信息
                            type: 'post',
                            data: function(){
                                return {
                                    id: $('[name=id]').val(),
                                    name: $('[name=name]').val(),
                                    type: 'name'
                                }
                            }
                        }
                    }
                },
                cn_name: {
                    validators: {
                        notEmpty: {message: '请输入角色中文名称'},    //非空提示
                        regexp: {//匹配规则
                            regexp: /^[\u4e00-\u9fa5]+$/,  //正则表达式
                            message:'角色中文名称仅支持汉字'
                        },
                        remote: { //ajax校验，获得一个json数据（{'valid': true or false}）
                            url: '{{url("admin/role/check")}}',                  //验证地址
                            message: '该名称已存在',   //提示信息
                            type: 'post',
                            data: function(){
                                return {
                                    id: $('[name=id]').val(),
                                    cn_name: $('[name=cn_name]').val(),
                                    type: 'cn_name'
                                }
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv', function(e) {
            e.preventDefault();
            var $form = $(e.target);

            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/roles')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@endpush