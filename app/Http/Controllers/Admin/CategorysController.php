<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Datatables;
use Carbon\Carbon;
use App\Models\Categorys;

class CategorysController extends Controller 
{
    /**
    * 分类添加页面
    */
    public function index()
    {
        $categorys = new Categorys;
        $data = $categorys->where("pid",0)->get()->toArray();

        return view('admin.Categorys.add_type', [
            'content_header' => '分类管理',
            'box_title' => '分类添加',
            "data" => $data
        ]);
    }

    /**
    * 分类添加
    */
    public function addType(Request $request)
    {
        $img = $request->file('images');
        if ($img){
            $disk = \Storage::disk('qiniu');
            $time = date('Y/m/d-H:i:s-');
            $filename = $disk->put($time, $img);
            if(!$filename) {
                return back()->withErrors(['上传失败']);
            }else{
                $img_url = $disk->getDriver()->downloadUrl($filename);
            }
        }else{
            $img_url = "";
        }

        $categorys = new Categorys;
        $typeName = $request->input('type_name');
        $pid = $request->input('pid');
        $res = $categorys->typeAdd($typeName,$pid,$img_url);
        if($res){
            return redirect('admin/categorys');
        }else{
            return back()->withErrors(['添加失败']);
        }
    }

    /**
    * 分类列表
    */
    public function list_type()
    {
        return view('admin.Categorys.list_type', [
            'content_header' => '分类管理',
            'box_title' => '分类列表'
        ]);
    }

    /**
    * 分类展示
    */
    public function data(Request $request)
    {
        $categorys = new Categorys;
        $query = $categorys->get();
        $date = $this->getTree($query);
        $data = datatables()->of($date)->toJson();
        return $data;
    }

    /**
    * 递归算法
    */
    public function getTree($array, $pid =0, $level = 0)
    {
        static $list = [];
        foreach ($array as $value){
            if ($value->pid == $pid){
                $value->level = $level;
                $list[] = $value;
                $this->getTree($array, $value->type_id, $level+1);
            }
        }
        return $list;
    }
}