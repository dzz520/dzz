<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Session;
use DB;
use Log;

class Categorys extends Model
{
    protected $table = "type";

    /**
    * 分类添加
    */
    public function typeAdd($typeName,$pid,$img_url)
    {
        $this->type_name = $typeName;
        $this->pid = $pid;
        $this->type_img = $img_url;
        return $this->save();
    }

    /**
    * 获取分类信息
    */
    public function getTypeData()
    {
        return $this->where('pid','=',0)->get();
    }

    /**
    * 查询衣服的分类
    */
    public function dataType()
    {
        return DB::table('type')->where('pid','=',0)
        ->get()->map(function ($value) {
            return (array)$value;
        })->toArray();
    } 

}