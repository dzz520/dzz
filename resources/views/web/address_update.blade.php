@extends('web.layouts.common')
@section('common')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">家</a></li>
                    <li>地址</li>
                </ul>
            </div>
        </div>
        <div id="pageContent">
            <div class="container offset-18">
                <h1 class="block-title large">添加地址</h1>
                <div class="row offset-48">
                    <div class="col-md-8">
                        <div class="tab-content checkout-tab-content">
                            <div class="tab-pane active" id="Shipping">
                                <h2 class="title-checkout">收货地址</h2>
                                <form class="form-horizontal" id="formin">
                                    <input type="hidden" value="{{$res->address_id}}" name="address_id">
                                    <div class="form-group">
                                        <label for="inputLastName" class="col-sm-3 control-label">收货人: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputLastName" name="accept_name" value={{$res->accept_name}}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCity" class="col-sm-3 control-label">City: <span>*</span></label>
                                        <div class="col-sm-9" >
                                            <select id="first" class="form-control" name="country[]">
                                                <option>请选择</option>
                                                @foreach($one as $key=>$v)
                                                    <option value="{{$v->region_id}}">{{$v->region_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputStreet" class="col-sm-3 control-label">具体地址: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputStreet" name="address" value={{$res->hcity}}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputZip" class="col-sm-3 control-label">邮政编码: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputZip" name="postcode" value={{$res->postcode}}>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputDefau" class="col-sm-3 control-label">是否默认: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <select name="is_default" id="inputDefau" class="form-control" value={{$res->is_default}}>
                                                <option value="1">是</option>
                                                <option value="2">否</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone" class="col-sm-3 control-label">收货电话: <span>*</span></label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputPhone" name="mobile" value={{$res->mobile}}>
                                        </div>
                                    </div>
                                    <input class="btn icon-btn-right" type="button" id="check" value="修改">
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    <script src="external/jquery/jquery-2.1.4.min.js"></script>
    <script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
    <script src="js/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
    <script type="text/javascript">
        $("#inputLastName").blur(function () {
            var accept_name = $(this).val();
            if(accept_name==''){
                swal('收货人不能为空');
                return false;
            }
        });
        $("#inputStreet").blur(function () {
            var address = $(this).val();
            if(address==''){
                swal('详细地址不能为空');
                return false;
            }
        })
        $("#inputZip").blur(function () {
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var postcode = $(this).val();
            if(postcode==''){
                swal('邮政编码不能为空');
                return false;
            }
        })
        $("#inputPhone").blur(function () {
            var phone = $(this).val();
            if(phone==''){
                swal('电话不能为空');
                return false;
            }
        })

        $(document).on("change","#first",function(){
            var id = $(this).val();
            var _this = $(this);
            $.ajax({
                url:"/address_add",
                data:{id:id},
                dataType:"json",
                success:function(e){
                    console.log(e);
                    if(e.length != 0){
                        var select = '<br><select id="first" class="form-control" name="country[]">';
                        select += '<option>请选择</option>';
                        $.each(e,function (i,v) {
                            select += '<option value="'+v.region_id+'">'+v.region_name+'</option>';
                        })
                        select += "</select>";
                        _this.nextAll().remove();
                        _this.after(select);
                    }
                }
            })
        });
        $(document).on("click","#check",function(){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            $.ajax({
                type:'post',
                url:"/address_edit_do",
                data:$('#formin').serialize(),
                dataType:"json",
                success:function(e){
                    swal(e.message);
                    if(e.status=="success"){
                        window.location.href="{{url('account_address')}}?id={{Session::get('userInfo.id')}}";
                    }
                }
            })
        })
        
    </script>
@endsection
