var gulp = require('gulp');

gulp.task('copy', function () {
    // bootstrapValidator
    gulp.src("vendor/bower/bootstrapValidator/dist/css/bootstrapValidator.min.css")
        .pipe(gulp.dest("public/css/"));
    gulp.src("vendor/bower/bootstrapValidator/dist/js/bootstrapValidator.min.js")
        .pipe(gulp.dest("public/js/"));

    // toast
    gulp.src("vendor/bower/ui-toast/dist/toast.min.css")
        .pipe(gulp.dest("public/css/"));
    gulp.src("vendor/bower/ui-toast/dist/toast.min.js")
        .pipe(gulp.dest("public/js/"));

    // layer
    gulp.src("vendor/bower/layer/dist/**/*")
        .pipe(gulp.dest("public/js/layer"));

    // icheck
    gulp.src("vendor/bower/iCheck/icheck.min.js")
        .pipe(gulp.dest("public/js/iCheck"));
    gulp.src("vendor/bower/iCheck/icheck.jquery.json")
        .pipe(gulp.dest("public/js/iCheck"));
    gulp.src("vendor/bower/iCheck/skins/**/*")
        .pipe(gulp.dest("public/js/iCheck/skins"));
});

gulp.task('default', ['copy']);