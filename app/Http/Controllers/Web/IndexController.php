<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Models\GoodsShow;
use App\Models\Categorys;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {

       $webObj = new GoodsShow;
       $data = $webObj->selectHotProduct();
       $typeObj = new Categorys;
       $information = $typeObj->getTypeData();

       return view('web.index',['data'=>$data,'information'=>$information]);
    }

}
