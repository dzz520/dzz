<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Order extends Model
{   

	protected $table = 'orders';

    protected $primaryKey = 'order_id';

    /**
    * 查询商品信息
    */

	public function selectCart($cartId)
    {

        $goodsInfo = Db::table('cart')
        ->select('cart.goods_number','cart.sku_id','cart.attr_name','cart.goods_id','cart.user_id','cart.goods_number','cargo.goods_desc','goods_image.goods_img','cargo.cargo_price','goods_show.goods_name','goods_show.goods_price','cart.id')
        ->join('cargo','cargo.sku_id','=','cart.sku_id')
        ->join('goods_show', 'cart.goods_id', '=', 'goods_show.id')
        ->join('goods_image','goods_show.id','=','goods_image.goods_id')
        ->whereIn('cart.id',$cartId)
        ->groupBy('goods_image.goods_id')
        ->get();

        return $goodsInfo;
    }

    /**
    * 添加订单信息
    */

    public function addOrders($orders,$orderYear,$Quarter)
    {

        $this->table =  $orderYear.'_'.$Quarter.'_orders';
        
        if($Quarter != 1){

            $checkCount = DB::table(''.$orderYear.'_'.$Quarter.'_orders')->select(DB::raw('count(*) as count'))->first();
            $checkCount = $checkCount->count;

            if ($checkCount == 0) {

                $tableNumber = $Quarter-1;
                $prevCount = DB::table(''.$orderYear.'_'.$tableNumber.'_orders')->select(DB::raw('count(*) as count'))->first();
                
                $this->order_id = $prevCount->count+1;

            }
            
        }

        $this->order_sn = $orders['order_sn'];
        $this->order_price = $orders['order_price'];
        $this->real_price = $orders['real_price'];
        $this->user_id = $orders['user_id'];
        $this->order_name = $orders['order_name'];
        $this->order_tel = $orders['order_del'];
        $this->order_address = $orders['order_address'];
        $this->pay_type = $orders['pay_type'];

        $this->save();

        return $this->order_id; 

    }

    /**
    * 生成订单编号
    */

    public function orderNum($userInfo)
    {   

        $orderNumber = time().substr(str_pad($userInfo['id'],5,'0',STR_PAD_LEFT),-3,3).str_pad(mt_rand(1,777), 5, '0', STR_PAD_LEFT);

        return $orderNumber;
    }


    public function orderQuarter($orderMonth)
    {
        if ($orderMonth <= 3) {

            $Quarter = 1;

        }elseif ($orderMonth <= 6 && $orderMonth>3) {
            
            $Quarter = 2;

        }elseif ($orderMonth <= 9 && $orderMonth>6) {

            $Quarter = 3;
            
        }else{

            $Quarter = 4;

        }

        return $Quarter;
    }
    
}