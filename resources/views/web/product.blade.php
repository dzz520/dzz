@extends('web.layouts.common')
@section('common')
        <div class="loader-wrapper">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="loader-animation" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                </svg>
            </div>
        </div>
        <nav class="panel-menu">
            <ul>
                <li>
                    <a href="index.html">LAYOUT</a>
                    <ul>
                        <li><a href="index.html">Home page 1</a></li>
                        <li><a href="index-02.html">Home page 2</a></li>
                        <li><a href="index-03.html">Home page 3</a></li>
                        <li><a href="index-04.html">Home page 4</a></li>
                        <li><a href="index-05.html">Home page 5</a></li>
                        <li><a href="index-06.html">Home page 6</a></li>
                        <li><a href="index-07.html">Home page 7</a></li>
                        <li><a href="index-08.html">Home page 8</a></li>
                        <li><a href="index-09.html">Home page 9</a></li>
                        <li><a href="index-10.html">Home page 10</a></li>
                        <li><a href="index-11.html">Home page 11</a></li>
                        <li><a href="index-12.html">Home page 12</a></li>
                    </ul>
                </li>
                <li><a href="index-rtl.html">RTL</a></li>
                <li>
                    <a href="listing-left-column.html">LISTING</a>
                    <ul>
                        <li><a href="listing-left-column.html">Left Column</a></li>
                        <li><a href="listing-left-right-column.html">Left and Right Column</a></li>
                        <li><a href="listing-right-column.html">Right Column</a></li>
                        <li><a href="listing-left-column-without-block.html">Left Column Without Block</a></li>
                        <li><a href="listing-without-columns.html">Without Columns</a></li>
                        <li><a href="listing-without-columns-items6.html">Without Column and 6 items in line</a></li>
                        <li><a href="listing-without-columns-items4.html">Without Column and 4 items in line</a></li>
                        <li><a href="listing-without-columns-items3.html">Without Column and 3 items in line</a></li>
                        <li><a href="listing-without-columns-items2.html">Without Column and 2 items in line</a></li>
                    </ul>
                </li>
                <li>
                    <a href="product.html">PRODUCT</a>
                    <ul>
                        <li><a href="product.html">Image Size - Small</a></li>
                        <li><a href="product-02.html">Image Size - Medium</a></li>
                        <li><a href="product-03.html">Image Size - Big</a></li>
                        <li><a href="product-04.html">Image Size - Medium</a></li>
                        <li><a href="product-05.html">Image Size - Long</a></li>
                    </ul>
                </li>
                <li>
                    <a href="blog_listing.html">BLOG</a>
                    <ul>
                        <li><a href="blog_listing.html">Listing</a></li>
                        <li><a href="blog_grid_col_2.html">Grid Layout 2 cols</a></li>
                        <li><a href="blog_grid_col_3.html">Grid Layout 3 cols</a></li>
                        <li><a href="blog_masonry_col_2.html">Masonry Layout 2 cols</a></li>
                        <li><a href="blog_masonry_col_3.html">Masonry Layout 3 cols</a></li>
                        <li><a href="blog_single_post.html">Blog single post 1</a></li>
                        <li><a href="blog_single_post_01.html">Blog single post 2</a></li>
                    </ul>
                </li>
                <li>
                    <a href="gallery_masonry_col_3.html">GALLERY</a>
                    <ul>
                        <li><a href="gallery_grid_col_2.html">Grid col 2</a></li>
                        <li><a href="gallery_grid_col_3.html">Grid col 3</a></li>
                        <li><a href="gallery_masonry_col_2.html">Masonry col 2</a></li>
                        <li><a href="gallery_masonry_col_3.html">Masonry col 3</a></li>
                    </ul>
                </li>
                <li>
                    <a href="about.html">PAGES</a>
                    <ul>
                        <li><a href="about.html">About</a></li>
                        <li><a href="about_01.html">About 2</a></li>
                        <li><a href="contact.html">Contacts</a></li>
                        <li><a href="contact_01.html">Contacts 2</a></li>
                        <li><a href="comming-soon.html">Under Construction</a></li>
                        <li><a href="look-book.html">Lookbook</a></li>
                        <li><a href="collections.html">Collections</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="infographic.html">Infographic</a></li>
                        <li><a href="faqs.html">Delivery Page</a></li>
                        <li><a href="faqs.html">Payment page</a></li>
                        <li><a href="checkout.html">Checkout</a></li>
                        <li><a href="compare.html">Compare</a></li>
                        <li><a href="wishlist.html">Wishlist</a></li>
                        <li><a href="shopping_cart_01.html">Shopping cart 01</a></li>
                        <li><a href="shopping_cart_02.html">Shopping cart 02</a></li>
                        <li><a href="account.html">Page Account</a></li>
                        <li><a href="account-address.html">Page Account address</a></li>
                        <li><a href="account-order.html">Page Account order</a></li>
                        <li><a href="login-form.html">Login form</a></li>
                        <li><a href="faqs.html">Support page</a></li>
                        <li><a href="faqs.html">FAQs</a></li>
                        <li><a href="page-404.html">Page 404</a></li>
                        <li><a href="page-empty-category.html">Page Empty Category</a></li>
                        <li><a href="page-empty-search.html">Page Empty Search</a></li>
                        <li><a href="page-empty-shopping-cart.html">Page Empty shopping Cart</a></li>
                    </ul>
                </li>
                <li>
                    <a href="listing-left-column.html">WOMEN'S</a>
                    <ul>
                        <li>
                            <a href="listing-left-column.html">TOPS</a>
                            <ul>
                                <li><a href="listing-left-column.html">Blouses & Shirts</a></li>
                                <li><a href="listing-left-column.html">Dresses</a></li>
                                <li>
                                    <a href="listing-left-column.html">Tops & T-shirts</a>
                                    <ul>
                                        <li><a href="listing-left-column.html">Link level 1</a></li>
                                        <li>
                                            <a href="listing-left-column.html">Link level 1</a>
                                            <ul>
                                                <li><a href="listing-left-column.html">Link level 2</a></li>
                                                <li>
                                                    <a href="listing-left-column.html">Link level 2</a>
                                                    <ul>
                                                        <li><a href="listing-left-column.html">Link level 3</a></li>
                                                        <li><a href="listing-left-column.html">Link level 3</a></li>
                                                        <li><a href="listing-left-column.html">Link level 3</a></li>
                                                        <li>
                                                            <a href="listing-left-column.html">Link level 3</a>
                                                            <ul>
                                                                <li>
                                                                    <a href="listing-left-column.html">Link level 4</a>
                                                                    <ul>
                                                                        <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-left-column.html">Link level 5</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li><a href="listing-left-column.html">Link level 4</a></li>
                                                                <li><a href="listing-left-column.html">Link level 4</a></li>
                                                                <li><a href="listing-left-column.html">Link level 4</a></li>
                                                                <li><a href="listing-left-column.html">Link level 4</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="listing-left-column.html">Link level 3</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="listing-left-column.html">Link level 2</a></li>
                                                <li><a href="listing-left-column.html">Link level 2</a></li>
                                                <li><a href="listing-left-column.html">Link level 2</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="listing-left-column.html">Link level 1</a></li>
                                        <li><a href="listing-left-column.html">Link level 1</a></li>
                                        <li><a href="listing-left-column.html">Link level 1</a></li>
                                    </ul>
                                </li>
                                <li><a href="listing-left-column.html">Sleeveless Tops</a></li>
                                <li><a href="listing-left-column.html">Sweaters</a></li>
                                <li><a href="listing-left-column.html">Jackets</a></li>
                                <li><a href="listing-left-column.html">Outerwear</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="listing-left-column.html">BOTTOMS</a>
                            <ul>
                                <li><a href="listing-left-column.html">Trousers</a></li>
                                <li><a href="listing-left-column.html">Jeans</a></li>
                                <li><a href="listing-left-column.html">Leggings</a></li>
                                <li><a href="listing-left-column.html">Jumpsuit & shorts</a></li>
                                <li><a href="listing-left-column.html">Skirts</a></li>
                                <li><a href="listing-left-column.html">Tights</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="listing-left-column.html">ACCESSORIES</a>
                            <ul>
                                <li><a href="listing-left-column.html">Jewellery</a></li>
                                <li><a href="listing-left-column.html">Hats</a></li>
                                <li><a href="listing-left-column.html">Scarves & snoods</a></li>
                                <li><a href="listing-left-column.html">Belts</a></li>
                                <li><a href="listing-left-column.html">Bags</a></li>
                                <li><a href="listing-left-column.html">Shoes</a></li>
                                <li><a href="listing-left-column.html">Sunglasses</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="listing-left-column.html">SPECIALS</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="listing-right-column.html">MEN'S</a>
                    <ul>
                        <li>
                            <a href="listing-right-column.html">TOPS</a>
                            <ul>
                                <li><a href="listing-right-column.html">Blouses & Shirts</a></li>
                                <li><a href="listing-right-column.html">Dresses</a></li>
                                <li>
                                    <a href="listing-right-column.html">Tops & T-shirts</a>
                                    <ul>
                                        <li><a href="listing-right-column.html">Link level 1</a></li>
                                        <li>
                                            <a href="listing-right-column.html">Link level 1</a>
                                            <ul>
                                                <li><a href="listing-right-column.html">Link level 2</a></li>
                                                <li>
                                                    <a href="listing-right-column.html">Link level 2</a>
                                                    <ul>
                                                        <li><a href="listing-right-column.html">Link level 3</a></li>
                                                        <li><a href="listing-right-column.html">Link level 3</a></li>
                                                        <li><a href="listing-right-column.html">Link level 3</a></li>
                                                        <li>
                                                            <a href="listing-right-column.html">Link level 3</a>
                                                            <ul>
                                                                <li>
                                                                    <a href="listing-right-column.html">Link level 4</a>
                                                                    <ul>
                                                                        <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                        <li><a href="listing-right-column.html">Link level 5</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li><a href="listing-right-column.html">Link level 4</a></li>
                                                                <li><a href="listing-right-column.html">Link level 4</a></li>
                                                                <li><a href="listing-right-column.html">Link level 4</a></li>
                                                                <li><a href="listing-right-column.html">Link level 4</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="listing-right-column.html">Link level 3</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="listing-right-column.html">Link level 2</a></li>
                                                <li><a href="listing-right-column.html">Link level 2</a></li>
                                                <li><a href="listing-right-column.html">Link level 2</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="listing-right-column.html">Link level 1</a></li>
                                        <li><a href="listing-right-column.html">Link level 1</a></li>
                                        <li><a href="listing-right-column.html">Link level 1</a></li>
                                    </ul>
                                </li>
                                <li><a href="listing-right-column.html">Sleeveless Tops</a></li>
                                <li><a href="listing-right-column.html">Sweaters</a></li>
                                <li><a href="listing-right-column.html">Jackets</a></li>
                                <li><a href="listing-right-column.html">Outerwear</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="listing-right-column.html">BOTTOMS</a>
                            <ul>
                                <li><a href="listing-right-column.html">Trousers</a></li>
                                <li><a href="listing-right-column.html">Jeans</a></li>
                                <li><a href="listing-right-column.html">Leggings</a></li>
                                <li><a href="listing-right-column.html">Jumpsuit & shorts</a></li>
                                <li><a href="listing-right-column.html">Skirts</a></li>
                                <li><a href="listing-right-column.html">Tights</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="listing-right-column.html">ACCESSORIES</a>
                            <ul>
                                <li><a href="listing-right-column.html">Jewellery</a></li>
                                <li><a href="listing-right-column.html">Hats</a></li>
                                <li><a href="listing-right-column.html">Scarves & snoods</a></li>
                                <li><a href="listing-right-column.html">Belts</a></li>
                                <li><a href="listing-right-column.html">Bags</a></li>
                                <li><a href="listing-right-column.html">Shoes</a></li>
                                <li><a href="listing-right-column.html">Sunglasses</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#">BUY TEMPLATE</a></li>
            </ul>
            <div class="mm-navbtn-names" style="display:none">
                <div class="mm-closebtn">CLOSE</div>
                <div class="mm-backbtn">BACK</div>
            </div>
        </nav>

        <div class="breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="listing-left-column.html">Women’s</a></li>
                    <li>Dresses</li>
                </ul>
            </div>
        </div>
        <!-- Content -->
        <div id="pageContent">
            <div class="productPrevNext hidden-xs hidden-sm">
                <a href="#" class="product-prev"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt="" /></a>
                <a href="#" class="product-next"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt="" /></a>
            </div>
            <div class="container offset-0">
                <div class="row">
                    <div class="col-md-5 hidden-xs">
                        <div class="product-main-image">
                            <div class="product-main-image-item">
                                <img class="zoom-product" src="{{$dataimgs[0]['goods_img']}}" 
                                data-zoom-image="" alt="" />
                                <!-- 图片 -->
                            </div>
                        </div>
                        <div class="product-images-carousel">
                            <ul id="smallGallery">
                                <!-- <li><a class="zoomGalleryActive" href="#" data-image="{{asset('vendor/weblte/images/product/product-big-1.jpg')}}" data-zoom-image="{{asset('vendor/weblte/images/product/product-big-1.jpg')}}"><img src="{{asset('vendor/weblte/images/product/product-small-1.jpg')}}" alt="" /></a></li>
                                <li><a href="#" data-image="{{asset('vendor/weblte/images/product/product-big-2.jpg')}}" data-zoom-image="{{asset('vendor/weblte/images/product/product-big-2-zoom.jpg')}}"><img src="{{asset('vendor/weblte/images/product/product-small-2.jpg')}}" alt="" /></a></li>
                                <li><a href="#" data-image="{{asset('vendor/weblte/images/product/product-big-3.jpg')}}" data-zoom-image="{{asset('vendor/weblte/images/product/product-big-3-zoom.jpg')}}"><img src="{{asset('vendor/weblte/images/product/product-small-3.jpg')}}" alt="" /></a></li>
                                <li> -->
                                @foreach($dataimgs as $img)
                                <li>
                                    <a class="zoomGalleryActive" href="#" data-image="{{$img['goods_img']}}" data-zoom-image="{{$img['goods_img']}}">
                                        <img src="{{$img['goods_img']}}" alt="" />
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="visible-xs">
                            <div class="clearfix"></div>
                            <ul class="mobileGallery-product">
                                <li><img src="{{asset('vendor/weblte/images/product/product-big-1.jpg')}}" alt="" /></li>
                                <li><img src="{{asset('vendor/weblte/images/product/product-big-2.jpg')}}" alt="" /></li>
                                <li><img src="{{asset('vendor/weblte/images/product/product-big-3.jpg')}}" alt="" /></li>
                                <li>
                                    <div class="video-carusel">
                                        <img src="{{asset('vendor/weblte/images/product/product-small-empty.png')}}" alt="" />
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="product-info">
                            <h1 class="title vendor-top" goods_id="{{$dataList->id}}">{{$dataList->goods_name}}</h1>
                            <div class="price">
                                <font color="red">￥</font><span class="new-price" price="{{$dataList->goods_price}}" goodsPrive="{{$dataList->goods_price}}" id="des_pric">{{$scope->min}}—{{$scope->max}}</span>
                            </div>
                    <?php foreach($data as $key=>$val){?>
                            <div class="title-options" style="margin-top: 20px"><?=$val['name']?><span class="color-required">*</span></div>
                        <?php if ($val['name']=='颜色'){?>
                             <ul class="tags-list" style="margin-top: -6px">
                                    <?php foreach($val['child'] as $k=>$v){?>
                                      <li><a href="javascript:void(0)" class="btnbutton bt1" where="{{$dataList->id}}" attr_price="<?=$v['attr_value']?>"><?=$v['attr_value']?></a></li>
                                        <?php }?>
                                    </ul>
                        <?php }else {?>
                                 <ul class="tags-list" style="margin-top: -6px">
                                    <?php foreach($val['child'] as $k=>$v){?>
                                          <li><a class="btnbutton bt2" href="javascript:void(0)" where="{{$dataList->id}}" attr_price="<?=$v['attr_value']?>"><?=$v['attr_value']?></a></li>
                                        <?php }?>
                                    </ul>
                        <?php }?>
                    <?php }?>

                            <!-- <div> -->
                                库存:<span id="Stock"></span>
                           <!--  </div> -->
                            
                            <div class="wrapper" style="margin-top: 15px" >
                                <td>购买数量:</td>
                                <div class="style-2 input-counter">
                                    <span class="minus-btn" status='up'></span>
                                    <input type="text" value="1" size="1000" class="car_ipt"/>
                                    <span class="plus-btn" status='down'></span>
                                </div>
                            </div>
                            <div class="wrapper">
                                <div class="pull-left">
                                    <div class="btn btn-lg btn-addtocart"><span class="icon icon-shopping_basket"></span>添加购物车!</div>
                                </div>
                                <div class="pull-left">
                                    <ul class="product_inside_info_link">
                                        <li class="text-right">
                                            <a href="#">
                                                <span class="fa fa-heart-o"></span>
                                                <span class="text">加入愿望清单</span>
                                            </a>
                                        </li>
                                        <li class="text-left">
                                            <a href="#" class="compare-link">
                                                <span class="fa fa-balance-scale"></span>
                                                <span class="text">加入对比</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="wrapper">
                                <ul class="social-icon-square">
                                    <li><a class="icon-01" href="#"></a></li>
                                    <li><a class="icon-02" href="#"></a></li>
                                    <li><a class="icon-03" href="#"></a></li>
                                    <li><a class="icon-04" href="#"></a></li>
                                    <li><a class="icon-05" href="#"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container offset-80">
                <div class="tt-product-page__tabs tt-tabs">
                    <div class="tt-tabs__head">
                        <ul>
                            <li data-active="true"><span>描述</span></li>
                            <li><span>支付方式介绍</span></li>
                            <li><span>尺寸指南</span></li>
                            <li><span>TAGS</span></li>
                            <li><span>点评</span></li>
                        </ul>
                        <div class="tt-tabs__border"></div>
                    </div>
                    <div class="tt-tabs__body">
                        <div>
                           <span class="tt-tabs__title">描述</span>
                           <div class="tt-tabs__content">
                                <h5 class="tab-title">描述</h5>
                                <p>
                                    银色,金属蓝色和金属色薰衣草真丝混纺提花,图案,褶皱荷叶边领,长袖,系扣钮扣,搭扣银色细腰带,臀部饰有大褶状玫瑰花饰.
                                </p>
                                <ul class="list-simple-dot">
                                    <li><a href="#">轻盈柔软的手感编织</a></li>
                                    <li><a href="#">高腰</a></li>
                                    <li><a href="#">拉链带钩和纽扣</a></li>
                                    <li><a href="#">防滑口袋</a></li>
                                    <li><a href="#">腰带</a></li>
                                    <li><a href="#">轻松合身</a></li>
                                    <li><a href="#">机洗</a></li>
                                    <li><a href="#">76％涤纶,18％粘胶,6％弹性纤维</a></li>
                                    <li><a href="#">我们的模特穿着英国8 / EU 36 / US 4，高174厘米/5'8.5英寸</a></li>
                                </ul>
                           </div>
                        </div>
                        <div>
                            <span class="tt-tabs__title">支付方式介绍</span>
                            <div class="tt-tabs__content">
                                <h5 class="tab-title">支付方式介绍</h5>
                                <h6><span class="color-base icon icon-local_shipping"></span>  送货和送货</h6>
                                我们致力于尽可能快速,经济地提供您的购买.
                                <div class="divider"></div>
                                <h6><span class="color-base icon icon-payment"></span> 支付方式</h6>
每个国家和购物者都有他们喜欢的在线支付方式.为您的买家提供安全方便的付款选择,可以帮助您顺利完成销售,为您赢得积极的反馈,并为他们带来更多回报
                            </div>
                        </div>
                        <div>
                            <span class="tt-tabs__title">尺寸指南</span>
                            <div class="tt-tabs__content">
                                <h5 class="tab-title">服装 - 单一尺寸转换(续)</h5>
                                <div class="table-responsive">
                                    <table class="table table-parameters">
                                        <tbody>
                                            <tr>
                                                <td>联合王国</td>
                                                <td>18</td>
                                                <td>20</td>
                                                <td>22</td>
                                                <td>24</td>
                                                <td>26</td>
                                            </tr>
                                            <tr>
                                                <td>欧洲的</td>
                                                <td>46</td>
                                                <td>48</td>
                                                <td>50</td>
                                                <td>52</td>
                                                <td>54</td>
                                            </tr>
                                            <tr>
                                                <td>我们</td>
                                                <td>14</td>
                                                <td>16</td>
                                                <td>18</td>
                                                <td>20</td>
                                                <td>22</td>
                                            </tr>
                                            <tr>
                                                <td>澳大利亚</td>
                                                <td>8</td>
                                                <td>10</td>
                                                <td>12</td>
                                                <td>14</td>
                                                <td>16</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <span class="tt-tabs__title">TAGS</span>
                            <div class="tt-tabs__content">
                                <h5 class="tab-title">TAGS</h5>
                                <ul class="tags-list">
                                    <li><a href="#">酿酒</a></li>
                                    <li><a href="#">样式</a></li>
                                    <li><a href="#">街头风</a></li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <span class="tt-tabs__title">点评</span>
                            <div class="tt-tabs__content">
                                <h5 class="tab-title">顾客评论</h5>
                                <div class="review">
                                    <div class="rating">
                                        <span class="icon-star"></span>
                                        <span class="icon-star"></span>
                                        <span class="icon-star"></span>
                                        <span class="icon-star"></span>
                                        <span class="icon-star empty-star"></span>
                                    </div>
                                    <span>1 Review(s)</span>
                                    <a href="#">添加您的评论</a>
                                </div>
                                <div class="divider"></div>
                                <h6>写评论</h6>
                                <div class="divider"></div>
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="form-name" class="col-sm-2 control-label">名称</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="form-name" placeholder="你的名称">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-email" class="col-sm-2 control-label">电子邮件</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="form-email" placeholder="你的电子邮件">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-sm-2 control-label">评分</label>
                                        <div class="col-sm-10">
                                            <div class="rating">
                                                <span class="icon-star empty-star"></span>
                                                <span class="icon-star empty-star"></span>
                                                <span class="icon-star empty-star"></span>
                                                <span class="icon-star empty-star"></span>
                                                <span class="icon-star empty-star"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-review-title" class="col-sm-2 control-label">评论标题</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="form-review-title" placeholder="评论标题">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="form-review-title" class="col-sm-2 control-label">评论主体 (1500)</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" rows="7" placeholder="评论主体"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default">提交评论</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="reviews-comments">
                                    <div class="item">
                                        <div class="rating">
                                            <span class="icon-star"></span>
                                            <span class="icon-star"></span>
                                            <span class="icon-star"></span>
                                            <span class="icon-star"></span>
                                            <span class="icon-star empty-star"></span>
                                        </div>
                                        <div class="title">管理员</div>
                                        <div class="data">Prabhu在2016年5月19日</div>
                                        <p>
                                            哇!在这个模板上非常棒的工作:-)<br>
只是想说,因为我们在过去的一周里越来越多地使用了YourStore模板,我们真的给人留下了深刻的印象.这必须是我们在ThemeForest上遇到的最好的模板之一.只有少数小错误,这个板上的优秀支持立即得到了解决。代码干净灵活,易于让更多新手设计师适应他们的需求.我们很高兴终极拥有一个愉快的专业设计,这将使我们能够在预算内按时完成业务扩展
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal (quickViewModal) -->
        <div class="modal  fade"  id="ModalquickView" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <!--modal-quick-view-->
                            <div class="modal-quick-view">
                                <div class="row">
                                    <div class="col-sm-5 col-lg-6">
                                        <div class="product-main-image">
                                            <img src="{{asset('vendor/weblte/images/product/product-big-1.jpg')}}" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/modal-quick-view-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- / Modal (quickViewModal) -->
        <!-- Modal (newsletter) -->
        <div class="modal  fade"  id="Modalnewsletter" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"  data-pause=2000>
            <div class="modal-dialog modal-md-middle">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                        <h4 class="modal-title text-center text-uppercase">GET THE LATEST NEWS<br>DELIVERED DAILY!</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <!--modal-add-cart-->
                            <div class="modal-newsletter">
                                <p>
                                    Give us your email and you will be daily updated with <br>the latest events, in detail!
                                </p>
                                <div  class="subscribe-form">
                                    <div class="row-subscibe">
                                        <div class="col-left">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                <span class="icon icon-email"></span>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Enter please your e-mail">
                                            </div>
                                        </div>
                                        <div class="col-right">
                                            <button type="submit" class="btn btn-fill">SUBSCRIBE</button>
                                        </div>
                                    </div>
                                    <div class="checkbox-group form-group-top clearfix">
                                        <input type="checkbox" id="checkBox1">
                                        <label for="checkBox1">
                                        <span class="check"></span>
                                        <span class="box"></span>
                                        Don’t show this popup again
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--/modal-add-cart-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- / Modal (newsletter) -->
        <!-- modalLoginForm-->
        <div class="modal  fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md-small">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                        <h4 class="modal-title text-center text-uppercase">Login form</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <!--modal-add-login-->
                            <div class="modal-login">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        <span class="icon icon-person_outline"></span>
                                        </span>
                                        <input type="text" id="LoginFormName" class="form-control" placeholder="Name:">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                        <span class="icon icon-lock_outline"></span>
                                        </span>
                                        <input type="password" id="LoginFormPass" class="form-control" placeholder="Password:">
                                    </div>
                                </div>
                                <div class="checkbox-group">
                                    <input type="checkbox" id="checkBox2">
                                    <label for="checkBox2">
                                    <span class="check"></span>
                                    <span class="box"></span>
                                    Remember me
                                    </label>
                                </div>
                                <button type="button" class="btn btn-full">SIGN IN</button>
                                <button type="button" class="btn btn-full">CREATE AN ACCOUNT</button>
                                <div class="text-center"></div>
                                <div class="social-icon-fill">
                                    <div>Or sign in with social</div>
                                    <ul>
                                        <li><a class="icon bg-facebook fa fa-facebook" href="http://www.facebook.com/"></a></li>
                                        <li><a class="bg-twitter fa fa-twitter" href="http://www.twitter.com/"></a></li>
                                        <li><a class="bg-google-plus fa fa-google-plus" href="http://www.google.com/"></a></li>
                                    </ul>
                                </div>
                                <ul class="link-functional">
                                    <li><a href="#">Forgot your username?</a></li>
                                    <li><a href="#">Forgot your password?</a></li>
                                </ul>
                            </div>
                            <!--/modal-add-login-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /modalLoginForm-->
        <!-- modalAddToCart -->
        <div class="modal  fade"  id="modalAddToCart" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <!--  -->
                        <div class="modal-add-cart">
                            <span class="icon color-base icon-check_circle"></span>
                            <p>
                                Added to cart successfully!
                            </p>
                            <a class="btn btn-underline color-defaulttext2" href="#">GO TO CART</a>
                        </div>
                        <!-- / -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /modalAddToCart -->
        <!-- modalAddToCartProduct -->
        <div class="modal  fade"  id="modalAddToCartProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <!--  -->
                        <div class="modal-add-cart-product desctope">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="modal-messages">
                                        <span class="icon color-base icon-check_circle"></span>
                                        <p>
                                            Product successfully added to your shopping cart
                                        </p>
                                    </div>
                                    <div class="modal-product">
                                        <div class="image-box">
                                            <img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt="">
                                        </div>
                                        <div class="title">
                                            Daisy Street 3/4 Sleeve Panelled Casual Shirt
                                        </div>
                                        <div class="description">
                                            Black,  Xl
                                        </div>
                                        <div class="qty">
                                            Qty: 1
                                        </div>
                                    </div>
                                    <div class="total">
                                        TOTAL: <span>$135</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="cart-item-total">
                                        <a href="shopping_cart_01.html">
                                            <div class="cart-item-icon">
                                                <span class="icon icon-shopping_basket"></span>View Cart
                                            </div>
                                        </a>
                                        <p>
                                            There are 3 items<br> in your cart
                                        </p>
                                    </div>
                                    <div class="total-product">
                                        Total products: <span>$245</span>
                                    </div>
                                    <div class="total">
                                        TOTAL: <span>$135</span>
                                    </div>
                                    <a href="#" class="btn invert">CONTINUE SHOPPING</a>
                                    <a href="#" class="btn">PROCEED TO CHECKOUT</a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-add-cart-product mobile">
                            <div class="modal-messages">
                                <span class="icon color-base icon-check_circle"></span>
                                <p>
                                    Added to cart successfully!
                                </p>
                            </div>
                            <a href="#" class="btn btn-underline">GO TO CART!</a>
                        </div>
                        <!-- / -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /modalAddToCartProduct -->
        <!-- modalCompare -->
        <div class="modal-compare">
            <div class="container">
                <a href="#" class="icon icon-close button-close"></a>
                <div class="title-top">COMPARE</div>
                <div class="row-content">
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                    </div>
                </div>
                <div class="row-button">
                    <a href="#" class="clear-all btn btn-lg btn-underline"><span>CLEAR ALL</span></a>
                    <a href="#" class="btn btn-inversion btn-compare"><span class="fa fa-balance-scale"></span>COMPARE</a>
                </div>
            </div>
        </div>
        <!-- /modalCompare -->
        <!-- modalWishlist -->
        <div class="modal-wishlist">
            <div class="container">
                <a href="#" class="icon icon-close button-close"></a>
                <div class="title-top">MY WISHLIST</div>
                <div class="row-content">
                   <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                     <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                      <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                     <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                     <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div> 
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div>
                    <div class="item">
                        <a href="#" class="img"><img src="{{asset('vendor/weblte/images/product/product-01.jpg')}}" alt=""/></a>
                        <a href="#" class="title">Daisy Street 3/4 Sleeve Panelled Casual Shirt</a>
                        <div class="price">
                            $45
                        </div>
                        <a href="#" class="icon icon-delete"></a>
                        <a href="#" class="icon icon-check icon-check_circle"></a>
                    </div>
                </div>
                <div class="row-button">
                    <a href="#" class="clear-all btn btn-lg btn-underline"><span>CLEAR ALL</span></a>
                </div>
            </div>
        </div>
        <!-- /modalWishlist -->
        <!-- modalVideoProduct -->
        <div class="modal  fade"  id="modalVideoProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-video">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-video-content">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('vendor/weblte/external/jquery/jquery-2.1.4.min.js')}}"></script>
        <script src="{{asset('vendor/weblte/js/sweetalert.min.js')}}"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/weblte/css/sweetalert.css')}}">
        <script>
            obj = new Object();
            data = new Object();
            var repertory = 0;
            var sku_id = 0;
            $(document).on('click','.btnbutton',function(){
                $(this).addClass('checked');
                $(this).attr('style','background-color:black');
                $(this).css('color','#fff');
                $(this).parent().siblings().children('a').removeClass("checked");
                $(this).parent().siblings().children('a').removeAttr('style');
                var goodsPrive=$('#des_pric').attr('goods_prive');
                var goods_id=$('.vendor-top').attr('goods_id');
                var num = 0;
                liPrice = $(this).attr("attr_price");//尺寸当前的状态
                $(".checked").each(function(i) {
                    num++;
                    obj[i] = $(this).attr("attr_price");//尺寸当前的状态                      
                });

                 nums = "<?=$num?>";
                 
                 obj['goods_id'] = goods_id;
                 if (num == nums) {
                   $.ajax({
                      url:'/dtailsRevised',
                      data:obj,
                      dataType:"json",
                      type:'get',
                      success(e){
                        if(e.data){
                            repertory = 1;
                            sku_id = e.data.sku_id;
                            $("#des_pric").text(e.data.data);
                            $("#des_pric").attr('price',e.data.data);
                            $("#Stock").text(e.data.goodsNumber).css("color","green");
                        }else{
                            repertory = 0;
                            var goodsPrive=$('#des_pric').attr('goods_prive');
                            //$("#des_pric").text(goods_prive);
                            $("#Stock").text('0').css("color","red");
                        }
                      }
                   })
                }
            });
            $(document).on('click', '.btn-addtocart', function() {
                if (repertory == 1) {
                    $(".checked").each(function(i) {
                        data[i] = $(this).attr("attr_price");                     
                    });
                    var number = $(".car_ipt").val();
                    var goods_id = $(".vendor-top").attr('goods_id');
                    $.ajax({
                        url:"{{asset('shoppingcart/addCart')}}",
                        data:{data:data,number:number,goodsId:goods_id,skuId:sku_id},
                        dataType:"json",
                        type:'get',
                        success(e){
                            if (e.message == '0') {
                                swal("库存不够充足", "库存已经没有您要购买的那么多数量了~", "error");
                            }else if(e.message == '3'){
                                swal("请先登录", "登录成功后再来添加商品哟~", "error");
                            }else{
                                swal("加入购物车成功", "商品已加入您的购物车", "success");
                            }
                        }
                    })
                }else{
                    swal("库存不足", "请选择其他商品属性", "error");
                }
            });
        </script>
@endsection