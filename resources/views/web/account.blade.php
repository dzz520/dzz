@extends('web.layouts.common')

@section('common')
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="{{asset('/')}}">家</a></li>
                <li>账户</li>
            </ul>
        </div>
    </div>
    <!-- Content -->
    <div id="pageContent">
        <div class="container offset-18">
            <h1 class="block-title large">账户</h1>
            <div class="container offset-80">
                <div class="tt-product-page__tabs tt-tabs">
                    <div class="tt-tabs__head">
                        <ul>
                            <li data-active="true"><span>用户信息</span></li>
                            <li><span>账户中心</span></li>
                            <li><span>订单中心</span></li>
                            <li><span>收货地址</span></li>
                        </ul>
                        <div class="tt-tabs__border"></div>
                    </div>
                    <div class="tt-tabs__body">
                        <div>
                            <div class="tt-tabs__content">
                                <div class="responsive-table">
                                    <table class="table table-params">
                                        <tbody>
                                        <tr>
                                            <td>名称:</td>
                                            <td>{{$data->user_name}}</td>
                                        </tr>
                                        <tr>
                                            <td>性别:</td>
                                            <td>{{$data->user_sex}}</td>
                                        </tr>
                                        <tr>
                                            <td>电子邮件:</td>
                                            <td><a href="mailto:{{$data->email}}">{{$data->email}}</a></td>
                                        </tr>
                                        <tr>
                                            <td>电话:</td>
                                            <td>{{$data->mobile}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="tt-tabs__content">
                                <p><a href="#" data-toggle="modal" data-target="#modalLoginForm">修改用户信息</a></p>
                            </div>
                        </div>
                        <div>
                            <div class="tt-tabs__content">
                                <p><a href="{{url('account_order')}}">我的订单</a></p>
                            </div>
                        </div>
                        <div>
                            <div class="tt-tabs__content">
                                <p><a href="/account_address?id={{$data->id}}">查看地址</a></p>
                                <p><a href="/address_add?id={{$data->id}}">添加地址</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--修改--}}
    <div class="modal  fade"  id="modalLoginForm" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md-small">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    <h4 class="modal-title text-center text-uppercase">修改用户信息</h4>
                </div>
                <form>
                    <input type="hidden" value="{{$data->id}}" id="userId">
                    <div class="modal-body">
                        <div class="modal-login">
                            <div class="form-group">
                                <div class="name">
                                    <span class="name-addon">名称:</span>
                                    <input type="text" name="user_name" id="userName" class="form-control" value="{{$data->user_name}}" placeholder="请输入用户昵称，必须是汉字">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="sex">
                                    <span class="sex-addon">性别:</span>
                                    <select name="user_sex" id="userSex" value="{{$data->user_sex}}" class="form-control">
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="email">
                                    <span class="email-addon">电子邮件:</span>
                                    <input type="text" name="email" id="Email" class="form-control" value="{{$data->email}}"  placeholder="请输入正确的邮箱格式">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="mobile">
                                    <span class="mobile-addon">手机:</span>
                                    <input type="text" name="mobile" id="Mobile" class="form-control" value="{{$data->mobile}}"  placeholder="请输入正确的手机格式">
                                </div>
                            </div>
                            <button type="button" class="btn btn-full" id="button">保存</button>
                            <a href="{{url('account')}}" class="btn btn-full">取消</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
<script src="js/sweetalert.min.js"></script>
<script src="external/jquery/jquery-2.1.4.min.js"></script>
<script>
    $(document).on("blur","#userName",function () {
        var userName = $(this).val();
        if(userName==''){
            $(this).next().remove();
            $(this).after("<font color='red' size='1'>名称不能为空</font>");
            return false;
        }else{
            $(this).next().remove();
            return true;
        }
    });
    //性别
    $(document).on("blur","#userSex",function () {
        var userSex = $(this).val();
        if(userSex==''){
            $(this).next().remove();
            $(this).after("<font color='red' size='1'>性别不能为空</font>");
            return false;
        }else{
            $(this).next().remove();
            return true;
        }
    });
    //邮箱验证
    $(document).on("blur","#Email",function () {
        var email = $(this).val();
        var patrn = /^([0-9A-Za-z\-_\.]+)@([0-9A-Za-z]+\.[A-Za-z]{2,3}(\.[A-Za-z]{2})?)$/g;
        if (!patrn.exec(email)){
            $(this).next().remove();
            $(this).after("<font color='red' size='1'>邮箱格式不正确</font>");
            return false;
        }else{
            $(this).next().remove();
            return true;
        }
    });
    //手机号
    $(document).on("blur","#Mobile",function () {
        var mobile = $(this).val();
        var patrn=/^1[3,5,8]\d{9}$/;
        if (!patrn.exec(mobile)){
            $(this).next().remove();
            $(this).after("<font color='red' size='1'>请输入正确的手机格式</font>");
            return false;
        }else{
            $(this).next().remove();
            return true;
        }
    });
    $(document).on("click","#button",function () {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        //ID
        var id = $("#userId").val();
        //名称
        var userName = $("#userName").val();
        //性别
        var userSex = $("#userSex").val();
        //电子邮箱
        var email = $("#Email").val();
        //手机
        var mobile = $('#Mobile').val();
        if(userName=='' && userSex=='' && email=='' && mobile==''){
            swal('请填写信息');
            return false;
        }
        //ajax
        $.ajax({
            type:'post',
            url:"{{url('update')}}",
            data:{id:id,user_name:userName,user_sex:userSex,email:email,mobile:mobile},
            dataType:"json",
            success:function(e){
                swal(e.message);
                if(e.status == "success"){
                    window.location.href="{{url('account')}}";
                }
            }
        })
    })
</script>