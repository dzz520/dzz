<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>MyShop - Responsive HTML5 Template</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="descriptivertical-align: inherit;on" content="MyShop - Responsive HTML5 Template">
    <meta name="author" content="etheme.com">
    <link rel="shortcut icon" href="{{asset('vendor/weblte/favicon.ico')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('vendor/weblte/external/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/weblte/external/slick/slick.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/weblte/external/rs-plugin/css/settings.min.css')}}" media="screen" />
    <link rel="stylesheet" href="{{asset('vendor/weblte/css/template.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/weblte/font/icont-fonts.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/weblte/external/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/weblte/css/sweetalert.css')}}">

</head>
<body>
<header class="no-shadow">
    <!-- mobile-header -->
    <div class="mobile-header">
        <div class="container-fluid">
            <div class="pull-left">
                <!-- language -->
                <div class="mobile-parent-language"></div>
                <!-- /language -->
                <!-- currency -->
                <div class="mobile-parent-currency"></div>
                <!-- /currency -->
                <div class="mini-menu-dropdown dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span class="icon icon-more_horiz"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="mini-menu">
                            <ul>
                                <li class="active"><a href="index.html">家</a></li>
                                <li><a href="faqs.html">交货</a></li>
                                <li><a href="blog_listing.html">博客</a></li>
                                <li><a href="contact.html">往来</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="pull-right">--}}
                {{--<!-- account -->--}}
                {{--<div class="account dropdown">--}}
                    {{--<a class="dropdown-toggle" data-toggle="dropdown"><span class="icon icon-person "></span></a>--}}
                    {{--<div class="dropdown-label hidden-sm hidden-xs">My Account</div>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="http://web.domain.com/register"><span class="icon icon-person"></span>My Account</a></li>--}}
                        {{--<li><a href="wishlist.html"><span class="icon icon-favorite_border"></span>My Wishlist</a></li>--}}
                        {{--<li><a href="compare.html"><span class="fa fa-balance-scale"></span>Compare</a></li>--}}
                        {{--<li><a href="checkout.html"><span class="icon icon-check"></span>Checkout</a></li>--}}
                        {{--<li><a href="#" data-toggle="modal" data-target="#modalLoginForm"><span class="icon icon-lock_outline"></span>Log In</a></li>--}}
                        {{--<li><a href="{{asset('register')}}"><span class="icon icon-person_add"></span>Create an account</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<!-- /account -->--}}
                {{--<!-- cart -->--}}
                {{--<div class="mobile-parent-cart"></div>--}}
                {{--<!-- /cart -->--}}
            {{--</div>--}}
        </div>
        <div class="container-fluid text-center">
            <!-- logo -->
            <div class="logo">
                <a href="javascript:void(0)"><img src="{{asset('vendor/weblte/images/logo-mobile.png')}}" alt=""/></a>
            </div>
            <!-- /logo -->
        </div>
        <div class="container-fluid top-line">
            <div class="pull-left">
                <div class="mobile-parent-menu">
                    <div class="mobile-menu-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="menu-text">
                                菜单
                                </span>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <!-- search -->
                <div class="search">
                    <a href="#" class="search-open"><span class="icon icon-search"></span></a>
                    <div class="search-dropdown">
                        <form action="#" method="get">
                            <div class="input-outer">
                                <input type="search" name="search" value="" maxlength="128" placeholder="Enter keyword">
                                <button type="submit" class="btn-search"><span>搜索</span></button>
                            </div>
                            <a href="#" class="search-close"><span class="icon icon-close"></span></a>
                        </form>
                    </div>
                </div>
                <!-- /search -->
            </div>
        </div>
    </div>
    <!-- /mobile-header -->
    
    <!-- desktop-header -->
    <div class="desktop-header  header-06">
        <div class="top-line">
            <div class="container">
                <div class="pull-left">
                    <!-- logo -->
                    <div class="logo">
                        <a href="javascript:void(0)"><img src="{{asset('images/logo.jpg')}}" alt=""/></a>
                    </div>
                    <!-- /logo -->
                </div>
                <div class="pull-right">
<!--                     <div class="account dropdown" style="margin-top:38px;margin-right: 8px;">
                        <a href="{{asset('/')}}" ><span class="dropdown-label hidden-sm hidden-xs" style="font-size: 18px;font-weight: bold;">首 页</span>
                        </a>
                    </div> -->
                    <!-- account -->
                    <div class="pull-right">
                        <!-- account -->
                        <div class="account dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                @if( Session::get('userInfo.user_name') == ''){
                                <span class="icon icon-person "></span>
                                <span class="dropdown-label hidden-sm hidden-xs">未登录</span>
                                }@else{
                                <span class="icon icon-person "></span>
                                <span class="dropdown-label hidden-sm hidden-xs">{{Session::get('userInfo.user_name')}}</span>
                                }@endif
                            </a>
                            @if (Session::get('userInfo.user_name') == ''){
                                 <ul class="dropdown-menu">
                                    <li><a href="{{asset('login')}}"><span class="icon icon-lock_outline"></span>登录</a></li>
                                    <li><a href="{{asset('register')}}"><span class="icon icon-person_add"></span>创建账户</a></li>
                                </ul>
                            }@else{
                                 <ul class="dropdown-menu">
                                    <li><a href="{{url('account')}}"><span class="icon icon-person"></span>我的账户</a></li>
                                    <li><a href="#"><span class="icon icon-favorite_border"></span>我的收藏</a></li>
                                    <li><a href="#"><span class="fa fa-balance-scale"></span>相比</a></li>
                                    <li><a href="#"><span class="icon icon-check"></span>查看</a></li>
                                    <li><a href="{{asset('logout_account')}}"><span class="icon icon-lock_outline"></span>退出登录</a></li>
                                </ul>
                            }@endif
                        </div>
                    <!-- /account -->
                    <!-- cart -->
                    <div class="main-parent-cart">
                        <div class="cart">
                            <div class="dropdown" onclick="location.href='/shoppingcart/cart'">
                                <a class="dropdown-toggle">
                                    <span class="icon icon-shopping_basket"></span>
                                    <div class="dropdown-label hidden-sm hidden-xs">购物车</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /cart -->
                </div>
            </div>
        </div>
        <div class="top-line">
            <div class="container">
                <div class="pull-left">
                    <div class="menu-parent-box">
                        <!-- header-menu -->
                        <nav class="header-menu">
                            <ul>
                                <li>
                                    <a href="{{asset('/')}}" >首页</a>
                                </li>
                                <li class="dropdown megamenu">
                                    <a href="{{asset('listing')}}">女士服装</a>
                                </li>
                                <li class="dropdown megamenu">
                                    <a href="{{asset('listing')}}">男士服装</a>
                                </li>
                            </ul>
                        </nav>
                        <!-- /header-menu -->
                    </div>
                </div>
                <div class="pull-right">
                    <!-- search -->
                    <div class="search">
                        <a href="#" class="search-open"><span class="icon icon-search"></span></a>
                        <div class="search-dropdown">
                            <form action="#" method="get">
                                <div class="input-outer">
                                    <input type="search" name="search" value="" maxlength="128" placeholder="请输入搜索的">
                                    <button type="submit" class="btn-search">搜索</button>
                                </div>
                                <a href="#" class="search-close"><span class="icon icon-close"></span></a>
                            </form>
                        </div>
                    </div>
                    <!-- /search -->
                </div>
            </div>
        </div>
    </div>
    <!-- /desktop-header -->
    <!-- stuck nav -->
    <div class="stuck-nav">
        <div class="container">
            <div class="pull-left">
                <div class="stuck-menu-parent-box"></div>
            </div>
            <div class="pull-right">
                <div class="stuck-cart-parent-box"></div>
            </div>
        </div>
    </div>
    <!-- /stuck nav -->
</header> <!-- header -->

@yield('common')

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="subscribe-box-01">
                <div class="container">
                    <div class="row mobile-collapse">
                        <h4 class="mobile-collapse_title visible-xs">每天获取最新消息!</h4>
                        <div class="mobile-collapse_content">
                            <div class="col-md-12 col-lg-7">
                                <div class="title hidden-xs">每天获取最新消息!</div>
                                <p>
                                    给我们你的电子邮件，你将每天都得到最新事件的详细更新!
                                </p>
                            </div>
                            <div class="col-md-12 col-lg-5">
                                <form class="form-inline">
                                    <label class="sr-only" for="email">邮件</label>
                                    <span class="addon-icon">
                                            <span class="icon icon-email"></span>
                                            </span>
                                    <input type="email" class="form-control addon-icon" id="email" placeholder="Enter please your e-mail">
                                    <a href="#" class="btn btn-inversion">订阅</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-content-col">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title visible-xs">免运费</h4>
                        <div class="mobile-collapse_content">
                            <a href="#" class="services-block">
                                <span class="icon icon-airplanemode_active"></span>
                                <div class="title">免运费</div>
                                <p>所有产品均免费</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title visible-xs">安全购物</h4>
                        <div class="mobile-collapse_content">
                            <a href="#" class="services-block">
                                <span class="icon icon-security"></span>
                                <div class="title">安全购物</div>
                                <p>我们使用最好的安全功能</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title visible-xs">免费退货</h4>
                        <div class="mobile-collapse_content">
                            <a href="#" class="services-block">
                                <span class="icon icon-assignment_return"></span>
                                <div class="title">自由回报</div>
                                <p>30天内免费返回</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title visible-xs">支持</h4>
                        <div class="mobile-collapse_content">
                            <a href="#" class="services-block">
                                <span class="icon icon-headset_mic"></span>
                                <div class="title">支持</div>
                                <p>有效和友好的支持</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 hidden-xs">
                    <div class="row">
                        <div class="col-sm-4 col-md-12">
                            <div class="footer-logo">
                                <a href="javascript:void(0)"><img src="{{asset('images/logo.jpg')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-12">
                            <div class="social-icon-round">
                                <ul>
                                    <li><a class="icon fa fa-facebook" href="http://www.facebook.com/"></a></li>
                                    <li><a class="icon fa fa-twitter" href="http://www.twitter.com/"></a></li>
                                    <li><a class="icon fa fa-google-plus" href="http://www.google.com/"></a></li>
                                    <li><a class="icon fa fa-instagram" href="https://instagram.com/"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title">信息</h4>
                        <div class="mobile-collapse_content">
                            <div class="v-links-list">
                                <ul>
                                    <li><a href="#">关于我们</a></li>
                                    <li><a href="#">客户服务</a></li>
                                    <li><a href="#">隐私策略</a></li>
                                    <li><a href="#">站点地图</a></li>
                                    <li><a href="#">搜索条件name</a></li>
                                    <li><a href="#">高级搜索</a></li>
                                    <li><a href="#">订单和退货</a></li>
                                    <li><a href="#">联系我们</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title">为什么从美国购买</h4>
                        <div class="mobile-collapse_content">
                            <div class="v-links-list ">
                                <ul>
                                    <li><a href="#">装运和退货</a></li>
                                    <li><a href="#">安全购物</a></li>
                                    <li><a href="#">国际航运</a></li>
                                    <li><a href="#">附属公司</a></li>
                                    <li><a href="#">团体销售</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix divider visible-sm"></div>
                <div class="col-sm-6 col-md-2">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title">我的账户</h4>
                        <div class="mobile-collapse_content">
                            <div class="v-links-list ">
                                <ul>
                                    <li><a href="#">签到</a></li>
                                    <li><a href="#">查看购物车</a></li>
                                    <li><a href="#">我的愿望清单</a></li>
                                    <li><a href="#">跟踪我的订单</a></li>
                                    <li><a href="#">帮助</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="mobile-collapse">
                        <h4 class="mobile-collapse_title">联系人</h4>
                        <div class="mobile-collapse_content">
                            <div class="list-info">
                                <ul>
                                    <li>地址:格拉斯哥圣维森特广场7563号</li>
                                    <li>电话: +777 2345 7885</li>
                                    <li>传真: +777 2345 7886</li>
                                    <li>小时数: 7 Days a week from 10:00 am</li>
                                    <li>E-mail: <a href="mailto:info@mydomain.com">info@mydomain.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container visible-xs">
            <div class="social-icon-round">
                <ul>
                    <li><a class="icon fa fa-facebook" href="http://www.facebook.com/"></a></li>
                    <li><a class="icon fa fa-twitter" href="http://www.twitter.com/"></a></li>
                    <li><a class="icon fa fa-google-plus" href="http://www.google.com/"></a></li>
                    <li><a class="icon fa fa-instagram" href="https://instagram.com/"></a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="pull-right">
                <div class="payment-list">
                    <ul>
                        <li><a class="icon-01" href="#"></a></li>
                        <li><a class="icon-02" href="#"></a></li>
                        <li><a class="icon-03" href="#"></a></li>
                        <li><a class="icon-04" href="#"></a></li>
                        <li><a class="icon-05" href="#"></a></li>
                        <li><a class="icon-06" href="#"></a></li>
                        <li><a class="icon-07" href="#"></a></li>
                        <li><a class="icon-08" href="#"></a></li>
                    </ul>
                </div>
            </div>
            <div class="pull-left">
                <div class="box-copyright">
                    <a href="index.html">MyShop</a> &copy; 2017. <span>版权所有.</span>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="back-to-top">
        <span class="icon icon-keyboard_arrow_up"></span>
        <span class="text">回到顶端</span>
    </a>
</footer>  <!-- footer-->

<!-- /modalWishlist -->
<script src="{{asset('vendor/weblte/external/jquery/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/countdown/jquery.plugin.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/countdown/jquery.countdown.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/slick/slick.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/panelmenu/panelmenu.js')}}"></script>
<script src="{{asset('vendor/weblte/js/quick-view.js')}}"></script>
<script src="{{asset('vendor/weblte/js/main.js')}}"></script>

<!-- /modalWishlist -->
<script src="{{asset('vendor/weblte/external/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/instafeed/instafeed.min.js')}}"></script>
<script src="{{asset('vendor/weblte/external/elevatezoom/jquery.elevatezoom.js')}}"></script>
<script src="{{asset('vendor/weblte/external/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('vendor/weblte/js/sweetalert.min.js')}}"></script>
</body>
</html>