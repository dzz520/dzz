@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $box_title }}</h3>
    </div>
    <div class="box-body">
        <form class="form-horizontal" action="{{url('admin/goodsattr/attrv')}}" method="post" id="form">
            <div class="box-body">
                @if(!empty($attrs))
                <div class="form-group" style="height: 55px">
                    <label class="col-sm-2 control-label">商品属性键</label>
                    <div class="col-sm-10">
                        <select class="form-control select2" style="width: 100%;" name="attr_id">
                            @foreach($attrs as $attr)
                            <option value="{{$attr['attr_id']}}">{{$attr['attr_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="form-group" style="height: 55px">
                    <label for="email" class="col-sm-2 control-label" style="color: #333">商品属性值</label>

                    <div class="col-sm-10">
                        <input type="text" name="attr_value" class="form-control" id="attr_value" placeholder="请输入商品属性值">
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="button" class="btn btn-default btn-flat" onclick="javascript:history.back(-1);">取消</button>
                <button type="submit" id="save" class="btn btn-info pull-right btn-flat">保存</button>
            </div>
            @else
            
            暂无属性键：<a href="{{URL('admin/goodsattrk')}}"><button type="button" class="btn btn-danger">添加属性键</button></a>	 
            
            @endif
        </form>
    </div>
</div>
@stop

@section('css')
@parent
<link rel="stylesheet" href="{{asset('css/bootstrapValidator.min.css')}}">
@stop

@section('js')
@parent
<script src="{{asset('js/bootstrapValidator.min.js')}}"></script>
<script>
    $(function(){
        $('#form').bootstrapValidator({
            live: 'enabled',
            fields: {
                attr_value: {
                    validators: {
                        notEmpty: {message: '请输入属性值'},    //非空提示 
                        regexp: {//匹配规则
                            regexp: /^[A-Z0-9\u4e00-\u9fa5]+$/,  //正则表达式
                            message:'商品属性仅支持汉字或大写字母'
                        },
                    },
                    
                }
            }
        }).on('success.form.bv', function(e) {
            // 阻止表单的默认提交
            e.preventDefault();
            var $form = $(e.target);

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if(result.status == 'success'){
                    $toast.success(result.message);
                    setTimeout(function(){
                        location.href="{{url('admin/goodsattrv')}}"
                    }, 3000);
                }else{
                    $toast.error(result.message);
                    $('#form').bootstrapValidator('disableSubmitButtons', false);
                }
            }, 'json');
        });
    })
</script>
@stop