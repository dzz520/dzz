<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class OrderDetails extends Model
{
	protected $table = 'order_details';

    protected $primaryKey = 'orders_details_id';

    /**
     * 添加订单商品详细信息
     */
    
    public function addDetails($orderDetails)
    {
    	$success = DB::table('order_details')->insert($orderDetails);

    	return $success;
    }



}