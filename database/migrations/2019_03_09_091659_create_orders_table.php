<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id')->commit('自增id');
            $table->string('order_sn',200)->comment('订单货号');
            $table->decimal('order_price',10,2)->comment('订单应付金额');
            $table->decimal('real_price', 10,2)->comment('订单实付金额');
            $table->integer('user_id')->comment('用户id');
            $table->string('order_name', 100)->nullable()->comment('收货人姓名');
            $table->char('order_tel',11)->nullable()->comment('收货人手机');
            $table->string('order_address', 200)->nullable()->comment('收货地址');
            $table->char('order_state',10)->default(1)->comment('订单状态  1-待付款 2-待发货 3-配送中  5-待评价  6-已完成  7-已取消 ');
            $table->char('pay_type',10)->nullable()->comment('支付方式 1-在线支付   2-货到付款');
            $table->char('pay_state',10)->default(2)->comment('支付状态 1-已付款  2-未付款  3-取消付款');
            $table->datetime('pay_time')->default('0000-01-01 00:00:00')->comment('付款时间');
            $table->datetime('sell_time')->default('0000-01-01 00:00:00')->comment('发货时间');
            $table->integer('mode_id')->nullable()->comment('物流id');
            $table->char('mode_sn',50)->nullable()->comment('物流货号');
            $table->string('order_message', 200)->nullable()->comment('订单留言');
            $table->timestamps();
        });
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('order_detailes_id')->commit('自增id');
            $table->integer('order_id')->comment('订单id');
            $table->integer('goods_id')->comment('商品id');
            $table->string('goods_name', 100)->comment('购买商品名称');
            $table->char('buy_num',10)->comment('购买商品数量');
            $table->decimal('goods_price',10,2)->comment('商品价格');
            $table->string('goods_desc',80)->comment('商品信息');
            $table->integer('sku_id')->comment('货品id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_details');
    }
}
