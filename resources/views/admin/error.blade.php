<div id="error-prompt" class="callout callout-danger lead error-prompt" style="position:fixed; top: 0; width:100%; font-size: 14px;">
    <h4>Error</h4>
    <p style="font-weight: bold;">
        {{$errors->prompt->first('message')}}
    </p>
</div>


<script>
    window.onload = function(){
        $('#error-prompt').fadeOut(3000);
    }
</script>

