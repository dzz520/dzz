<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('address_id')->comment('自增id');
            $table->integer('user_id')->comment('用户id')->nullable();
            $table->string('accept_name', 100)->comment('收货人')->nullable();
            $table->char('postcode',11)->comment('邮政编码')->nullable();
            $table->char('mobile',11)->comment('电话号码')->nullable();
            $table->string('hcity', 100)->comment('收货地址')->nullable();
            $table->integer('is_default')->default('1')->comment('是否默认,2:为非默认,1:默认')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
