<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Session;
use Log;

class Register extends Model
{
    protected $table = 'user';

    /**
     * 用户注册
    */
    public function add($data)
    {
        foreach($data as $field => $value){
                $this->$field = $value;
        }
        $result = true;
        try{
            $this->save();
        }catch(\Exception $e){
            $result = false;
            Log::error('admin:add '.$e->getMessage());
        }

        return $result;
    }
    //查询
    public function find($data){
        return DB::table('user')->where('user_name',$data['user_name'])->first();
    }
    /**
     * 检测用户名是否存在
     */
    public static function isExistName($userName, $id = null)
    {
        $query = self::where('user_name', $userName);
        if($id){
            $result = $query->where('id', '!=', $id);
        }
        $result = $query->first();

        return $result ? true : false;
    }

    /**
     * 检测邮箱是否存在
     */
    public static function isExistEmail($email, $id = null)
    {
        $query = self::where('email', $email);
        if($id){
            $result = $query->where('id', '!=', $id);
        }
        $result = $query->first();

        return $result ? true : false;
    }

    /**
     * 检测手机号是否存在
     */
    public static function isExistMobile($mobile, $id = null)
    {
        $query = self::where('mobile', $mobile);
        if($id){
            $result = $query->where('id', '!=', $id);
        }
        $result = $query->first();

        return $result ? true : false;
    }

    /**
     * 编辑个人信息
     */
    public function edit($id, $data)
    {
        $user = $this->find($id);
        foreach ($data as $field => $value){
            $user->$field = $value;
        }

        $result = true;
        try{
            $user->save();
        } catch(\Exception $e){
            $result = false;
            Log::error('admin:edit '.$e->getMessage());
        }

        return $result;
    }
}