<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

/**
 * 
 */
class GoodsAttr extends Model
{
    protected $table = 'goods_attr';

    public function getlistId($id)
    {
        return DB::table('goods_show')
            ->where('id',$id)
            ->first();
    }

    public function attributeId($id)
    {
        $goods = DB::table('goods_attr')
        ->join('attr', 'goods_attr.attr_id', '=', 'attr.attr_id')
        ->join('attr_value','goods_attr.value_id','=','attr_value.value_id')
        ->where('goods_attr.goods_id',$id)
        ->get()->map(function($value){
            return (array)$value;
        })
        ->toArray();
        return $goods;
    }

    public function record($data,$goodsId)
    {
        return DB::table('cargo')
            ->where('goods_id','=',$goodsId)
            ->where('value_list','=',$data)
            ->where('goods_number','>',0)
            ->get()->map(function($value){
            return (array)$value;
        })
        ->toArray();
    }

    public function num($id)
    {
        return count(DB::table('goods_attr')
            ->where('goods_id','=',$id)
            ->groupBy('attr_id')
            ->get()->map(function($value){
            return (array)$value;
        })
        ->toArray());
    }

    public function scope($id)
    {
        return DB::table('cargo')
            ->select(DB::raw('max(cargo_price) as max,min(cargo_price) as min'))
            ->where('goods_id','=',$id)
            ->first();
    }

    public function img($id)
    {
         return DB::table('goods_image')
            ->where('goods_id','=',$id)
            ->get()->map(function($value){
            return (array)$value;
        })
        ->toArray();
    }

}