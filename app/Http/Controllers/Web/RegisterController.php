<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 2019/2/18
 * Time: 10:31
 */

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\Register;
use Validator;

class RegisterController extends Controller
{
    /**
     * 注册
     */
    public function registerDo(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'user_name' => [
                'required',
                function($attribute, $value, $fail) {
                    if(!preg_match('/^[\x7f-\xff]+$/', $value)){
                        return $fail('名称必须是中文');
                    }
                }
            ],
            'email' => 'email',
            'user_pwd' => [
                'required',
                    function($attribute, $value, $fail) {
                        if(!preg_match('/^\w{6,}$/', $value)){
                            return $fail('密码不能小于六位');
                        }
                    }
                ]
            ],[
            'user_name.required' => '名称不能为空',
            'email.required' => '邮箱不能为空',
            'user_pwd.required' => '密码不能为空',
        ]);
        if ($validator->fails()) {
            return $this->ajaxError($validator->errors()->first());
        }
        // 验证是否存在
        $id = intval($request->id);
        if(Register::isExistName($request->user_name, $id)){
            return $this->ajaxError('名称已存在');
        }
        if(Register::isExistEmail($request->email, $id)){
            return $this->ajaxError('邮箱已存在');
        }
        //注册
        $data['user_name'] = $request->user_name;
        $data['rand_key'] = str_random(8);
        $pwd = $request->user_pwd;
        $data['user_pwd'] = md5(md5($pwd).$data['rand_key']);
        $data['email'] = $request->email;
        $time = time();
        $data['last_login_time'] = date('Y-m-d H:m:s',$time);
        $content = '注册成功!欢迎来到我的衣柜';
        Mail::raw($content, function ($message) use ($data) {
            $message->subject($data['user_name']);
                $message->to($data['email']);
        });
        //model
        $user = new Register;
        $result = $user->add($data);
        $res = $user->find($data);
        $data = json_decode(json_encode($res), true);
        //通过调用请求实例的 put 方法
        $request->session()->put("userInfo",$data);
        return $result ? $this->ajaxSuccess('注册成功') : $this->ajaxError('注册失败');
    }
}