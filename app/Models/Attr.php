<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Attr extends Model
{
    protected $table = 'attr';

    /**
    *检测属性键是否存在
    */
    public static function isExistAttrk($attrName,$typeId)
    {
        return DB::select("select * from attr WHERE attr_name = '$attrName' AND type_id = $typeId");
    }

    /**
    *检测属性值是否存在
    */
    public static function isExistAttrv($attrName)
    {
        $query = DB::table('attr_value')->where('attr_value', $attrName);
        
        $result = $query->first();
        
        return $result ? true : false;
    }

    /**
    *查询属性键
    */
    public function getAttrInfo()
    {
        return Db::table('attr')
        ->get()
        ->map(function ($value) {
            return (array)$value;
        })
        ->toArray();
    } 

    /**
    *添加属性键入库
    */
    public function addk($data)
    {
        return DB::table('attr')->insert($data);
    } 

    /**
    *添加属性值入库
    */
    public function addv($data)
    {
        return DB::table('attr_value')->insert($data);
    } 
}



