@extends('web.layouts.common')
@section('common')
    <link rel="stylesheet" type="text/css" href="css/sweetalert.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="breadcrumb">
        <div class="container">
            <ul>
                <li><a href="index.html">家</a></li>
                <li>登录</li>
            </ul>
        </div>
    </div>
    <!-- Content -->
    <div id="pageContent">
        <div class="container offset-18">
            <h1 class="block-title large">登录</h1>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="login-form-box">
                        <h2>新客户</h2>
                        <div class="extra-indent-bottom">
                            <p>通过在我们的商店创建一个账户，你将能够更快地完成结帐过程，存储多个送货地址，查看和跟踪你账户中的订单等等.</p>
                        </div>
                        <div class="extra-bottom">
                            <a href="register"><button class="btn btn-border color-default">创建帐户</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="login-form-box">
                        <h2>注册客户</h2>
                        <p>
                            如果你在我们这里有账户，请登录.
                        </p>
                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                    <span class="icon icon-person_outline"></span>
                                    </span>
                                    <input type="text" id="LoginFormEmail1" class="form-control" placeholder="请输入邮箱">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                    <span class="icon icon-lock_outline"></span>
                                    </span>
                                    <input type="password" id="LoginFormPass1" class="form-control" placeholder="请输入密码">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-3">
                                    <button type="button" id="submit_button" class="btn">登录</button>
                                </div>
                                <div class="col-md-12 col-lg-9">
                                    <ul class="additional-links">
                                        <li><a href="#">忘了你的密码?</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
    <script src="js/sweetalert.min.js"></script>
    <script src="http://libs.baidu.com/jquery/1.9.0/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).on("click","#submit_button",function(){
            $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
            var email = $("#LoginFormEmail1").val();
            var pwd = $("#LoginFormPass1").val();

            if (email == "") {
                swal("邮箱不能为空");
                return false;
            } else {
                var patrn = /^([0-9A-Za-z\-_\.]+)@([0-9A-Za-z]+\.[A-Za-z]{2,3}(\.[A-Za-z]{2})?)$/g;
                if (!patrn.exec(email)){
                    swal('邮箱格式不正确');
                    return false;
                }
            }

            if (pwd == "") {
                swal("密码不能为空");
                return false;
            }

            $.ajax({
                url:"login_do",
                data:{
                      email:email,
                      pwd:pwd
                     },
                dataType:"json",
                type:"post",
                success:function(e){
                    swal(e.message);
                    if (e.status == "success") {
                        window.location.href="/";
                    }
                }
            })
        })
    </script>