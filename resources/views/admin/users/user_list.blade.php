@extends('admin.template')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{$box_title}}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body" style="clear: both">
        <table id="table" class="table table-bordered table-striped"></table>
    </div>
</div>
@stop

@section('js')
@parent
<script src="{{asset('js/layer/layer.js')}}"></script>
<script>
    $(function(){
        var status;
        var statusval;
        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            "oLanguage": {
                "sLengthMenu": "每页显示 _MENU_ 条记录",
                "sZeroRecords": "抱歉， 没有找到",
                "sInfo": "从 _START_ 到 _END_ /共 _TOTAL_ 条数据",
                "sInfoEmpty": "没有数据",
                "sInfoFiltered": "(从 _MAX_ 条数据中检索)",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "前一页",
                    "sNext": "后一页",
                    "sLast": "尾页"
                },
                "sZeroRecords": "没有检索到数据",
            },
            ordering: false,
            ajax: "{{url('admin/users/data')}}",
            columns: [
                {data: 'id'},
                {data: 'user_name'},
                {data: 'user_sex'},
                {data: 'user_real_name'},
                {data: 'user_img'},
                {data: 'is_freeze'},
                {data: 'email'},
                {data: 'mobile'},
                {data: 'last_login_time'},
            ],
            columnDefs: [
                {title: 'ID', targets: 0},
                {title: '用户名', targets: 1},
                {title: '用户性别', targets: 2},
                {title: '用户真实姓名', targets: 3},
                {title: '用户头像', targets: 4},
                {title: '账号是否冻结', targets: 5},
                {title: '邮箱', targets: 6},
                {title: '手机号', targets: 7},
                {title: '最后一次登录时间', targets: 8},
                {
                    title: '操作', 
                    targets: 9,
                    render: function(data, type ,row){
                        if(row.is_freeze=='可用'){
                            status='冻结';
                            statusval=0;
                        }else{
                            status='可用';
                            statusval=1;
                        }
                        return '<span ref="'+row.id+'"> \
                                <a href="javascript:;" class="btn btn-sm btn-info btn-flat status" user_id="'+row.id+'" status="'+statusval+'">'+status+'</a> | <a href="javascript:;" class="btn btn-sm btn-flat btn-danger" id="delete" data-id="'+row.id+'" >删除</a> \
                                </span>';
                    }
                },
            ]
        });

       $('#table').on('click', '#delete', function(){
                var id = $(this).data('id');
                //alert(id);
                layer.msg('确定要删除该条记录？', {
                    time: 0, //不自动关闭
                    btn: ['确定', '取消'],
                    yes: function(index){
                        $.get("{{url('admin/users/delete')}}", {id: id}, function(result){
                            if(result.status == 'success'){
                                $toast.success(result.message);
                                table.ajax.reload();
                                layer.close(index);
                            }else{
                                $toast.error(result.message);
                            }
                        }, 'json')
                    }
                });
            })
    })

    $('#table').on("click",".status",function(){
        var id = $(this).attr('user_id');

        var status = $(this).attr('status');
            $.ajax({
            url:"{{url('admin/users/eint')}}",
            data:{id:id,status:status},
            success(e){
                location.reload();
            }
        })
    });
</script>
@stop