<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id')->comment('购物车id');
            $table->integer('sku_id')->comment('库存ID');
            $table->string('attr_name', 50)->comment('属性名称');
            $table->integer('goods_id')->comment('商品id');
            $table->integer('user_id')->comment('用户id');
            $table->string('goods_number', 10)->comment('购物车商品数量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}