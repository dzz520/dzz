<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Attr;
use App\Models\Categorys;

class GoodsAttrController extends Controller
{
    /*
    *添加属性
    */
    public function attrk()
    {
        //获取分类信息
        $categorys = new Categorys;
        $query = $categorys->get();
        $typeInfo = $this->getTree($query);
        return view('admin.goodsattr.attrk', [
            'content_header' => '添加管理',
            'box_title' => '商品属性名添加',
            'typeInfo' => $typeInfo,
        ]);
    }

    /**
     * 递归算法
     */
    public function getTree($array, $pid =0, $level = 0)
    {
        static $list = [];
        foreach ($array as $value){
            if ($value->pid == $pid){
                $value->level = $level;
                $list[] = $value;
                $this->getTree($array, $value->type_id, $level+1);
            }
        }
        return $list;
    }

    /*
    *添加属性值
    */
    public function attrv()
    {
        $attrInfo = (new Attr)->getAttrInfo();

        return view('admin.goodsattr.attrv', [
            'content_header' => '添加管理',
            'box_title' => '商品属性值添加',
            'attrs' => $attrInfo,
        ]);
    }

    /**
     * 保存属性键数据
     */ 
    public function addAttrk(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ],[
            'name.required' => '商品属性不能为空',
        ]);

        if(Attr::isExistAttrk($request->name, $request->type_id)){
            return $this->ajaxError('属性名已存在');

        }

        $data['attr_name'] = $request->name;
        $data['type_id'] = $request->type_id;

        $result = (new Attr)->addk($data);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');

    }

    /**
     * 保存属性值数据
     */ 
    public function addAttrv(Request $request)
    {
        // 验证
        $validator = Validator::make($request->all(), [
            'attr_value' => 'required',

        ],[
            'attr_value.required' => '商品属性值不能为空',
        ]);

        // 验证是否存在
        $adminId = intval($request->id);

        if(Attr::isExistAttrv($request->attr_value, $adminId)){
            return $this->ajaxError('属性值已存在');
        }
        $data['attr_id'] = $request->attr_id;
        $data['attr_value'] = $request->attr_value;
        // dd($data);
        $result = (new Attr)->addv($data);

        return $result ? $this->ajaxSuccess('操作成功') : $this->ajaxError('操作失败');

    }

    /**
     * 检测数据是否存在
     */
    public function isExistName(Request $request)
    {
        $result = true;
        $checkField = $request->name;
        if($request->type == 'name'){
            if(Attr::isExistName($checkField)){
                $result = false;
            }
        }else{
            return $this->ajaxError('非法请求');
        }

        return [
            'valid' => $result
        ];
    }

}