<?php

/**
 * secret_key 生成签名需要用到的秘钥
 * sign_timeout 签名过期时间，单位秒(s)，0表示永不过期
 */

return [
	'sign_secret_key' => env('SIGN_SECRET_KEY', ''),
	'sign_timeout' => env('SIGN_TIMEOUT', 0),
	'debug' => env('SIGN_DEBUG', false)
];