<?php

return [
	'admin_domain' => env('ADMIN_DOMAIN', ''),
	'api_domain' => env('API_DOMAIN', ''),
	'web_domain' => env('WEB_DOMAIN', '')
];